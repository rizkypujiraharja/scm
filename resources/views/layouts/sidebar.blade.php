                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-50 img-radius" src="{{Auth::user()->getPhoto()}}" alt="P-Store.Net">
                                    <div class="user-details">
                                        <span>{{ $signed_user->nama }}</span>
                                        <span id="more-details">
                                          @php
                                            $luser = ['admin', 'konsumen', 'supplier'];
                                          @endphp
                                          @if ( in_array($level, $luser) )
                                            {{$level}}
                                          @else
                                            {{ Auth::user()->pegawai->getJabatan() }}
                                          @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">

                                {{-- @include('layouts.menu.all') --}}
                            @include('layouts.menu.'.$level)


                            </ul>
                        </div>
