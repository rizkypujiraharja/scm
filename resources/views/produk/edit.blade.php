@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Edit Data Produk</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('produk.index') }}">Produk</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Edit Produk</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="{{ route('produk.update', $produk) }}" method="post" class="j-pro" id="j-pro" novalidate="" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="j-content">
                                <!-- start name -->
                                <div class="j-unit">
                                    <label class="j-label">Nama Lengkap</label>
                                    <div class="j-input {{ $errors->has('name') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="name">
                                            <i class="icofont icofont-ui-user"></i>
                                        </label>
                                        <input type="text" id="name" name="name" value="{{ old('name') ?? $produk->nama }}">
                                        @if ( $errors->has('name') )
                                            <span class="j-error-view">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->
                                <!-- start name -->
                                <div class="j-span6 j-unit">
                                    <label class="j-label">Mak Produksi (Yard/Hari)</label>
                                    <div class="j-input {{ $errors->has('max') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="max">
                                            <i class="icofont icofont-settings"></i>
                                        </label>
                                        <input type="text" id="max" name="max" value="{{ old('max') ?? $produk->max_produksi_per_hari }}">
                                        @if ( $errors->has('max') )
                                            <span class="j-error-view">{{$errors->first('max')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->
                                <!-- start name -->
                                <div class="j-span6 j-unit">
                                    <label class="j-label">Harga</label>
                                    <div class="j-input {{ $errors->has('harga') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="harga">
                                            <i class="icofont icofont-coins"></i>
                                        </label>
                                        <input type="text" id="harga" name="harga" value="{{ old('harga') ?? $produk->harga }}">
                                        @if ( $errors->has('harga') )
                                            <span class="j-error-view">{{$errors->first('harga')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->

                                <div class="clearfix"></div>

                                <div class="j-unit">
                                  <label class="j-label">Gambar</label>
                                  <div class="j-input j-append-small-btn {{ $errors->has('gambar') ? ' j-error-view' : '' }}">
                                    <div class="j-file-button">
                                        Browse
                                        <input type="file" id="file2" name="gambar" onchange="document.getElementById('file2_input').value = this.value;">
                                    </div>
                                    <input type="text" id="file2_input" readonly="" placeholder="Upload gambar Pegawai">
                                    <span class="j-hint">Dimensi gambar 4:3 </span>
                                    @if ( $errors->has('gambar') )
                                        <span class="j-error-view">{{$errors->first('gambar')}}</span>
                                    @endif
                                  </div>
                                </div>

                                <!-- start response from server -->
                                <div class="j-response"></div>
                                <!-- end response from server -->
                            </div>
                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus produk bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
