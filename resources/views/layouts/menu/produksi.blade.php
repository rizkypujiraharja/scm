
                                <li class="{{ Request::is('produk/*') ? 'active' : '' }}">
                                    <a href="{{ route('produk.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Data Produk</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('bom/*') ? 'active' : '' }}">
                                    <a href="{{ route('bom.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>B</b></span>
                                        <span class="pcoded-mtext">Data BOM</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('pesanan/design-baru*') ? 'active' : '' }}">
                                    <a href="{{ route('pesanan.design.baru.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>D</b></span>
                                        <span class="pcoded-mtext">Design Baru</span>

                                        @if($jml['designBaru'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['designBaru']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                
                                
                                <li class="{{ Request::is('produksi*') ? 'active' : '' }}">
                                    <a href="{{ route('produksi.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Produksi</span>
                                        @if($jml['produksi_proses'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['produksi_proses']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>