@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Penjualan</h4>
                    <span>Kirim bahan baku ke konsumen</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bom.index') }}">Penjualan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama<br>Bahan Baku</th>
                            <th>Stock</th>
                            <th>Jumlah</th>
                            <th>Jumlah<br>1x Kirim</th>
                            <th>Waktu<br>Antar</th>
                            <th>Dikirim</th>
                            <th>Tanggal</th>
                            <th>Catatan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <select class="form-control" name="filter[produk]">
                                    <option value="">Semua</option>
                                    @foreach ($produks as $produk)
                                        <option value="{{$produk->kode_produk}}" {{ !empty($param['produk']) ? ($produk->kode_produk == $param['produk'] ? 'selected' : '') : '' }}>[{{$produk->kode_produk}}] - {{$produk->nama}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('bom.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr> --}}
                    @foreach ($eoqData as $eoq)
                    @if ($eoq->dikirim < $eoq->frekuensi )
                        <tr>
                            <td>{{ $eoq->bahanBaku->nama }}</td>
                            <td>{{ App\BahanBakuSupplier::where('kode_supplier', $eoq->kode_supplier)->where('kode_bahan_baku', $eoq->kode_bahan_baku)->first()->stock }}</td>
                            <td>{{ rupiah($eoq->kebutuhan) }}</td>
                            <td>{{ rupiah($eoq->jumlah) }}</td>
                            <td>{{ $eoq->waktu_antar }} hari</td>
                            <td>{{ $eoq->dikirim }}</td>
                            <td>{{ date('d-m-Y', strtotime($eoq->pengadaan->tanggal)) }}</td>
                            <td>{!! $eoq->catatan !!}</td>
                            <td>
                                @if ($eoq->dikirim < $eoq->frekuensi )
                                    <a href="{{ route('supplier.penjualan.kirim', $eoq) }}"><button class="btn btn-mini btn-primary">Kirim</button></a>
                                @else
                                    <span class="label label-success">Selesai</span>
                                @endif
                            </td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $eoqData->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus Penjualan ini ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
