<?php

namespace App\Http\Controllers;

use App\{BahanBaku, Supplier, BahanBakuSupplier, PenerimaanBahanBaku};
use Illuminate\Http\Request;
use Exception, DB;

class BahanBakuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = $request->filter;
        
        $bahanBakus = BahanBaku::where('nama', 'like', '%' . ($request->filter['nama'] ?? '')  . '%')
                ->where('kode_bahan_baku', 'like', '%' . ($request->filter['kode'] ?? '')  . '%')
                ->where('satuan', 'like', '%' . ($request->filter['satuan'] ?? '')  . '%')
                ->paginate(10);
        
        return view('bahan-baku.index', compact('bahanBakus', 'param'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::get();

        return view('bahan-baku.create', compact('suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'string|max:50',
            'satuan' => 'string|max:50',
        ]);

        DB::beginTransaction();
        try {

            $n = explode(' ', $request->name);
            $kode = 'B_'.ucwords($n[0]);

            $cek = BahanBaku::where('kode_bahan_baku', 'like', $kode.'\_%')->orderBy('kode_bahan_baku', 'DESC')->first();

            if( is_null($cek) ){
                $kode = $kode.'-01';
            }else{
                $last = explode('-', $cek->kode_bahan_baku);
                $new = sprintf("%'02d", $last[1]+1);
                $kode = $kode.'-'.$new;
            }

            $bahanBaku = new BahanBaku;
            $bahanBaku->kode_bahan_baku = $kode;
            $bahanBaku->nama = $request->name;
            $bahanBaku->satuan = $request->satuan;
            $bahanBaku->stock = 0;
            $bahanBaku->save();

            $old = [];

            if( count($request->supplier) > 0 ){

                foreach ($request->supplier as $key => $supplier) {

                    if (in_array($request->supplier[$key], $old)){
                        throw new Exception("Supplier Tidak Boleh Duplikat");
                    }

                    if (intVal($request->harga[$key]) < 1){
                        throw new Exception("Harga tidak boleh kosong");
                    }

                    $bahanBakuSupplier = new BahanBakuSupplier;
                    $bahanBakuSupplier->kode_bahan_baku = $bahanBaku->kode_bahan_baku;
                    $bahanBakuSupplier->kode_supplier = $supplier;
                    $bahanBakuSupplier->harga = intVal($request->harga[$key]);
                    $bahanBakuSupplier->min_order = intVal($request->min_order[$key]);
                    $bahanBakuSupplier->stock = $request->stock[$key];
                    $bahanBakuSupplier->save();

                    $old[] = $supplier;
                }

            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());
        }

        return redirect()->route('bahan-baku.index')->with('alert-success', 'Berhasil menambah bahan baku!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BahanBaku  $bahanBaku
     * @return \Illuminate\Http\Response
     */
    public function show(BahanBaku $bahanBaku)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BahanBaku  $bahanBaku
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = Supplier::get();

        $bahanBaku = BahanBaku::findOrFail($id);

        return view('bahan-baku.edit', compact('bahanBaku', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BahanBaku  $bahanBaku
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BahanBaku $bahanBaku)
    {
        $this->validate($request, [
            'name' => 'string|max:50',
            'satuan' => 'string|max:50',
        ]);
        DB::beginTransaction();
        try {

            $bahanBaku->nama = $request->name;
            $bahanBaku->satuan = $request->satuan;
            $bahanBaku->save();
            
            $old = [];
            foreach ($request->supplier as $key => $supplier) {

                if (in_array($request->supplier[$key], $old)){
                    throw new Exception("Supplier Tidak Boleh Duplikat");
                }

                if (intVal($request->harga[$key]) < 1){
                    throw new Exception("Harga tidak boleh kosong");
                }

                BahanBakuSupplier::where('kode_bahan_baku', $bahanBaku->kode_bahan_baku)
                ->updateOrcreate(
                    ['kode_supplier' => $request->supplier[$key]],
                    [
                        'kode_supplier' => $request->supplier[$key],
                        'kode_bahan_baku' => $bahanBaku->kode_bahan_baku,
                        'harga' => $request->harga[$key],
                        'min_order' => intVal($request->min_order[$key]),
                        'stock' => $request->stock[$key]
                    ]    
                );

                $old[] = $supplier;
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());
        }

        return redirect()->route('bahan-baku.index')->with('alert-success', 'Berhasil mengubah bahan baku!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BahanBaku  $bahanBaku
     * @return \Illuminate\Http\Response
     */
    public function destroy(BahanBaku $bahanBaku)
    {
        DB::beginTransaction();
        try {

            $bahanBaku->supplier()->delete();
            $bahanBaku->delete();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        return redirect()->route('bahan-baku.index')->with('alert-success', 'Berhasil menghapus bahan baku!');
    }

    public function penerimaan()
    {
        $penerimaanData = PenerimaanBahanBaku::orderBy('id', 'DESC')->paginate(20);

        return view('penerimaan.index', compact('penerimaanData'));
    }

    public function terima($id)
    {
        DB::beginTransaction();
        try {

            $penerimaan = PenerimaanBahanBaku::where('id', $id)->first();
            $penerimaan->status = 1;

            $penerimaan->save();

            $bahanBaku = BahanBaku::findOrFail($penerimaan->eoq->bahanBaku->kode_bahan_baku);
            $bahanBaku->stock = $bahanBaku->stock + $penerimaan->jumlah;
            $bahanBaku->save();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }
        
        return redirect()->route('penerimaan.index')->with('alert-success', 'Pengadaan berhasil diterima');
    }
}
