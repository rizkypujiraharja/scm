@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Data Bahan Baku</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bahan-baku.index') }}">Bahan Baku</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Bahan Baku</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="{{ route('bahan-baku.store') }}" method="post" class="j-forms j-pro" id="j-forms" novalidate="" enctype="multipart/form-data">
                            @csrf
                            <div class="content">

                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Nama</label>
                                        <div class="j-input {{ $errors->has('name') ? ' j-error-view' : '' }}">
                                            <label class="j-icon-right" for="name">
                                                <i class="icofont icofont-ui-user"></i>
                                            </label>
                                            <input type="text" id="name" name="name" value="{{ old('name') }}">
                                            @if ( $errors->has('name') )
                                                <span class="j-error-view">{{$errors->first('name')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Satuan</label>
                                                <div class="j-input ">
                                                    <label class="input select" id="show-elements-select">
                                                            <select name="satuan">
                                                                <option value="Kg">Kg</option>
                                                                <option value="Liter">Liter</option>
                                                            </select>
                                                    </label>
                                                </div>
                                        @if ( $errors->has('satuan') )
                                            <span class="j-error-view">{{$errors->first('satuan')}}</span>
                                        @endif
                                        </div>
                                    </div>

                                <div class="j-divider j-gap-bottom-25"></div>

                            @if( is_null(old('supplier')) )
                                <div class="clone-link cloneya-wrap">
                                    <div class="toclone cloneya">
                                        <button href="#" class=" clone btn btn-primary m-b-15">Tambah Supplier</button>
                                        <button href="#" class=" delete  btn btn-danger m-b-15">Hapus Supplier</button>
                                        <div class="j-row">
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Supplier</label>
                                                <div class="j-input ">
                                                    <label class="input select" id="show-elements-select">
                                                            <select name="supplier[]">
                                                                @foreach ($suppliers as $supplier)
                                                                    <option value="{{$supplier->kode_supplier}}">
                                                                        {{$supplier->user->nama}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Harga</label>
                                                <div class="j-input {{ $errors->has('harga') ? ' j-error-view' : '' }}">
                                                    <label class="j-icon-right" for="harga">
                                                        <i class="icofont icofont-coins"></i>
                                                    </label>
                                                    <input type="text" id="harga" name="harga[]">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Stock</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="stock">
                                                        <i class="icofont icofont-box"></i>
                                                    </label>
                                                    <input type="text" id="stock" name="stock[]">
                                                </div>
                                            </div>
                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Min Order</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="min_order">
                                                        <i class="icofont icofont-box"></i>
                                                    </label>
                                                    <input type="text" id="min_order" name="min_order[]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end /.toclone -->
                                </div>
                            @else
                            @foreach ( old('supplier') as $key => $data)
                            <div class="clone-link cloneya-wrap">
                              <div class="toclone cloneya">
                                  <button href="#" class=" clone btn btn-primary m-b-15">Tambah Supplier</button>
                                  <button href="#" class=" delete  btn btn-danger m-b-15">Hapus Supplier</button>
                                  <div class="j-row">
                                      <div class="j-span6 j-unit">
                                          <label class="j-label">Supplier</label>
                                          <div class="j-input ">
                                              <label class="input select" id="show-elements-select">
                                                      <select name="supplier[]">
                                                          @foreach ($suppliers as $supplier)
                                                              <option value="{{$supplier->kode_supplier}}"  {{ old('supplier.'.$key) == $supplier->kode_supplier ? 'selected' : ''}}>
                                                                  {{$supplier->user->nama}}
                                                              </option>
                                                          @endforeach
                                                      </select>
                                              </label>
                                          </div>
                                      </div>
                                      <div class="j-span6 j-unit">
                                          <label class="j-label">Harga</label>
                                          <div class="j-input {{ $errors->has('harga') ? ' j-error-view' : '' }}">
                                              <label class="j-icon-right" for="harga">
                                                  <i class="icofont icofont-coins"></i>
                                              </label>
                                              <input type="text" id="harga" name="harga[]"  value="{{ old('harga.'.$key) }}">
                                          </div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div class="j-span6 j-unit">
                                          <label class="j-label">Stock</label>
                                          <div class="j-input">
                                              <label class="j-icon-right" for="stock">
                                                  <i class="icofont icofont-box"></i>
                                              </label>
                                              <input type="text" id="stock" name="stock[]" value="{{ old('stock.'.$key) }}">
                                          </div>
                                      </div>
                                      <div class="j-span6 j-unit">
                                          <label class="j-label">Min Order</label>
                                          <div class="j-input">
                                              <label class="j-icon-right" for="min_order">
                                                  <i class="icofont icofont-box"></i>
                                              </label>
                                            <input type="text" id="min_order" name="min_order[]" value="{{ old('min_order.'.$key) }}">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!-- end /.toclone -->
                          </div>
                            @endforeach
                            @endif
                                <!-- start response from server -->
                                <div class="j-response"></div>
                                <!-- end response from server -->
                            </div>
                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')

    <script type="text/javascript" src="{{ asset('guru/default/assets/pages/j-pro/js/jquery-cloneya.min.js') }}"></script>
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus bahan-baku bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
$(document).ready(function(){

        /***************************************/
        /* Cloned element */
        /***************************************/
        $('.clone-widget').cloneya();

        $('.clone-rightside-btn-1').cloneya();

        $('.clone-rightside-btn-2').cloneya();

        $('.clone-leftside-btn-1').cloneya();

        $('.clone-leftside-btn-2').cloneya();

        $('.clone-link').cloneya();
        /***************************************/
        /* end Cloned element */
        /***************************************/

    });
</script>
@endsection
