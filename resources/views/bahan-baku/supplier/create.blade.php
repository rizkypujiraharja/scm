@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Data Bahan Baku</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bahan-baku-supplier.index') }}">Bahan Baku</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Bahan Baku</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="{{ route('bahan-baku-supplier.store') }}" method="post" class="j-forms j-pro" id="j-forms" novalidate="" enctype="multipart/form-data">
                            @csrf
                            <div class="content">
                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Bahan Baku</label>
                                                <div class="j-input ">
                                                    <label class="input select" id="show-elements-select">
                                                            <select name="bb">
                                                                @foreach ($bahanBakus as $bahanBaku)
                                                                    <option value="{{ $bahanBaku->kode_bahan_baku }}">{{ $bahanBaku->nama }} [{{ $bahanBaku->satuan }}]</option>
                                                                @endforeach
                                                            </select>
                                                    </label>
                                                </div>
                                        @if ( $errors->has('satuan') )
                                            <span class="j-error-view">{{$errors->first('satuan')}}</span>
                                        @endif
                                        </div>

                                            <div class="j-span6 j-unit">
                                                <label class="j-label">Min Order</label>
                                                <div class="j-input">
                                                    <label class="j-icon-right" for="min_order">
                                                        <i class="icofont icofont-box"></i>
                                                    </label>
                                                    <input type="text" id="min_order" name="min_order">
                                                </div>
                                            </div>
                                </div>
                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Stock</label>
                                        <div class="j-input">
                                            <label class="j-icon-right" for="name">
                                                <i class="icofont icofont-package"></i>
                                            </label>
                                            <input type="text" id="name" name="stock" value="">
                                        </div>
                                    </div>
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Harga</label>
                                        <div class="j-input">
                                            <label class="j-icon-right" for="harga">
                                                <i class="icofont icofont-coins"></i>
                                            </label>
                                            <input type="text" id="name" name="harga" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('guru/default/assets/pages/j-pro/js/jquery-cloneya.min.js') }}"></script>
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus bahan-baku bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
$(document).ready(function(){

        /***************************************/
        /* Cloned element */
        /***************************************/
        $('.clone-widget').cloneya();

        $('.clone-rightside-btn-1').cloneya();

        $('.clone-rightside-btn-2').cloneya();

        $('.clone-leftside-btn-1').cloneya();

        $('.clone-leftside-btn-2').cloneya();

        $('.clone-link').cloneya();
        /***************************************/
        /* end Cloned element */
        /***************************************/

    });
</script>
@endsection
