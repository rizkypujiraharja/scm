@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Pesanan</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pesanan.index') }}">Pesanan</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Pesanan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- clone elements card start -->
            <div class="card">
                <div class="card-block">
                    <div class="wrapper wrapper-640">
                        <form action="{{ route('pesanan.design.baru.store') }}" method="POST" enctype="multipart/form-data" class="j-forms j-pro" id="j-forms" novalidate>
                            @csrf
                            <!-- end /.header-->
                            <div class="content">
                                <div class="j-unit">
                                    <label class="j-label">Nama Produk</label>
                                    <div class="j-input {{ $errors->has('nama') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="nama">
                                            <i class="icofont icofont-layers"></i>
                                        </label>
                                        <input type="text" id="nama" name="nama" value="{{ old('nama') }}">
                                        @if ( $errors->has('nama') )
                                            <span class="j-error-view">{{$errors->first('nama')}}</span>
                                        @endif
                                    </div>
                                </div>

@if(session('user_level') != 'konsumen')
                                <div class="j-unit">
                                    <label class="j-label">Konsumen</label>
                                        <div class="j-input ">
                                            <label class="input select" id="show-elements-select">
                                                    <select name="konsumen">
                                                         @foreach ($konsumens as $konsumen)
                                                            <option value="{{$konsumen->id}}" {{ $konsumen->id == old('konsumen') ? 'selected' : '' }}>{{$konsumen->user->nama}}</option>
                                                        @endforeach
                                                    </select>
                                            </label>
                                        </div>
                                </div>
@else
<input type="hidden" name="konsumen" value="konsumen">
@endif

                                <!-- start name -->
                                <div class="j-unit">
                                    <label class="j-label">Jumlah Pemesanan (Yard)</label>
                                    <div class="j-input {{ $errors->has('jumlah') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="jumlah">
                                            <i class="icofont icofont-layers"></i>
                                        </label>
                                        <input type="text" id="jumlah" name="jumlah" value="{{ old('jumlah') }}">
                                        @if ( $errors->has('jumlah') )
                                            <span class="j-error-view">{{$errors->first('jumlah')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->

                                <div class="j-unit">
                                    <label class="j-label">Design</label>
                                    <div class="j-input j-append-small-btn {{ $errors->has('design') ? ' j-error-view' : '' }}">
                                        <div class="j-file-button">
                                            Browse
                                            <input type="file" id="file2" name="design" onchange="document.getElementById('file2_input').value = this.value;">
                                        </div>
                                        <input type="text" id="file2_input" readonly="" placeholder="Upload Gambar">
                                        @if ( $errors->has('design') )
                                            <span class="j-error-view">{{$errors->first('design')}}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <!-- end /.content -->
                            <div class="footer">
                                <button type="submit" class="btn btn-primary m-b-0">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- clone elements card end -->
        </div>
    </div>
</div>

@endsection
