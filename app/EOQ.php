<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EOQ extends Model
{
    protected $table = 'eoq';

    public $timestamps = false;

    public function bahanBaku()
    {
    	return $this->belongsTo('App\BahanBaku', 'kode_bahan_baku');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'kode_supplier');
    }

    public function pengadaan()
    {
    	return $this->belongsTo('App\Pengadaan');
    }

}
