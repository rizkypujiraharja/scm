@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Data Produksi</h4>
                    <span>Pesanan : {{ $pesanan->id }} ({{ $pesanan->produk->nama }})</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('produksi.index') }}">Produksi</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        @if($level == 'produksi')
        <div class="card-header">
            <a href="{{ route('produksi.create', request()->id) }}"><button class="btn btn-sm btn-primary">Tambah Produksi</button></a>
        </div>
        @endif
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tanggal</th>
                            <th>Jumlah (Yard)</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                                <td></td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nopol]" value="{{ $param['nopol'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kapasitas]" value="{{ $param['kapasitas'] ?? ''}}">
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('pesanan.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr> --}}
                    @foreach ($produksis as $produksi)
                        <tr>
                            <td>{{ $produksi->id }}</td>
                            <td>{{ date('d-m-Y', strtotime($produksi->tanggal)) }}</td>
                            <td>{{ $produksi->jumlah }}</td>
                            <td><span class="label label-{{$status[$produksi->status][1]}}">{{$status[$produksi->status][0]}}</span></td>
                            <td>
                                <a href="{{ route('produksi.detail', $produksi) }}">
                                    <button class="btn btn-mini btn-primary">Detail</button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $produksis->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus pesanan bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
