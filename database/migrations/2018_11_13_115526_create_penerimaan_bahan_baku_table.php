<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenerimaanBahanBakuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penerimaan_bahan_baku', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eoq_id')->unsigned();
            $table->string('kode_supplier', 20);
            $table->float('jumlah', 12, 3);
            $table->date('tanggal');
            $table->integer('status')->default(0);

            $table->foreign('eoq_id')->references('id')->on('eoq');
            $table->foreign('kode_supplier')->references('kode_supplier')->on('supplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerimaan_bahan_baku');
    }
}
