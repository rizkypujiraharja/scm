@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-user bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Data Pegawai</h4>
                    <span>Tambah Data Pegawai, Ubah Data Pegawai dan Hapus Data Pegawai</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pegawai.index') }}">Pegawai</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-header">
            <a href="{{ route('pegawai.create') }}"><button class="btn btn-sm btn-primary">Tambah Pegawai</button></a>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Kontak</th>
                            <th>Jabatan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nip]" value="{{ $param['nip'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nama]" value="{{ $param['nama'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[email]" value="{{ $param['email'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kontak]" value="{{ $param['kontak'] ?? '' }}">
                            </td>
                            <td>
                                <select class="form-control" name="filter[jabatan]">
                                    <option value="">Semua</option>
                                    @foreach ($jabatans as $jabatan)
                                        <option value="{{$jabatan[0]}}" {{ !empty($param['jabatan']) ? ($jabatan[0] == $param['jabatan'] ? 'selected' : '') : '' }}>{{$jabatan[1]}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('pegawai.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr>
                    @foreach ($pegawais as $pegawai)
                        <tr>
                            <th>{{ $pegawai->nip == 0 ? '-' : $pegawai->nip }}</th>
                            <td>{{ $pegawai->nama }}</td>
                            <td>{{ $pegawai->email }}</td>
                            <td>{{ $pegawai->kontak }}</td>
                            <td>{{ $pegawai->getJabatan() }}</td>
                            <td>
                                <a href="{{ route('pegawai.edit', $pegawai->id) }}">
                                    <button class="btn btn-mini btn-warning">Edit</button>
                                </a>
                                <button id="delete" data-title="{{$pegawai->nama}}" href="{{ route('pegawai.destroy', $pegawai) }}" class="btn btn-mini btn-danger">Hapus</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div style="padding: 15px;width: 100%">
                <center>{{ $pegawais->appends(request()->except('page'))->links() }}</center>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus pegawai bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
