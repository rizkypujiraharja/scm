<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Pesanan</title>
  <style>
    .header {
      line-height: 3px
    }

    body {
      font-family: "Times New Roman", Times, serif;
      line-height: 25px;
    }

    table {
      border-collapse: collapse;
    }

    .table1 th, .table1 tr, .table1 td {
      border: 1px solid black;
      padding: 3px 0 3px 10px;
    }
  </style>
</head>
<body>
      <div class="header">
        <img src="{{ asset('images/logo.png') }}" style="width: 70px;float: left;padding: 10px;">
        <h4>PT. Tata Cakra Investama</h4>
        <span>Jl. Raya Bandung – Garut Km.28 - Cicalengka 40395 Bandung Indonesia </span><br><br><br><br><br><br><br>
        <span style="font-size:10;"><i><b>Telp (022) 779-6567 ( hunting ) Fax (022) 779-8068 Mail pt.tata.cakra.investama@gmail.com</b></i></span>
      </div>
      <hr>
      <table class="table0">
        <tr valign="top">
            <td width="450px">
                {{ $pesanan->konsumen->user->nama }}
                <br>{{ $pesanan->konsumen->user->alamat }}
                <br>Telepon : {{ $pesanan->konsumen->user->kontak }}
            </td>
            <td>
              Kode Pesanan
              <br> Tanggal
            </td>
            <td>
              : {{$pesanan->id}}
              <br>: {{ $pesanan->tgl_pesan }}
            </td>
        </tr>
      </table>
<center><h3> PROFORMA INVOICE </h3></center>
<p>Dear Sir</p>
<p>We are pleased to have a Proforma Invoice  for the supply of following articles to you  based on terms and conditions her in after stated as follows
</p>

<b><i style="font-size:12px">1 DESCRIPTION AND OFFER ETC</i></b>
      <table class="table1" style="margin-left:8px">
          <tr>
              <th width="300px" align="center">Item</th>
              <th width="90px" align="center">QTY</th>
              <th width="90px" align="center">Price</th>
              <th width="150px" align="center">Total Harga</th>
          </tr>
          <tr valign="top">
              <td height="100px">
                  {{ $pesanan->produk->nama }}

                @if ( $pesanan->jenis == 'bbkonsumen')
                  <br><br>
                  Bahan Baku Dari Konsumen
                  @foreach ($boms as $bom)
                    <br>- {{ $bom->bahanBaku->nama }}
                  @endforeach
                @endif
              </td>
              <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
              <td>Rp. {{ rupiah($pesanan->produk->harga) }}</td>
              <td>Rp. {{ rupiah($pesanan->jumlah * $pesanan->produk->harga) }}
                @if ( $pesanan->jenis == 'bbkonsumen')
                  <br><br>
                  - Rp. {{ rupiah($pesanan->minus_harga_bb_konsumen) }}
                @endif
              </td>
          </tr>

          <tr>
              <td><b>Total</b></td>
              <td align="center"><b>{{ $pesanan->jumlah }}</b></td>
              <td></td>
              <td><b>Rp. {{ rupiah(($pesanan->jumlah * $pesanan->produk->harga) - $pesanan->minus_harga_bb_konsumen) }}</b></td>
          </tr>
      </table>
<br>

<b><i style="font-size:12px">2 GENERAL TERMS AND CONDITIONS</i></b>
<table class="table0" style="margin-left:8px; font-size:14px;">
  <tr>
    <td width="130px">Origin</td>
    <td>Made in Indonesia</td>
  </tr>
  <tr>
    <td>Packing</td>
    <td>Loose packing</td>
  </tr>
  <tr>
    <td>Delivery</td>
    <td>{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
  </tr>
  <tr valign="top">
    <td>Payment</td>
    <td>
      HADI SEBASTIAN W
      <br>BANK PERMATA KOPO PERMAI BANDUNG
      <br>AC 400.185.2081
    </td>
  </tr>
  <tr>
    <td>Quality</td>
    <td>Export Standard quality</td>
  </tr>
</table>
<br>We will appreciate if you return 1 copy of this Contract after it is signed by you at below<br><br>
<table class="table0">
  <tr>
    <td width="500px">
      Very truly yours
      <br>
      <br>
      <br>
      <b>PT. TATA CAKRA INVESTAMA</b>
    </td>
    <td>
      Agreed by
      <br>
      <br>
      <br>
      <b> {{ strtoupper($pesanan->konsumen->user->nama) }} </b>
      </td>
  </tr>
</table>
</body>
</html>
