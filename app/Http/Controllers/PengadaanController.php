<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Pengadaan, Pesanan, BOM, EOQ, BahanBaku, BahanBakuSupplier};
use Exception, DB;

use Carbon\Carbon;

class PengadaanController extends Controller
{
    public function index()
    {
    	$pengadaans = Pengadaan::orderBy('id', 'DESC')->paginate(10);
        $status = Pengadaan::status;
        return view('pengadaan.index', compact('pengadaans', 'status'));
    }

    public function indexPesanan()
    {
        $pesanans = Pesanan::where('status', 1)->get();

        return view('pengadaan.index-pesanan', compact('pesanans'));
    }

    public function create(Request $request)
    {
        foreach ($request->pesanan as $pesanan) {

            $pesanans = Pesanan::where('status', 1)->findOrFail($pesanan);

            $boms = BOM::where('kode_produk', $pesanans->kode_produk)
                    ->get();

            foreach ($boms as $bom) {
                $kebutuhans[$bom->kode_bahan_baku] = $kebutuhans[$bom->kode_bahan_baku] ?? 0;
                $kebutuhans[$bom->kode_bahan_baku] = $kebutuhans[$bom->kode_bahan_baku] + ($pesanans->jumlah * $bom->jumlah);
            }

        }
        foreach ($kebutuhans as $key => $value) {
            $kodebb[] = $key;
        }

        $pesanan = json_encode($request->pesanan);
        $boms = BahanBaku::whereIn('kode_bahan_baku', $kodebb)->get();
    	return view('pengadaan.create', compact('pesanan', 'kebutuhans', 'boms'));
    }

    public function store(Request $request)
    {
    	DB::beginTransaction();
        try {

            $kode = date('ymd');
            $cek = Pengadaan::where('id', 'like', $kode.'%')->orderBy('id', 'DESC')->first();

            if( is_null($cek) ){
                $kode = $kode.'-01';
            }else{
                $last = explode('-', $cek->id);
                $new = sprintf("%'02d", $last[1]+1);
                $kode = $kode.'-'.$new;
            }

        Pesanan::whereIn('id', json_decode($request->pesanan, true))->update(['status' => 4]);

        	$pengadaan = New Pengadaan;
            $pengadaan->id = $kode;
        	$pengadaan->pesanan = $request->pesanan;
        	$pengadaan->tanggal = date('Y-m-d');
            $pengadaan->status = 0;
        	$pengadaan->save();
            foreach ($request->jumlah as $key => $value) {
            	$bbs = BahanBakuSupplier::findOrFail($request->bbs[$key]);
            	$bb = $bbs->bahanBaku->stock_sisa;
            	if($bbs->stock < $request->jumlah[$key] ){
            		return redirect()->back()->withInput()->with('alert-error', 'Stock Tidak Cukup');
            	}


                if($bbs->min_order > $request->jumlah[$key] ){
                    return redirect()->back()->withInput()->with('alert-error', 'Harus melebihi minimal order!');
                }

            	$kebutuhan = $request->jumlah[$key] - $bb;
            	$biayaSimpan = 0.05 * $bbs->harga;
            	$jumlah = ceil( sqrt( (2 * $kebutuhan * 500000) / $biayaSimpan))  + ($request->jumlah[$key] * 10/100);
				$waktu_antar = floor(($jumlah/$kebutuhan) * 30);
				$frekuensi = ceil($kebutuhan / $jumlah);

                if($frekuensi == 1)
                    $catatan = "Pengiriman Pertama Max ".Carbon::now()->addDays(2)->toDateString();
                else{
                    $tgl = [];
                    $now = Carbon::now();
                    for ($i=0; $i < $frekuensi ; $i++) {
                        $now = $now->addDays($waktu_antar);
                        $tgl[] = $now->toDateString();
                    }
                    $t = $tgl;
                    $catatan = "Pengiriman Pertama Max ".date('d-m-Y', strtotime(Carbon::now()->addDays(2)->toDateString()));
                    array_shift($t);
                    foreach($t as $i => $date)
                    {
                        $catatan .= '<br/>Pengiriman ke-'.($i+2).' : '. date('d-m-Y', strtotime($date));
                    }
                }
            	$eoq = New EOQ;
            	$eoq->pengadaan_id = $pengadaan->id;
                $eoq->kode_bahan_baku = $bbs->kode_bahan_baku;
                $eoq->kode_supplier = $bbs->kode_supplier;
                $eoq->sisa_stock = $bb;
                $eoq->harga = $bbs->harga;
                $eoq->kebutuhan = $request->jumlah[$key];
                $eoq->min_order = $bbs->min_order;
            	$eoq->jumlah = ceil($jumlah);
            	$eoq->waktu_antar = $waktu_antar;
            	$eoq->frekuensi = $frekuensi;
            	$eoq->dikirim = 0;
                $eoq->catatan = $catatan;
            	$eoq->save();

                $bbs->bahanBaku->stock_sisa = 0;
                $bbs->save();
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());
        }


            return redirect()->route('pengadaan.index')->with('alert-successs', 'Berhasil menambah pengadaan');
    }

    public function detail($id)
    {
        $pengadaan = Pengadaan::findOrFail($id);

        $status = Pengadaan::status;

        return view('pengadaan.view', compact('pengadaan', 'status'));
    }

    public function setujui($id)
    {
        $pengadaan = Pengadaan::findOrFail($id);

        $pengadaan->status = 1;
        $pengadaan->save();


        Pesanan::whereIn('id', json_decode($pengadaan->pesanan, true))->update(['status' => 5]);

        return redirect()->back()->with('alert-success', 'Pengadaan #'.$id.' disetujui!');
    }

    public function tolak($id)
    {


        $pengadaan = Pengadaan::findOrFail($id);
        $pengadaan->status = 2;
        $pengadaan->save();

        Pesanan::whereIn('id', json_decode($pengadaan->pesanan, true))->update(['status' => 1]);

        return redirect()->back()->with('alert-success', 'Pengadaan #'.$id.' ditolak!');
    }
}
