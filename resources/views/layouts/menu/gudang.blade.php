

                                <li class="pcoded-hasmenu {{ Request::is('bahan-baku*') ? 'active' : '' }}" dropdown-icon="style1" subitem-icon="style6">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-shopping-cart-full"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Data Bahan Baku</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="{{ route('bahan-baku.index') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Gudang</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ route('penerimaan.index') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">ACC Pengadaan</span>
                                        @if($jml['penerimaanBB'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['penerimaanBB']}}</span>
                                        @endif
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="{{ Request::is('produksi*') ? 'active' : '' }}">
                                    <a href="{{ route('produksi.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Produksi</span>
                                        @if($jml['produksi_permintaanBB'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['produksi_permintaanBB']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>