@extends('layouts.app')

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container">
        <!-- Main content starts -->
        <div>
            <!-- Invoice card start -->
            <div class="card">
                <div class="row invoice-contact">
                    <div class="col-md-12">
                        <div class="invoice-box row">
                            <div class="col-sm-12">
                                <table class="table table-responsive invoice-table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img src="{{ asset('images/logo.png') }}" class="m-b-10" alt="PT. Tata Cakra Investama" style="width: 100px;float: left;padding: 15px">
                                                <h5>PT. Tata Cakra Investama</h5>
                                                <p>Jl. Raya Bandung – Garut Km.28
                                                    <br>Cicalengka 40395 Bandung Indonesia
                                                    <br><b><i>Telp (022) 779-6567 ( hunting ) Fax (022) 779-8068 Mail pt.tata.cakra.investama@gmail.com</i></b>
                                                </p>
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                            <td><a href="mailto:tatacakraInvestama@gmail.com" target="_top">mailto:tatacakraInvestama@gmail.com</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>06464132354</td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row invoive-info">
                        <div class="col-md-4 col-sm-6">
                            <h6>Tanggal Produksi : {{ $produksi->tanggal }}</h6>
                            <span class="label label-{{$status[$produksi->status][1]}}">{{$status[$produksi->status][0]}}</span>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h6 class="m-b-20">ID Pesanan <span>#{{$produksi->pesanan_id}}</span></h6>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h6 class="m-b-20">Jumlah Produksi <span>{{$produksi->jumlah}}</span></h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table  invoice-detail-table">
                                    <thead>
                                        <tr class="thead-default">
                                            <th>Bahan Baku</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($permintaans as $permintaan)
                                            <tr>
                                                <td>{{ $permintaan->bahanBaku->nama }}</td>
                                                <td>{{$permintaan->jumlah}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row">
                        <div class="col-sm-12">
                            <h6>Terms And Condition :</h6>
                            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
                        </div>
                    </div> --}}
                </div>
            </div>
            <!-- Invoice card end -->
            <div class="row text-center">
                <div class="col-sm-12 invoice-btn-group text-center">
                        <a href="{{ route('produksi.index-pesanan', $produksi->pesanan_id) }}">
                            <button class="btn btn-inverse">Kembali</button>
                        </a>
                    @if ($produksi->status == 0 && $level == 'gudang')
                        <a href="{{ route('produksi.setujui', $produksi->id) }}">
                            <button class="btn btn-primary">Setujui</button>
                        </a>
                    @endif
                    @if ($produksi->status == 2 && $level == 'produksi')
                        <a href="{{ route('produksi.selesai', $produksi->id) }}">
                            <button class="btn btn-primary">Selesai</button>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Container ends -->
</div>
@endsection
