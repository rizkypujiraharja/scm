<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->integer('pegawai_id')->unsigned();
            $table->integer('konsumen_id')->unsigned();
            $table->string('kode_produk', 20);
            $table->integer('jumlah')->unsigned();
            $table->bigInteger('total_harga')->nullable();
            $table->string('jenis');
            $table->integer('status')->default(0)->unsigned();

            $table->date('tgl_pesan');
            $table->date('tgl_selesai')->nullable();
            $table->bigInteger('minus_harga_bb_konsumen')->default(0)->unsigned();

            $table->foreign('kode_produk')->references('kode_produk')->on('produk');
            $table->foreign('pegawai_id')->references('id')->on('pegawai');
            $table->foreign('konsumen_id')->references('id')->on('konsumen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan');
    }
}
