


                                <li class="{{ Request::is('bahan-baku*') ? 'active' : '' }}">
                                    <a href="{{ route('bahan-baku-supplier.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Bahan Baku</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('penjualan') ? 'active' : '' }}">
                                    <a href="{{ route('supplier.penjualan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Penjualan Bahan Baku</span>
                                        @if($jml['penjualan_bb'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['penjualan_bb']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('penjualan/pengiriman*') ? 'active' : '' }}">
                                    <a href="{{ route('supplier.pengiriman') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">History Pengiriman</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>