
                                 <li class="{{ Request::is('pesanan/*') ? 'active' : '' }}">
                                    <a href="{{ route('pesanan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Pemesanan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                 <li class="{{ Request::is('pesanan/design-baru*') ? 'active' : '' }}">
                                    <a href="{{ route('pesanan.design.baru.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>D</b></span>
                                        <span class="pcoded-mtext">Design Baru</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('produksi*') ? 'active' : '' }}">
                                    <a href="{{ route('produksi.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Produksi</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>


                                <li class="{{ Request::is('pengiriman*') ? 'active' : '' }}">
                                    <a href="{{ route('pengiriman.index') }}">
                                        <span class="pcoded-micon"><i class="ti-car"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Pengiriman</span>
                                        @if($jml['penerimaanKonsumen'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['penerimaanKonsumen']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
