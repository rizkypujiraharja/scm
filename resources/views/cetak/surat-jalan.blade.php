<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Surat Jalan {{ $pengiriman->kode_pengiriman }}</title>
<style>
  .header {
    line-height: 3px
  }

  body {
    font-family: "Times New Roman", Times, serif;
    line-height: 25px;
  }

  table {
    border-collapse: collapse;
  }

  .table1 th, .table1 tr, .table1 td {
    border: 1px solid black;
    padding: 3px;
  }
</style>
</head>
<body>
    <center><span style="font-size:17px"><b>SURAT JALAN</span></b></center>

    <div class="header">
      <img src="{{ asset('images/logo.png') }}" style="width: 70px;float: left;padding: 10px;">
      <h4>PT. Tata Cakra Investama</h4>
      <span>Jl. Raya Bandung – Garut Km.28 - Cicalengka 40395 Bandung Indonesia </span><br><br><br><br><br><br><br>
      <span style="font-size:10;"><i><b>Telp (022) 779-6567 ( hunting ) Fax (022) 779-8068 Mail pt.tata.cakra.investama@gmail.com</b></i></span>
    </div>
      <hr>

      <table class="table0">
        <tr valign="top">
            <td width="60px">
              Nomor
              <br>Kepada
            </td>
            <td width="400px">
                : {{ $pengiriman->kode_pengiriman }}
                <br>: {{ $pengiriman->pesanan->konsumen->user->nama }}
                <br>&nbsp;&nbsp;{{ $pengiriman->pesanan->konsumen->user->alamat }}
            </td>
            <td>
              Tanggal
              <br> No Kendaraan
            </td>
            <td>
              : {{$pengiriman->tanggal_kirim}}
              <br>: {{ $pengiriman->kendaraan->nopol }}
            </td>
        </tr>
      </table>
<p>Bersama ini kami mengirim bak barang berikut dibawah ini :</p>
      <table class="table1">
        <tr>
          <th width="120px" align="center">Kode</th>
          <th width="150px" align="center">Nama Barang</th>
          <th width="130px" align="center">Jumlah (Yard)</th>
          <th width="130px" align="center">Jumlah (Psc)</th>
          <th width="70px" align="center">Harga</th>
          <th width="70px" align="center">Total Harga</th>
        </tr>
        <tr valign="top">
          <td  height="60px">{{ $pengiriman->pesanan->id }}</td>
          <td>{{ $pengiriman->pesanan->produk->nama }}</td>
          <td align="center">{{ rupiah($pengiriman->jumlah) }}</td>
          <td align="center">{{ $pengiriman->jumlah / 100 }}</td>
          <td align="center">{{ ($pengiriman->pesanan->total_harga / $pengiriman->pesanan->jumlah)}}</td>
          <td align="center">{{ ($pengiriman->pesanan->total_harga / $pengiriman->pesanan->jumlah) * $pengiriman->jumlah}}</td>
        </tr>
      </table>
<br>
      <table class="table0">
          <tr valign="top">
              <td align="center" width="270">Tanda Terima</td>
              <td align="center" width="250">Hormat Kami</td>
          </tr>
        </table>

</body>
</html>
