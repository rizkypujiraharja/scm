<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahanBakuSupplier extends Model
{
    protected $table = 'bahan_baku_supplier';

    public $timestamps = false;

    protected $guarded = [];

    public function supplier()
    {
    	return $this->belongsTo('App\Supplier', 'kode_supplier');
    }

    public function bahanBaku()
    {
    	return $this->belongsTo('App\BahanBaku', 'kode_bahan_baku');
    }
}
