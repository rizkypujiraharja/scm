<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\{User, Pegawai};
use Image;
use Exception, DB;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $jabatans = Pegawai::data_jabatan;

        if(!empty($request->filter['jabatan'])){
            $jabatan[] = $request->filter['jabatan'];
        }else{
            foreach ($jabatans as $jab) {
                $jabatan[] = $jab[0];
            }
        }

        $param = $request->filter;
        
    	$pegawais = Pegawai::join('users', 'pegawai.user_id', '=', 'users.id')
                ->select('pegawai.nip', 'pegawai.id', 'pegawai.jabatan', 'users.nama', 'users.email', 'users.kontak')
    			->where('pegawai.nip', 'like', '%' . ($request->filter['nip'] ?? '') . '%')
    			->where('users.nama', 'like', '%' . ($request->filter['nama'] ?? '' ). '%')
    			->where('users.email', 'like', '%' . ($request->filter['email'] ?? ''). '%')
    			->where('users.kontak', 'like', '%' . ($request->filter['kontak'] ?? '' ). '%')
    			->whereIn('pegawai.jabatan', $jabatan)
                ->paginate(10);

        return view('pegawai.index', compact('pegawais', 'param', 'jabatans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jabatans = Pegawai::data_jabatan;

        return view('pegawai.create', compact('jabatans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:50|alpha',
            'email' => 'email|unique:users,email',
            'password' => 'required|string|min:6',
            'kontak' => 'nullable|numeric',
            'foto'  => 'nullable|image',
            'nip'   => 'required|numeric|digits:8',
            'alamat' => 'nullable|string|max:200'
        ]);

        $imgname = Null;
        if( !is_null($request->foto) ){
            $imageLocation = public_path('images/foto').'/';

            $imgname = 'pegawai-'.time().'.'.$request->foto->getClientOriginalExtension();
            Image::make($request->foto)->resize(300, 300)
                    ->save($imageLocation .$imgname);
        }

        $user = new User;
        $user->nama = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->jenis_kelamin = $request->jeniskelamin;
        $user->alamat = $request->alamat ?? '-';
        $user->kontak = $request->kontak ?? '-';
        $user->foto = $imgname;
        $user->save();

        $pegawai = New Pegawai;
        $pegawai->nip = $request->nip;
        $pegawai->jabatan = $request->jabatan;
        $user->pegawai()->save($pegawai);

        return redirect()->route('pegawai.index')->with('alert-success', 'Berhasil menambah pegawai!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jabatans = Pegawai::data_jabatan;
        $pegawai = Pegawai::findOrFail($id);
        return view('pegawai.edit', compact('pegawai', 'jabatans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pegawai = Pegawai::findOrFail($id);

        $user = User::findOrFail($pegawai->user_id);

        $this->validate($request, [
            'name' => 'required|min:4|max:50|alpha',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            'password' => 'nullable|string|min:6',
            'kontak' => 'nullable|numeric',
            'foto'  => 'nullable|image',
            'nip'   => 'required|numeric',
            'alamat' => 'nullable|string|max:200'
        ]);

        DB::beginTransaction();
        try {
            
            $imgname = Null;

            if( !is_null($request->foto) ){
                $imageLocation = public_path('images/foto').'/';

                $img = $user->foto;
                if(!is_null($img)) !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

                $imgname = 'pegawai-'.time().'.'.$request->foto->getClientOriginalExtension();
                Image::make($request->foto)->resize(300, 300)
                        ->save($imageLocation .$imgname);
            }

            $pegawai->nip = $request->nip;
            $pegawai->jabatan = $request->jabatan;
            $pegawai->save();

            $user->nama = $request->name;
            $user->email = $request->email;
            if(!is_null($request->password))
                $user->password = bcrypt($request->password);
            $user->jenis_kelamin = $request->jeniskelamin;
            $user->alamat = $request->alamat ?? '-';
            $user->kontak = $request->kontak ?? '-';
            $user->foto = $imgname;
            $user->save();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        return redirect()->route('pegawai.index')->with('alert-success', 'Berhasil mengubah pegawai!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::findOrFail($id);

        $user = User::findOrFail($pegawai->user_id);

        $img = $user->foto;

        DB::beginTransaction();
        try {
            
            $pegawai->delete();
            $user->delete();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        $imageLocation = public_path('images/foto').'/';
        if (!is_null($img)) !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

        return redirect()->back()->with('alert-success', 'Berhasil menghapus data pegawai.');
    }
}
