<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Supplier, BahanBakuSupplier, BahanBaku};
use Auth;
use Exception, DB;

class BahanBakuSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = $request->filter;

        if(session('user_level') == 'supplier'){
            $bbsData = BahanBakuSupplier::where('kode_supplier', Auth::user()->supplier->kode_supplier)
            ->orderBy('kode_supplier', 'ASC')->paginate(10);
        }else{
            $bbsData = BahanBakuSupplier::where('kode_supplier', 'like', '%'.$request->filter['supplier'])
            ->orderBy('kode_supplier', 'ASC')->paginate(10);
        }
        

        $suppliers = Supplier::get();

        return view('bahan-baku.supplier.index', compact('bbsData', 'suppliers', 'param'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bahanBakus = BahanBaku::get();
        
        return view('bahan-baku.supplier.create', compact('bahanBakus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bahanBakuSupplier = new BahanBakuSupplier;
        $bahanBakuSupplier->kode_bahan_baku = $request->bb;
        $bahanBakuSupplier->kode_supplier = Auth::user()->supplier->kode_supplier;
        $bahanBakuSupplier->harga = intVal($request->harga);
        $bahanBakuSupplier->min_order = intVal($request->min_order);
        $bahanBakuSupplier->stock = $request->stock;
        $bahanBakuSupplier->save();

        return redirect()->route('bahan-baku-supplier.index')->with('alert-success', 'Berhasil menambah bahan baku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BahanBakuSupplier $bahanBakuSupplier)
    {
        return view('bahan-baku.supplier.edit', compact('bahanBakuSupplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BahanBakuSupplier $bahanBakuSupplier)
    {
        $bahanBakuSupplier->stock = $request->stock;
        $bahanBakuSupplier->harga = $request->harga;
        $bahanBakuSupplier->save();
        return redirect()->route('bahan-baku-supplier.index')->with('alert-success', 'Berhasil menghapus bahan baku!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BahanBakuSupplier $bahanBakuSupplier)
    {
        DB::beginTransaction();
        try {

            $bahanBakuSupplier->delete();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }
        return redirect()->route('bahan-baku-supplier.index')->with('alert-success', 'Berhasil menghapus bahan baku!');
    }
}
