<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <style>
    body{
      line-height: normal;
      font-family: Arial, Helvetica, sans-serif;
    }
    .table {
      border-collapse: collapse;
      font-size: 11px;
    }

    table {
      font-size: 11px;
    }

    .table th, .table td {
      border: 1px solid black;
    }

    .table th {
      vertical-align: middle;
      text-align: center;
      background-color: lightgray;
    }
  </style>
<title>Laporan Pengiriman Bulan {{$times[1]}} {{$times[0]}}</title>
</head>
<body>
<center>LAPORAN PENGIRIMAN PT. TATA CAKRA INVESTAMA
  <br> {{$times[1]}} {{$times[0]}}
</center>
<hr>
<table class="table">
  <tr>
      <th rowspan="2" width="60px">Tanggal<br>Pesan</th>
      <th rowspan="2" width="60px">Tanggal<br>Kirim</th>
      <th rowspan="2" width="95px">Nama<br>Konsumen</th>
      <th rowspan="2" width="95px">Nama<br>Produk</th>
      <th colspan="2">Jumlah</th>
      <th rowspan="2" width="180px">Alamat</th>
      <th rowspan="2" width="70px">Supir</th>
  </tr>
  <tr>
      <th width="60px">PCS</th>
      <th width="60px">Yard</th>
  </tr>

  @php
      $sumJumlah = 0;
  @endphp
  @foreach ($pengirimans as $pengiriman)
      <tr>
          <td>{{date('d-m-Y', strtotime($pengiriman->pesanan->tgl_pesan) )}}</td>
          <td>{{date('d-m-Y', strtotime($pengiriman->tanggal_kirim) )}}</td>
          <td>{{$pengiriman->pesanan->konsumen->user->nama}}</td>
          <td>{{$pengiriman->pesanan->produk->nama}}</td>
          <td>{{rupiah($pengiriman->jumlah/100)}}</td>
          <td>{{rupiah($pengiriman->jumlah)}}</td>
          <td>{{$pengiriman->pesanan->konsumen->user->alamat}}</td>
          <td>{{$pengiriman->kendaraan->supir}}</td>
      </tr>
      @php
          $sumJumlah = $pengiriman->jumlah;
      @endphp
  @endforeach
      <tr>
          <td colspan="4" align="center"><b>Total</b></td>
          <td><b>{{rupiah($sumJumlah/100)}}</b></td>
          <td><b>{{rupiah($sumJumlah)}}</b></td>
          <td colspan="2"></td>
      </tr>
</table>

<br>
<br>
<table border="0">
<tr>
    <td>
        <div style="margin-left:90px">
            <center>
              <br>
          General Manager
          <br><br><br><br>
          <b><u>{{ $manager->user->nama }}</b></u>
            </center>
          </div>
    </td>
    <td>
        <div style="margin-left:330px">
            <center>
          Bandung, {{ date('d-m-Y') }}<br>
          {{ ucwords($level) }}
          <br><br><br><br>
          <b><u>{{ Auth::user()->nama }}</b></u>
            </center>
          </div>
    </td>
</tr>
</table>
</body>
</html>
