<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesananDesignBaru extends Model
{
    protected $table = 'pesanan_design_baru';
	
	public $timestamps = false;

	const status = [
			['Menunggu Persetujuan', 'warning'],
			['Disetujui', 'success'],
			['Tidak Disetujui', 'danger'],
    	];

	public function konsumen()
	{
		return $this->belongsTo('App\Konsumen', 'konsumen_id');
	}

	public function produk()
	{
		return $this->belongsTo('App\Produk', 'kode_produk');
	}

	public function getDesign()
    {
        return url('/images/design').'/'.$this->design;
    }
}
