
                                <li class="{{ Request::is('pegawai/*') ? 'active' : '' }}">
                                    <a href="{{ route('pegawai.index') }}">
                                        <span class="pcoded-micon"><i class="ti-user"></i><b>P</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.packages.main">Data Pegawai</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('konsumen/*') ? 'active' : '' }}">
                                    <a href="{{ route('konsumen.index') }}">
                                        <span class="pcoded-micon"><i class="ti-user"></i><b>K</b></span>
                                        <span class="pcoded-mtext">Data Konsumen</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('produk/*') ? 'active' : '' }}">
                                    <a href="{{ route('produk.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Data Produk</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('supplier/*') ? 'active' : '' }}">
                                    <a href="{{ route('supplier.index') }}">
                                        <span class="pcoded-micon"><i class="ti-user"></i><b>S</b></span>
                                        <span class="pcoded-mtext">Data Supplier</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('kendaraan/*') ? 'active' : '' }}">
                                    <a href="{{ route('kendaraan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-car"></i><b>K</b></span>
                                        <span class="pcoded-mtext">Data Kendaraan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="pcoded-hasmenu {{ Request::is('bahan-baku*') ? 'active' : '' }}" dropdown-icon="style1" subitem-icon="style6">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-shopping-cart-full"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Data Bahan Baku</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="{{ route('bahan-baku.index') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Gudang</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ route('bahan-baku-supplier.index') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Supplier</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ route('penerimaan.index') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Penerimaan</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="{{ Request::is('bom/*') ? 'active' : '' }}">
                                    <a href="{{ route('bom.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>B</b></span>
                                        <span class="pcoded-mtext">Data BOM</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                 <li class="{{ Request::is('pesanan/*') ? 'active' : '' }}">
                                    <a href="{{ route('pesanan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Pemesanan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                 <li class="{{ Request::is('pesanan/design-baru*') ? 'active' : '' }}">
                                    <a href="{{ route('pesanan.design.baru.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>D</b></span>
                                        <span class="pcoded-mtext">Design Baru</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('pengadaan/*') ? 'active' : '' }}">
                                    <a href="{{ route('pengadaan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Pengadaan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('bahan-baku*') ? 'active' : '' }}">
                                    <a href="{{ route('bahan-baku-supplier.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Bahan Baku</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('penjualan') ? 'active' : '' }}">
                                    <a href="{{ route('supplier.penjualan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Penjualan Bahan Baku</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('penjualan/pengiriman*') ? 'active' : '' }}">
                                    <a href="{{ route('supplier.pengiriman') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">History Pengiriman</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('produksi*') ? 'active' : '' }}">
                                    <a href="{{ route('produksi.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Produksi</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="pcoded-hasmenu {{ Request::is('laporan*') ? 'active' : '' }}" dropdown-icon="style1" subitem-icon="style6">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-file"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Laporan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="{{ route('laporan.pesanan') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Laporan Pemesanan</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ route('laporan.pengadaan') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Laporan Pengadaan</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ route('laporan.pengiriman') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Laporan Pengiriman</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
