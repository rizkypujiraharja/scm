<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\User;
use Auth, Image;


class ProfileController extends Controller
{
    public function form()
    {
        $user = Auth::user();
    	return view('profile', compact('user'));
    }

    public function update(Request $request)
    {
    	$user = Auth::user();

    	$this->validate($request, [
            'name' => 'required|min:4|max:50|alpha',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            'password' => 'nullable|string|min:6',
            'kontak' => 'nullable',
            'foto'  => 'nullable|image',
            'alamat' => 'nullable|string|max:200'
        ]);

    	$user->nama = $request->name;
        $user->email = $request->email;
        if(!is_null($request->password))
            $user->password = bcrypt($request->password);
        $user->jenis_kelamin = $request->jeniskelamin;
        $user->alamat = $request->alamat ?? '-';
        $user->kontak = $request->kontak ?? '-';

        if( !is_null($request->foto) ){
            $imageLocation = public_path('images/foto').'/';

            $img = $user->foto;
            if(!is_null($img)) !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

            $imgname = 'pegawai-'.time().'.'.$request->foto->getClientOriginalExtension();
            Image::make($request->foto)->resize(300, 300)
                    ->save($imageLocation .$imgname);

        	$user->foto = $imgname;
        }

        $user->save();

        return redirect()->back()->with('alert-success', 'Berhasil mengubah data profile');
    }
}
