@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Data Pengadaan</h4>
                    <span>Manage Data Pengadaan</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pengadaan.index') }}">Pengadaan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>ID Pesanan</th>
                            <th>Konsumen</th>
                            <th>Produk</th>
                            <th>Jenis</th>
                        </tr>
                    </thead>
                    <tbody>
                    <form action="{{ route('pengadaan.create') }}" method="get">
                    @foreach ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $pesanan->tgl_pesan }}</td>
                            <td>{{ $pesanan->id }}</td>
                            <td>{{ $pesanan->konsumen->user->nama }}</td>
                            <td>{{ $pesanan->produk->nama }}</td>
                            <td>{{ $pesanan->jenis }}</td>
                        </tr>
                        <tr style="display: none">
                            <td><input type="checkbox" name="pesanan[]" value="{{$pesanan->id}}" checked></td>
                        </tr>
                    @endforeach

                @if( count($pesanans) > 0 )
                    <tr>
                        <td colspan="4">
                        </td>
                        <td>
                            <button type="submit" class="btn btn btn-success">+ Pengadaan</button>
                        </td>
                    </tr>
                @endif
                    </form>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

