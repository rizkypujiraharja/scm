<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BOM extends Model
{
    protected $table = 'bom';

    public $timestamps = false;

    public function produk()
    {
    	return $this->belongsTo('App\Produk', 'kode_produk');
    }

    public function bahanBaku()
    {
    	return $this->belongsTo('App\BahanBaku', 'kode_bahan_baku');
    }
}
