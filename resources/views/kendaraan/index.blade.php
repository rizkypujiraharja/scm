@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Data Kendaraan</h4>
                    <span>Tambah Data Kendaraan, Ubah Data Kendaraan dan Hapus Data Kendaraan</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('kendaraan.index') }}">Kendaraan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-header">
            <a href="{{ route('kendaraan.create') }}"><button class="btn btn-sm btn-primary">Tambah Kendaraan</button></a>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No Pol</th>
                            <th>Kapasitas (PCS)</th>
                            <th>Supir</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nopol]" value="{{ $param['nopol'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kapasitas]" value="{{ $param['kapasitas'] ?? ''}}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[supir]" value="{{ $param['supir'] ?? ''}}">
                            </td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('kendaraan.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr>
                    @foreach ($kendaraans as $kendaraan)
                        <tr>
                            <td>{{ $kendaraan->nopol }}</td>
                            <td>{{ $kendaraan->kapasitas }}</td>
                            <td>{{ $kendaraan->supir }}</td>
                            <td>{{ $kendaraan->status == 0 ? 'Tidak Tersedia' : 'Tersedia' }}</td>
                            <td>
                              <a href="{{ route('kendaraan.edit', $kendaraan) }}">
                                  <button class="btn btn-mini btn-warning">Edit</button>
                              </a>
                              <a href="{{ route('kendaraan.switch', $kendaraan) }}">
                                  <button class="btn btn-mini btn-primary">Ubah Status</button>
                              </a>
                                <button id="delete" data-title="{{$kendaraan->nama}}" href="{{ route('kendaraan.destroy', $kendaraan) }}" class="btn btn-mini btn-danger">Hapus</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $kendaraans->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus kendaraan bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
