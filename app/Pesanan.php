<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Pesanan extends Model
{
    protected $table = 'pesanan';

	protected $primaryKey = 'id';

    public $incrementing = false;

	public $timestamps = false;

	const status = [
			['Menunggu Persetujuan', 'warning'],
			['Menunggu Pengadaan', 'info'],
			['Selesai', 'success'],
			['Tidak Disetujui', 'danger'],
			['Sudah Pengadaan', 'info'],
            ['Produksi', 'info'],
            ['Selesai Produksi', 'primary']
    	];

	public function konsumen()
	{
		return $this->belongsTo('App\Konsumen', 'konsumen_id');
	}

	public function produk()
	{
		return $this->belongsTo('App\Produk', 'kode_produk');
	}

	public function pengadaan()
	{
		return $this->hasMany('App\Pengadaan');
	}

	public function produksi()
	{
		return $this->hasMany('App\Produksi');
    }

    public function jmlProduksi() {
        return $this->produksi()->where('status', 1)->sum('jumlah');
    }

    public function dikirim() {
        return $this->hasMany('App\Pengiriman')->where('status', '>', 0);
    }

    public function jmlDikirim() {
        return $this->dikirim()->sum('jumlah');
    }

    public function requestPengiriman() {
        return $this->hasMany('App\Pengiriman')->where('status', 0);
    }

    public function jmlRequestPengiriman() {
        return $this->requestPengiriman()->sum('jumlah');
    }

	public function bisaPengadaan()
	{
		return $this->select('pesanan.*')
					->leftJoin('pengadaan', 'pengadaan.pesanan_id', 'pesanan.id')
					->where(function($w) {
						$w->whereNull('pengadaan.id')
							->orWhere(function($w2) {
								$w2->whereNotNull('pengadaan.id')
									->where('pengadaan.status', 2);
							});
					})->orderBy('pesanan.id', 'DESC');
	}
}
