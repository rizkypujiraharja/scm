@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Data Produk</h4>
                    <span>Tambah Data Produk, Ubah Data Produk dan Hapus Data Produk</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('produk.index') }}">Produk</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-header">
            <a href="{{ route('produk.create') }}"><button class="btn btn-sm btn-primary">Tambah Produk</button></a>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Kode Produk</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Mak Produksi (Yard/Hari)</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kode]" value="{{ $param['kode'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nama]" value="{{ $param['nama'] ?? '' }}">
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('produk.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr>
                    @foreach ($produks as $produk)
                        <tr>
                            <td>{{ $produk->kode_produk }}</td>
                            <td>{{ $produk->nama }}</td>
                            <td>{{ rupiah($produk->harga) }}</td>
                            <td>{{ $produk->max_produksi_per_hari }}</td>
                            <td>
                                <a href="{{ route('produk.edit', $produk) }}">
                                    <button class="btn btn-mini btn-warning">Edit</button>
                                </a>
                                <button id="delete" data-title="{{$produk->nama}}" href="{{ route('produk.destroy', $produk) }}" class="btn btn-mini btn-danger">Hapus</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>
                        @if(count($produks) > 0)
                        {{ $produks->appends(request()->except('page'))->links() }}
                        @endif
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus produk bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
