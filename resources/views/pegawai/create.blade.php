@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-user bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Data Pegawai</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pegawai.index') }}">Pegawai</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Pegawai</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="{{ route('pegawai.store') }}" method="post" class="j-pro" id="j-pro" novalidate="" enctype="multipart/form-data">
                            @csrf
                            <div class="j-content">
                                <!-- start name -->
                                <div class="j-unit">
                                    <label class="j-label">Nama Lengkap</label>
                                    <div class="j-input {{ $errors->has('name') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="name">
                                            <i class="icofont icofont-ui-user"></i>
                                        </label>
                                        <input type="text" id="name" name="name" value="{{ old('name') }}">
                                        @if ( $errors->has('name') )
                                            <span class="j-error-view">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->

                                <!-- start email phone -->
                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Email</label>
                                        <div class="j-input {{ $errors->has('email') ? ' j-error-view' : '' }}">
                                            <label class="j-icon-right" for="email">
                                                <i class="icofont icofont-envelope"></i>
                                            </label>
                                            <input type="email" id="email" name="email" value="{{ old('email') }}">
                                        @if ( $errors->has('email') )
                                            <span class="j-error-view">{{$errors->first('email')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Password</label>
                                        <div class="j-input {{ $errors->has('password') ? ' j-error-view' : '' }}"">
                                            <label class="j-icon-right" for="password">
                                                <i class="icofont icofont-key"></i>
                                            </label>
                                            <input type="password" id="password" name="password">
                                        @if ( $errors->has('password') )
                                            <span class="j-error-view">{{$errors->first('password')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- end email phone -->
                                <div class="j-divider j-gap-bottom-25"></div>
                                <!-- start guests -->
                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Jabatan</label>
                                        <div class="j-input ">
                                            <label class="input select" id="show-elements-select">
                                                    <select name="jabatan">
                                                        @foreach ($jabatans as $jabatan)
                                                            <option value="{{ $jabatan[0] }}" {{ old('jabatan') == $jabatan[0] ? 'selected' : ''}}>{{ $jabatan[1] }}</option>
                                                        @endforeach
                                                    </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">NIP</label>
                                        <div class="j-input {{ $errors->has('nip') ? ' j-error-view' : '' }}"">
                                            <label class="j-icon-right" for="nip">
                                                <i class="icofont icofont-barcode"></i>
                                            </label>
                                            <input type="text" id="children" name="nip" value=" {{ old('nip') }}">
                                        @if ( $errors->has('nip') )
                                            <span class="j-error-view">{{$errors->first('nip')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- end guests -->
                                <!-- start date -->
                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Jenis Kelamin</label>
                                        <div class="j-input">
                                            <label class="input select" id="show-elements-select">
                                                <select name="jeniskelamin">
                                                   <option value="Laki-Laki" {{ old('jeniskelamin') == 'Laki-Laki' ? 'selected' : ''}}>Laki-Laki</option>
                                                   <option value="Perempuan" {{ old('jeniskelamin') == 'Perempuan' ? 'selected' : ''}}>Perempupan</option>
                                                </select>
                                                <i></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Kontak</label>
                                        <div class="j-input {{ $errors->has('kontak') ? ' j-error-view' : '' }}"">
                                            <label class="j-icon-right" for="kontak">
                                                <i class="icofont icofont-phone"></i>
                                            </label>
                                            <input type="text" id="kontak" name="kontak" value="{{ old('kontak') }}">
                                        @if ( $errors->has('kontak') )
                                            <span class="j-error-view">{{$errors->first('kontak')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="j-unit">
                                    <div class="j-input j-append-small-btn {{ $errors->has('foto') ? ' j-error-view' : '' }}">
                                        <div class="j-file-button">
                                            Browse
                                            <input type="file" id="file2" name="foto" onchange="document.getElementById('file2_input').value = this.value;">
                                        </div>
                                        <input type="text" id="file2_input" readonly="" placeholder="Upload Foto Pegawai">
                                        <span class="j-hint">Ukuran foto 512x512px / 300x300px </span>
                                        @if ( $errors->has('foto') )
                                            <span class="j-error-view">{{$errors->first('foto')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end date -->
                                <div class="j-divider j-gap-bottom-25"></div>
                                <!-- start message -->
                                <div class="j-unit">
                                    <label class="j-label">Alamat</label>
                                    <div class="j-input {{ $errors->has('alamat') ? ' j-error-view' : '' }}"">
                                        <textarea spellcheck="false" name="alamat">{{ old('alamat') }}</textarea>
                                        @if ( $errors->has('alamat') )
                                            <span class="j-error-view">{{$errors->first('alamat')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end message -->
                                <!-- start response from server -->
                                <div class="j-response"></div>
                                <!-- end response from server -->
                            </div>
                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus pegawai bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
