

                                <li class="{{ Request::is('supplier/*') ? 'active' : '' }}">
                                    <a href="{{ route('supplier.index') }}">
                                        <span class="pcoded-micon"><i class="ti-user"></i><b>S</b></span>
                                        <span class="pcoded-mtext">Data Supplier</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="pcoded-hasmenu {{ Request::is('bahan-baku*') ? 'active' : '' }}" dropdown-icon="style1" subitem-icon="style6">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-shopping-cart-full"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Data Bahan Baku</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="{{ route('bahan-baku.index') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Gudang</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ route('bahan-baku-supplier.index') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Supplier</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="{{ Request::is('pengadaan/*') ? 'active' : '' }}">
                                    <a href="{{ route('pengadaan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Pengadaan</span>
                                        @if($jml['pesanan_pengadaan'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['pesanan_pengadaan']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="pcoded-hasmenu {{ Request::is('laporan*') ? 'active' : '' }}" dropdown-icon="style1" subitem-icon="style6">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-file"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Laporan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">

                                        <li class="">
                                            <a href="{{ route('laporan.pengadaan') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Laporan Pengadaan</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>




