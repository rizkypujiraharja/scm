<?php

namespace App\Http\Controllers;

use App\{BOM, Produk, BahanBaku};
use Illuminate\Http\Request;

use Exception, DB;

class BOMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = $request->filter;

        if(!empty($request->filter['produk'])){
            $boms = BOM::where('kode_produk', $request->filter['produk'])->paginate(10);
        }else{
            $boms = BOM::paginate(10);
        }  

        $produks = Produk::where('status', 1)->get();

        return view('bom.index', compact('boms', 'produks', 'param'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produks = Produk::where('status', 1)->get();

        $bahanBakus = BahanBaku::get();

        $jenis = ['Benang Lusi', 'Benang Pakan','Obat Kanji'];

        return view('bom.create', compact('produks', 'bahanBakus', 'jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produk = Produk::where('kode_produk', $request->produk)->first();

        DB::beginTransaction();
        try {

            $old = [];

            for ($i=0; $i < count($request->bahan_baku) ; $i++) { 

                $cek = BOM::where('kode_produk', $produk->kode_produk)
                        ->where('kode_bahan_baku', $request->bahan_baku[$i])
                        ->count();
                // if ($cek > 0 || in_array($request->bahan_baku[$i], $old)){
                //     throw new Exception("Bahan Baku Tidak Boleh Duplikat");
                // }

                if (intVal($request->jumlah[$i]) < 0){
                    throw new Exception("Jumlah (takaran tidak boleh kosong)");
                }

                $bom = new BOM;
                
                $bom->kode_produk = $produk->kode_produk;
                $bom->kode_bahan_baku = $request->bahan_baku[$i];
                $bom->jenis = $request->jenis[$i];
                $bom->jumlah = $request->jumlah[$i];

                $bom->save();
                $old[] = $request->bahan_baku[$i];

            }
            
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());
        }

        return redirect()->route('bom.index')
                ->withInput()
                ->with('alert-success', 'Berhasil menambah BOM!');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BOM  $bom
     * @return \Illuminate\Http\Response
     */
    public function show(BOM $bom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BOM  $bom
     * @return \Illuminate\Http\Response
     */
    public function edit(BOM $bom)
    {
        $produk = Produk::where('kode_produk', $bom->kode_produk)->first();

        $bahanBaku = BahanBaku::findOrFail($bom->kode_bahan_baku);

        return view('bom.edit', compact('bom', 'produk', 'bahanBaku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BOM  $bom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BOM $bom)
    {
        $this->validate($request, [
            'jumlah'    => 'required'
        ]);

        $bom->jumlah = $request->jumlah;
        $bom->save();

        return redirect()->route('bom.index')->with('alert-success', 'Berhasil mengubah BOM!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BOM  $bom
     * @return \Illuminate\Http\Response
     */
    public function destroy(BOM $bom)
    {
        DB::beginTransaction();
        try {

            $bom->delete();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        return redirect()->route('bom.index')->with('alert-success', 'Berhasil menghapus BOM!');
    }

    public function getAjax(Request $request){
        $boms = BOM::join('bahan_baku', 'bom.kode_bahan_baku', 'bahan_baku.kode_bahan_baku')->select('bom.id', 'bom.jenis', 'bahan_baku.nama')->where('bom.kode_produk', $request->id)->get();

        return response()->json(['success' => true, 'data' => $boms]);
    }
}
