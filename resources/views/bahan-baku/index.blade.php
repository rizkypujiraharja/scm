@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Data Bahan Baku</h4>
                    <span>Tambah Data Bahan Baku, Ubah Data Bahan Baku dan Hapus Data Bahan Baku</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bahan-baku.index') }}">Bahan Baku</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        @if($level != 'gudang')
        <div class="card-header">
                <a href="{{ route('bahan-baku.create') }}"><button class="btn btn-sm btn-primary">Tambah Bahan Baku</button></a>
        </div>
        @endif
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Kode Bahan Baku</th>
                            <th>Nama</th>
                            <th>Satuan</th>
                            <th>Stock</th>
                            <th>Stock Sisa</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kode]" value="{{ $param['kode'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nama]" value="{{ $param['nama'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[satuan]" value="{{ $param['satuan'] ?? ''}}">
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('bahan-baku.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr>
                    @foreach ($bahanBakus as $bahanBaku)
                        <tr>
                            <td>{{ $bahanBaku->kode_bahan_baku }}</td>
                            <td>{{ $bahanBaku->nama }}</td>
                            <td>{{ $bahanBaku->satuan }}</td>
                            <td>{{ $bahanBaku->stock }}</td>
                            <td>{{ $bahanBaku->stock_sisa }}</td>
                            <td>
                            @if($level != 'gudang')
                                <a href="{{ route('bahan-baku.edit', $bahanBaku) }}">
                                    <button class="btn btn-mini btn-warning">Edit</button>
                                </a>
                                <button id="delete" data-title="{{$bahanBaku->nama}}" href="{{ route('bahan-baku.destroy', $bahanBaku) }}" class="btn btn-mini btn-danger">Hapus</button>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $bahanBakus->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus bahan baku bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
