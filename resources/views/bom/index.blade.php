@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Data BOM</h4>
                    <span>Tambah Data BOM, Ubah Data BOM dan Hapus Data BOM</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bom.index') }}">BOM</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-header">
            <a href="{{ route('bom.create') }}"><button class="btn btn-sm btn-primary">Tambah BOM</button></a>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Jenis</th>
                            <th>Bahan Baku</th>
                            <th>Jumlah (takaran)</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <select class="form-control" name="filter[produk]">
                                    <option value="">Semua</option>
                                    @foreach ($produks as $produk)
                                        <option value="{{$produk->kode_produk}}" {{ !empty($param['produk']) ? ($produk->kode_produk == $param['produk'] ? 'selected' : '') : '' }}>[{{$produk->kode_produk}}] - {{$produk->nama}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('bom.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr>
                    @foreach ($boms as $bom)
                        <tr>
                            <td>{{ $bom->produk->nama }}</td>
                            <td>{{ $bom->jenis }}</td>
                            <td>{{ $bom->bahanBaku->nama }}</td>
                            <td>{{ $bom->jumlah }}</td>
                            <td>
                                <a href="{{ route('bom.edit', $bom) }}">
                                    <button class="btn btn-mini btn-warning">Edit</button>
                                </a>
                                <button id="delete" href="{{ route('bom.destroy', $bom) }}" class="btn btn-mini btn-danger">Hapus</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $boms->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus BOM ini ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
