<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\BOM;

class BOMTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
            0 =>
            array (
              'id' => 1,
              'kode_produk' => 'K4_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.069000000000000005773159728050814010202884674072265625,
            ),
            1 =>
            array (
              'id' => 2,
              'kode_produk' => 'K4_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.05000000000000000277555756156289135105907917022705078125,
            ),
            2 =>
            array (
              'id' => 3,
              'kode_produk' => 'K4_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.069000000000000005773159728050814010202884674072265625,
            ),
            3 =>
            array (
              'id' => 4,
              'kode_produk' => 'KA4_001',
              'kode_bahan_baku' => 'B_FINE-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.08100000000000000255351295663786004297435283660888671875,
            ),
            4 =>
            array (
              'id' => 5,
              'kode_produk' => 'KA4_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.055000000000000000277555756156289135105907917022705078125,
            ),
            5 =>
            array (
              'id' => 6,
              'kode_produk' => 'KA4_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.10499999999999999611421941381195210851728916168212890625,
            ),
            6 =>
            array (
              'id' => 7,
              'kode_produk' => 'KA4_002',
              'kode_bahan_baku' => 'B_DTY-03',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.037999999999999999056310429068616940639913082122802734375,
            ),
            7 =>
            array (
              'id' => 8,
              'kode_produk' => 'KA4_002',
              'kode_bahan_baku' => 'B_DTY-03',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0340000000000000024424906541753443889319896697998046875,
            ),
            8 =>
            array (
              'id' => 9,
              'kode_produk' => 'KA4_002',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.04599999999999999922284388276239042170345783233642578125,
            ),
            9 =>
            array (
              'id' => 10,
              'kode_produk' => 'KA9_001',
              'kode_bahan_baku' => 'B_SDY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.0879999999999999948929740867242799140512943267822265625,
            ),
            10 =>
            array (
              'id' => 11,
              'kode_produk' => 'KA9_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.036999999999999998168132009368491708301007747650146484375,
            ),
            11 =>
            array (
              'id' => 12,
              'kode_produk' => 'KA9_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.0970000000000000028865798640254070051014423370361328125,
            ),
            12 =>
            array (
              'id' => 13,
              'kode_produk' => 'KBS_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.057000000000000002053912595556539599783718585968017578125,
            ),
            13 =>
            array (
              'id' => 14,
              'kode_produk' => 'KBS_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.050999999999999996724842077355788205750286579132080078125,
            ),
            14 =>
            array (
              'id' => 15,
              'kode_produk' => 'KBS_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.068000000000000004884981308350688777863979339599609375,
            ),
            15 =>
            array (
              'id' => 16,
              'kode_produk' => 'KBT(S1)_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.060999999999999998667732370449812151491641998291015625,
            ),
            16 =>
            array (
              'id' => 17,
              'kode_produk' => 'KBT(S1)_001',
              'kode_bahan_baku' => 'B_DTY-07',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.037999999999999999056310429068616940639913082122802734375,
            ),
            17 =>
            array (
              'id' => 18,
              'kode_produk' => 'KBT(S1)_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.07299999999999999544808559903685818426311016082763671875,
            ),
            18 =>
            array (
              'id' => 19,
              'kode_produk' => 'KBT(1_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.057000000000000002053912595556539599783718585968017578125,
            ),
            19 =>
            array (
              'id' => 20,
              'kode_produk' => 'KBT(1_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.04800000000000000099920072216264088638126850128173828125,
            ),
            20 =>
            array (
              'id' => 21,
              'kode_produk' => 'KBT(1_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.068000000000000004884981308350688777863979339599609375,
            ),
            21 =>
            array (
              'id' => 22,
              'kode_produk' => 'KB(S1)_001',
              'kode_bahan_baku' => 'B_BSY-02',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.059999999999999997779553950749686919152736663818359375,
            ),
            22 =>
            array (
              'id' => 23,
              'kode_produk' => 'KB(S1)_001',
              'kode_bahan_baku' => 'B_BSY-02',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.04499999999999999833466546306226518936455249786376953125,
            ),
            23 =>
            array (
              'id' => 24,
              'kode_produk' => 'KB(S1)_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.066000000000000003108624468950438313186168670654296875,
            ),
            24 =>
            array (
              'id' => 25,
              'kode_produk' => 'KB7_001',
              'kode_bahan_baku' => 'B_PBS-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.11500000000000000499600361081320443190634250640869140625,
            ),
            25 =>
            array (
              'id' => 26,
              'kode_produk' => 'KB7_001',
              'kode_bahan_baku' => 'B_DTY-06',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.07699999999999999900079927783735911361873149871826171875,
            ),
            26 =>
            array (
              'id' => 27,
              'kode_produk' => 'KB7_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.10399999999999999522604099411182687617838382720947265625,
            ),
            27 =>
            array (
              'id' => 28,
              'kode_produk' => 'KC(7)_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.13400000000000000799360577730112709105014801025390625,
            ),
            28 =>
            array (
              'id' => 29,
              'kode_produk' => 'KC(7)_001',
              'kode_bahan_baku' => 'B_DTY-06',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.08100000000000000255351295663786004297435283660888671875,
            ),
            29 =>
            array (
              'id' => 30,
              'kode_produk' => 'KC(7)_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.13400000000000000799360577730112709105014801025390625,
            ),
            30 =>
            array (
              'id' => 31,
              'kode_produk' => 'KDC_001',
              'kode_bahan_baku' => 'B_FINE-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.056000000000000001165734175856414367444813251495361328125,
            ),
            31 =>
            array (
              'id' => 32,
              'kode_produk' => 'KDC_001',
              'kode_bahan_baku' => 'B_FINE-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.04900000000000000188737914186276611872017383575439453125,
            ),
            32 =>
            array (
              'id' => 33,
              'kode_produk' => 'KDC_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.07199999999999999455990717933673295192420482635498046875,
            ),
            33 =>
            array (
              'id' => 34,
              'kode_produk' => 'KE_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.064000000000000001332267629550187848508358001708984375,
            ),
            34 =>
            array (
              'id' => 35,
              'kode_produk' => 'KE_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.057000000000000002053912595556539599783718585968017578125,
            ),
            35 =>
            array (
              'id' => 36,
              'kode_produk' => 'KE_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.07699999999999999900079927783735911361873149871826171875,
            ),
            36 =>
            array (
              'id' => 37,
              'kode_produk' => 'KH_002',
              'kode_bahan_baku' => 'B_FDY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.13300000000000000710542735760100185871124267578125,
            ),
            37 =>
            array (
              'id' => 38,
              'kode_produk' => 'KH_002',
              'kode_bahan_baku' => 'B_FDY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.058999999999999996891375531049561686813831329345703125,
            ),
            38 =>
            array (
              'id' => 39,
              'kode_produk' => 'KH_002',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.172999999999999987121412914348184131085872650146484375,
            ),
            39 =>
            array (
              'id' => 40,
              'kode_produk' => 'KH_003',
              'kode_bahan_baku' => 'B_FDY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.04700000000000000011102230246251565404236316680908203125,
            ),
            40 =>
            array (
              'id' => 41,
              'kode_produk' => 'KH_003',
              'kode_bahan_baku' => 'B_FDY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.037999999999999999056310429068616940639913082122802734375,
            ),
            41 =>
            array (
              'id' => 42,
              'kode_produk' => 'KH_003',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.060999999999999998667732370449812151491641998291015625,
            ),
            42 =>
            array (
              'id' => 43,
              'kode_produk' => 'KHG_001',
              'kode_bahan_baku' => 'B_FDY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.063000000000000000444089209850062616169452667236328125,
            ),
            43 =>
            array (
              'id' => 44,
              'kode_produk' => 'KHG_001',
              'kode_bahan_baku' => 'B_FDY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.04599999999999999922284388276239042170345783233642578125,
            ),
            44 =>
            array (
              'id' => 45,
              'kode_produk' => 'KHG_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.08200000000000000344169137633798527531325817108154296875,
            ),
            45 =>
            array (
              'id' => 46,
              'kode_produk' => 'KH_001',
              'kode_bahan_baku' => 'B_FINE-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.0940000000000000002220446049250313080847263336181640625,
            ),
            46 =>
            array (
              'id' => 47,
              'kode_produk' => 'KH_001',
              'kode_bahan_baku' => 'B_DTY-02',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.051999999999999997613020497055913438089191913604736328125,
            ),
            47 =>
            array (
              'id' => 48,
              'kode_produk' => 'KH_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.12199999999999999733546474089962430298328399658203125,
            ),
            48 =>
            array (
              'id' => 49,
              'kode_produk' => 'KJ_001',
              'kode_bahan_baku' => 'B_BSY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.11400000000000000410782519111307919956743717193603515625,
            ),
            49 =>
            array (
              'id' => 50,
              'kode_produk' => 'KJ_001',
              'kode_bahan_baku' => 'B_FINE-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.08000000000000000166533453693773481063544750213623046875,
            ),
            50 =>
            array (
              'id' => 51,
              'kode_produk' => 'KJ_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.125,
            ),
            51 =>
            array (
              'id' => 52,
              'kode_produk' => 'KL_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.058999999999999996891375531049561686813831329345703125,
            ),
            52 =>
            array (
              'id' => 53,
              'kode_produk' => 'KL_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.051999999999999997613020497055913438089191913604736328125,
            ),
            53 =>
            array (
              'id' => 54,
              'kode_produk' => 'KL_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.070000000000000006661338147750939242541790008544921875,
            ),
            54 =>
            array (
              'id' => 55,
              'kode_produk' => 'KM1_001',
              'kode_bahan_baku' => 'B_BORA-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.1710000000000000131006316905768471769988536834716796875,
            ),
            55 =>
            array (
              'id' => 56,
              'kode_produk' => 'KM1_001',
              'kode_bahan_baku' => 'B_SDY-02',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0909999999999999975575093458246556110680103302001953125,
            ),
            56 =>
            array (
              'id' => 57,
              'kode_produk' => 'KM1_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.188000000000000000444089209850062616169452667236328125,
            ),
            57 =>
            array (
              'id' => 58,
              'kode_produk' => 'KM4_001',
              'kode_bahan_baku' => 'B_BORA-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.067000000000000003996802888650563545525074005126953125,
            ),
            58 =>
            array (
              'id' => 59,
              'kode_produk' => 'KM4_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.052999999999999998501198916756038670428097248077392578125,
            ),
            59 =>
            array (
              'id' => 60,
              'kode_produk' => 'KM4_001',
              'kode_bahan_baku' => 'B_SILKRA-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.07399999999999999633626401873698341660201549530029296875,
            ),
            60 =>
            array (
              'id' => 61,
              'kode_produk' => 'KPD_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.055000000000000000277555756156289135105907917022705078125,
            ),
            61 =>
            array (
              'id' => 62,
              'kode_produk' => 'KPD_001',
              'kode_bahan_baku' => 'B_DTY-06',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.070000000000000006661338147750939242541790008544921875,
            ),
            62 =>
            array (
              'id' => 63,
              'kode_produk' => 'KPD_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.066000000000000003108624468950438313186168670654296875,
            ),
            63 =>
            array (
              'id' => 64,
              'kode_produk' => 'KPN_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.0350000000000000033306690738754696212708950042724609375,
            ),
            64 =>
            array (
              'id' => 65,
              'kode_produk' => 'KPN_001',
              'kode_bahan_baku' => 'B_DTY-05',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.05000000000000000277555756156289135105907917022705078125,
            ),
            65 =>
            array (
              'id' => 66,
              'kode_produk' => 'KPN_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.042000000000000002609024107869117869995534420013427734375,
            ),
            66 =>
            array (
              'id' => 67,
              'kode_produk' => 'KPP_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.05000000000000000277555756156289135105907917022705078125,
            ),
            67 =>
            array (
              'id' => 68,
              'kode_produk' => 'KPP_001',
              'kode_bahan_baku' => 'B_DTY-05',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.057000000000000002053912595556539599783718585968017578125,
            ),
            68 =>
            array (
              'id' => 69,
              'kode_produk' => 'KPP_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.060999999999999998667732370449812151491641998291015625,
            ),
            69 =>
            array (
              'id' => 70,
              'kode_produk' => 'KPS_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.057000000000000002053912595556539599783718585968017578125,
            ),
            70 =>
            array (
              'id' => 71,
              'kode_produk' => 'KPS_001',
              'kode_bahan_baku' => 'B_DTY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.053999999999999999389377336456163902767002582550048828125,
            ),
            71 =>
            array (
              'id' => 72,
              'kode_produk' => 'KPS_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.068000000000000004884981308350688777863979339599609375,
            ),
            72 =>
            array (
              'id' => 73,
              'kode_produk' => 'KPA5_001',
              'kode_bahan_baku' => 'B_PSY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.1390000000000000124344978758017532527446746826171875,
            ),
            73 =>
            array (
              'id' => 74,
              'kode_produk' => 'KPA5_001',
              'kode_bahan_baku' => 'B_DTY-06',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.07900000000000000077715611723760957829654216766357421875,
            ),
            74 =>
            array (
              'id' => 75,
              'kode_produk' => 'KPA5_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.125,
            ),
            75 =>
            array (
              'id' => 76,
              'kode_produk' => 'KS(S1)_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.057000000000000002053912595556539599783718585968017578125,
            ),
            76 =>
            array (
              'id' => 77,
              'kode_produk' => 'KS(S1)_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.052999999999999998501198916756038670428097248077392578125,
            ),
            77 =>
            array (
              'id' => 78,
              'kode_produk' => 'KS(S1)_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.068000000000000004884981308350688777863979339599609375,
            ),
            78 =>
            array (
              'id' => 79,
              'kode_produk' => 'KS_002',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.07199999999999999455990717933673295192420482635498046875,
            ),
            79 =>
            array (
              'id' => 80,
              'kode_produk' => 'KS_002',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0350000000000000033306690738754696212708950042724609375,
            ),
            80 =>
            array (
              'id' => 81,
              'kode_produk' => 'KS_002',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.0859999999999999931166172473240294493734836578369140625,
            ),
            81 =>
            array (
              'id' => 82,
              'kode_produk' => 'KSC4_001',
              'kode_bahan_baku' => 'B_SDY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.11500000000000000499600361081320443190634250640869140625,
            ),
            82 =>
            array (
              'id' => 83,
              'kode_produk' => 'KSC4_001',
              'kode_bahan_baku' => 'B_FINE-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0950000000000000011102230246251565404236316680908203125,
            ),
            83 =>
            array (
              'id' => 84,
              'kode_produk' => 'KSC4_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.1270000000000000017763568394002504646778106689453125,
            ),
            84 =>
            array (
              'id' => 85,
              'kode_produk' => 'KSL_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.0940000000000000002220446049250313080847263336181640625,
            ),
            85 =>
            array (
              'id' => 86,
              'kode_produk' => 'KSL_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.04700000000000000011102230246251565404236316680908203125,
            ),
            86 =>
            array (
              'id' => 87,
              'kode_produk' => 'KSL_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.11300000000000000321964677141295396722853183746337890625,
            ),
            87 =>
            array (
              'id' => 88,
              'kode_produk' => 'KSN_001',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.07199999999999999455990717933673295192420482635498046875,
            ),
            88 =>
            array (
              'id' => 89,
              'kode_produk' => 'KSN_001',
              'kode_bahan_baku' => 'B_SDY-03',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0350000000000000033306690738754696212708950042724609375,
            ),
            89 =>
            array (
              'id' => 90,
              'kode_produk' => 'KSN_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.0859999999999999931166172473240294493734836578369140625,
            ),
            90 =>
            array (
              'id' => 91,
              'kode_produk' => 'KS1_001',
              'kode_bahan_baku' => 'B_FINE-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.08200000000000000344169137633798527531325817108154296875,
            ),
            91 =>
            array (
              'id' => 92,
              'kode_produk' => 'KS1_001',
              'kode_bahan_baku' => 'B_DTY-06',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.063000000000000000444089209850062616169452667236328125,
            ),
            92 =>
            array (
              'id' => 93,
              'kode_produk' => 'KS1_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.10599999999999999700239783351207734085619449615478515625,
            ),
            93 =>
            array (
              'id' => 94,
              'kode_produk' => 'KSN_002',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.07199999999999999455990717933673295192420482635498046875,
            ),
            94 =>
            array (
              'id' => 95,
              'kode_produk' => 'KSN_002',
              'kode_bahan_baku' => 'B_DTY-04',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0330000000000000015543122344752191565930843353271484375,
            ),
            95 =>
            array (
              'id' => 96,
              'kode_produk' => 'KSN_002',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.0859999999999999931166172473240294493734836578369140625,
            ),
            96 =>
            array (
              'id' => 97,
              'kode_produk' => 'KT_002',
              'kode_bahan_baku' => 'B_SDY-04',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.04800000000000000099920072216264088638126850128173828125,
            ),
            97 =>
            array (
              'id' => 98,
              'kode_produk' => 'KT_002',
              'kode_bahan_baku' => 'B_SDY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.035999999999999997279953589668366475962102413177490234375,
            ),
            98 =>
            array (
              'id' => 99,
              'kode_produk' => 'KT_002',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.052999999999999998501198916756038670428097248077392578125,
            ),
            99 =>
            array (
              'id' => 100,
              'kode_produk' => 'KT(S1)_001',
              'kode_bahan_baku' => 'B_BSY-02',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.050999999999999996724842077355788205750286579132080078125,
            ),
            100 =>
            array (
              'id' => 101,
              'kode_produk' => 'KT(S1)_001',
              'kode_bahan_baku' => 'B_DTY-06',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0259999999999999988065102485279567190445959568023681640625,
            ),
            101 =>
            array (
              'id' => 102,
              'kode_produk' => 'KT(S1)_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.056000000000000001165734175856414367444813251495361328125,
            ),
            102 =>
            array (
              'id' => 103,
              'kode_produk' => 'KT4_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.050999999999999996724842077355788205750286579132080078125,
            ),
            103 =>
            array (
              'id' => 104,
              'kode_produk' => 'KT4_001',
              'kode_bahan_baku' => 'B_DTY-06',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.0259999999999999988065102485279567190445959568023681640625,
            ),
            104 =>
            array (
              'id' => 105,
              'kode_produk' => 'KT4_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.056000000000000001165734175856414367444813251495361328125,
            ),
            105 =>
            array (
              'id' => 106,
              'kode_produk' => 'KWC4_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Lusi',
              'jumlah' => 0.13600000000000000976996261670137755572795867919921875,
            ),
            106 =>
            array (
              'id' => 107,
              'kode_produk' => 'KWC4_001',
              'kode_bahan_baku' => 'B_ITY-01',
              'jenis' => 'Benang Pakan',
              'jumlah' => 0.053999999999999999389377336456163902767002582550048828125,
            ),
            107 =>
            array (
              'id' => 108,
              'kode_produk' => 'KWC4_001',
              'kode_bahan_baku' => 'B_Obat-01',
              'jenis' => 'Obat Kanji',
              'jumlah' => 0.13600000000000000976996261670137755572795867919921875,
            ),
        );
        foreach ($users as $data) {
            $user = new BOM;
            $user->id = $data['id'];
            $user->kode_produk = $data['kode_produk'];
            $user->kode_bahan_baku = $data['kode_bahan_baku'];
            $user->jenis = $data['jenis'];
            $user->jumlah = $data['jumlah'];

            $user->save();
        }

        // $pegawais =
    }
}
