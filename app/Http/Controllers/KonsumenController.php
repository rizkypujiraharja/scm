<?php

namespace App\Http\Controllers;

use App\{Konsumen, User};
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Image;
use Exception, DB;

class KonsumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $param = $request->filter;
        
        $konsumens = Konsumen::join('users', 'users.id', '=', 'konsumen.user_id')
                ->select('konsumen.id', 'users.nama', 'users.email', 'users.kontak', 'users.alamat')
                ->where('users.nama', 'like', '%' . ($request->filter['nama'] ?? '')  . '%')
                ->where('users.email', 'like', '%' . ($request->filter['email'] ?? '') . '%')
                ->where('users.kontak', 'like', '%' . ($request->filter['kontak'] ?? '') . '%')
                ->where('users.alamat', 'like', '%' . ($request->filter['alamat'] ?? '')  . '%')
                ->paginate(10);

        return view('konsumen.index', compact('konsumens', 'param'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('konsumen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4|max:50|alpha',
            'email' => 'email|unique:users,email',
            'password' => 'required|string|min:6',
            'kontak' => 'required|numeric',
            'foto'  => 'nullable|image',
            'alamat' => 'required|string|max:200',
        ]);

        $imgname = Null;
        if( !is_null($request->foto) ){
            $imageLocation = public_path('images/foto').'/';
            
            $imgname = 'konsumen-'.time().'.'.$request->foto->getClientOriginalExtension();
            Image::make($request->foto)->resize(300, 300)
                    ->save($imageLocation .$imgname);
        }

        DB::beginTransaction();
        try {

            $user = new User;
            $user->nama = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->jenis_kelamin = $request->jeniskelamin;
            $user->alamat = $request->alamat ?? '-';
            $user->kontak = $request->kontak ?? '-';
            $user->foto = $imgname;
            $user->save();

            $konsumen = New Konsumen;
            $user->konsumen()->save($konsumen);

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }


        return redirect()->route('konsumen.index')->with('alert-success', 'Berhasil menambah konsumen!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $konsumen = Konsumen::findOrFail($id);

        return view('konsumen.edit', compact('konsumen', 'jabatans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $konsumen = Konsumen::findOrFail($id);

        $user = User::findOrFail($konsumen->user_id);

        $this->validate($request, [
            'name' => 'required|min:4|max:50|alpha',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            'password' => 'nullable|string|min:6',
            'kontak' => 'required|numeric',
            'foto'  => 'nullable|image',
            'alamat' => 'required|string|max:200'
        ]);

        DB::beginTransaction();
        try {

            $imgname = Null;

            if( !is_null($request->foto) ){
                $imageLocation = public_path('images/foto').'/';

                $img = $user->foto;
                !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

                $imgname = 'konsumen-'.time().'.'.$request->foto->getClientOriginalExtension();
                Image::make($request->foto)->resize(300, 300)
                        ->save($imageLocation .$imgname);
            }
            
            $konsumen->save();

            $user->nama = $request->name;
            $user->email = $request->email;
            if(!is_null($request->password))
                $user->password = bcrypt($request->password);
            $user->jenis_kelamin = $request->jeniskelamin;
            $user->alamat = $request->alamat ?? '-';
            $user->kontak = $request->kontak ?? '-';
            $user->foto = $imgname;
            $user->save();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        return redirect()->route('konsumen.index')->with('alert-success', 'Berhasil mengubah konsumen!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $konsumen = Konsumen::findOrFail($id);
        $user = User::findOrFail($konsumen->user_id);

        $img = $user->foto;

        DB::beginTransaction();
        try {

            
            $konsumen->delete();
            $user->delete();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        $imageLocation = public_path('images/foto').'/';
        if (!is_null($img)) !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

        return redirect()->back()->with('alert-success', 'Berhasil menghapus data konsumen.');
    }
}
