<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{EOQ, BahanBaku, BahanBakuSupplier, PenerimaanBahanBaku};
use Auth;
use Exception, DB;

class PenjualanBahanBakuController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	$eoqData = EOQ::where('kode_supplier', $user->supplier->kode_supplier)
                ->orderBy('id','DESC')
    			->paginate(20);

    	return view('supplier-penjualan.index', compact('eoqData'));
    }

    public function pengiriman()
    {
        $penerimaanData = PenerimaanBahanBaku::where('kode_supplier', Auth::user()->supplier->kode_supplier)->paginate(10);

        return view('supplier-penjualan.pengiriman', compact('penerimaanData'));
    }

    public function kirim($id)
    {
    	$user = Auth::user();
    	$eoq = EOQ::findOrFail($id);
        $bbs = BahanBakuSupplier::where('kode_supplier', $user->supplier->kode_supplier)
                ->where('kode_bahan_baku', $eoq->kode_bahan_baku)->first();

        DB::beginTransaction();
    	try
        {
    		$eoq->dikirim = $eoq->dikirim + 1;
            $eoq->save();

            if ($bbs->stock < $eoq->jumlah){
                throw new Exception("Stock Anda tidak mencukupi");
            }

            $bbs->stock = $bbs->stock - $eoq->jumlah;
            $bbs->save();

            $history = new PenerimaanBahanBaku;
            $history->eoq_id = $eoq->id;
            $history->kode_supplier = Auth::user()->supplier->kode_supplier;
            $history->jumlah = $eoq->jumlah;
            $history->tanggal = date('Y-m-d');
            $history->status = 0;
            $history->save();

            DB::commit();
    	}
        catch (Exception $e)
        {
            DB::rollback();
    		return redirect()->back()->with('alert-error', $e->getMessage());
    	}

        return redirect()->back()->with('alert-success', 'Pengiriman berhasil!');
    }
}
