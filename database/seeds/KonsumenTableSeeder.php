<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Konsumen;

class KonsumenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
            0 =>
            array (
              'id' => 1,
              'user_id' => 8,
            ),
            1 =>
            array (
              'id' => 2,
              'user_id' => 9,
            ),
            2 =>
            array (
              'id' => 3,
              'user_id' => 10,
            ),
            3 =>
            array (
              'id' => 4,
              'user_id' => 11,
            ),
        );
        foreach ($users as $data) {
            $user = new Konsumen;
            $user->id = $data['id'];
            $user->user_id = $data['user_id'];

            $user->save();
        }

        // $pegawais =
    }
}
