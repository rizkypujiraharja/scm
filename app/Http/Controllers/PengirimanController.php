<?php

namespace App\Http\Controllers;

use App\{Pengiriman, Pesanan, Kendaraan};
use Illuminate\Http\Request;
use Auth, PDF;

class PengirimanController extends Controller
{
    public function index()
    {
        if(Auth::user()->konsumen()->exists()){
            $pesanans = Pesanan::whereIn('status', [5,6])
                ->where('konsumen_id', Auth::user()->konsumen->id)
                ->orderBy('id', 'ASC')->paginate(10);
        }else{
            $pesanans = Pesanan::whereIn('status', [5,6])->orderBy('id', 'ASC')->paginate(10);
        }

        return view('pengiriman.index', compact('pesanans'));
    }

    public function indexPesanan($id)
    {
        if(Auth::user()->konsumen()->exists()){
            $pengirimans = Pengiriman::select('pengiriman.*')
                ->join('pesanan', 'pesanan.id', 'pengiriman.pesanan_id')
                ->where('pesanan.id', $id)
                ->where('pesanan.konsumen_id', Auth::user()->konsumen->id)
                ->orderBy('status', 'ASC')->orderBy('id', 'DESC')->paginate(10);
        }else{
            $pengirimans = Pengiriman::orderBy('status', 'ASC')
                ->where('pesanan_id', $id)
                ->orderBy('id', 'DESC')->paginate(10);
        }
        $status = Pengiriman::status;

        $pesanan = Pesanan::findOrFail($id);

        return view('pengiriman.index-pesanan', compact('pengirimans', 'status', 'pesanan'));

    }

    public function create($id)
    {
        $pesanan = Pesanan::findOrFail($id);
        $kendaraans = Kendaraan::where('status', 1)->get();

        return view('pengiriman.create', compact('pesanan', 'kendaraans'));
    }

    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'jml_pengiriman' => 'numeric|min:200|max:300'
        ],[
            'jml_pengiriman.min' => 'Minimal jumlah pengiriman 200 pcs',
            'jml_pengiriman.max' => 'Maksimal jumlah pengiriman 200 pcs'
        ]);

        $cariKode = Pengiriman::whereNotNull('kode_pengiriman')
                        ->where('tanggal_kirim', 'like', '%'.date('Y-m').'%')
                        ->orderBy('kode_pengiriman', 'DESC')->first();
        $kode = '001';
        if($cariKode != null){
            $kode = explode('/', $cariKode->kode_pengiriman);
            $kode = sprintf("%'02d", $kode[1]+1);
        }
        $newKode = 'TCI/'.$kode.'/GRY/'.date('m').'/'.substr(date('Y'), 2);


        $pesanan = Pesanan::findOrFail($id);
        if( $pesanan->jumlah < ($request->jml_pengiriman + $pesanan->jmlDikirim()) ) {
            return redirect()->back()->withInput()
                ->with('alert-error', 'Jumlah pengiriman melebihi produk yang dipesan');
        }

        if( $request->jml_pengiriman > $pesanan->jmlProduksi()/100 ) {
            return redirect()->back()->withInput()
                ->with('alert-error', 'Jumlah pengiriman melebihi produk yang yang sudah diproduksi');
        }

        if ( $request->kendaraan != null ) {
            $kendaraan = Kendaraan::findOrFail($request->kendaraan);
            if( $kendaraan->kapasitas < $request->jml_pengiriman ) {
                return redirect()->back()
                    ->with('alert-error', 'Jumlah pengiriman melebihi kapasistas');
            }
            $kendaraan->status = 0;
        }

        $pengiriman = new Pengiriman;
        $pengiriman->pesanan_id = $id;
        $pengiriman->kode_pengiriman = $newKode;
        $pengiriman->kendaraan_id = $request->kendaraan;
        $pengiriman->jumlah = $request->jml_pengiriman * 100;

        if ( $request->kendaraan != null ) {
            $pengiriman->status = 1;
        }else{
            $pengiriman->status = 0;
        }
        $pengiriman->tanggal_kirim = date('Y-m-d');

        $pengiriman->save();

        return redirect()->route('pengiriman.index')->with('alert-success', 'Berhasil membuat pengiriman');
    }

    public function terima($id) {
        $pengiriman = Pengiriman::where('id', $id)->first();
        $pengiriman->status = 2;
        $pengiriman->tanggal_terima = date('Y-m-d');
        $pengiriman->save();

        $kendaraan = Kendaraan::findOrFail($pengiriman->kendaraan_id);
        $kendaraan->status = 1;
        $kendaraan->save();

        return redirect()->back()->with('alert-success', 'Pesanan sudah diterima');
    }

    public function requestPengiriman($id)
    {
        $pesanan = Pesanan::findOrFail($id);

        return view('pengiriman.request', compact('pesanan'));
    }

    public function proses($id)
    {

        $pengiriman = Pengiriman::findOrFail($id);
        $pesanan = Pesanan::findOrFail($pengiriman->pesanan_id);
        $kendaraans = Kendaraan::where('status', 1)->get();

        return view('pengiriman.proses', compact('pesanan', 'pengiriman', 'kendaraans'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'jml_pengiriman' => 'numeric|min:200|max:300'
        ],[
            'jml_pengiriman.min' => 'Minimal jumlah pengiriman 200 pcs',
            'jml_pengiriman.max' => 'Maksimal jumlah pengiriman 200 pcs'
        ]);

        $pengiriman = Pengiriman::findOrFail($id);

        $cariKode = Pengiriman::whereNotNull('kode_pengiriman')
                        ->where('tanggal_kirim', 'like', '%'.date('Y-m').'%')
                        ->orderBy('kode_pengiriman', 'DESC')->first();
        $kode = '001';
        if($cariKode != null){
            $kode = explode('/', $cariKode->kode_pengiriman);
            $kode = sprintf("%'02d", $kode[1]+1);
        }
        $newKode = 'TCI/'.$kode.'/GRY/'.date('m').'/'.substr(date('Y'), 2);


        $pesanan = Pesanan::findOrFail($pengiriman->pesanan_id);
        if( $pesanan->jumlah < ($request->jml_pengiriman + $pesanan->jmlDikirim()) ) {
            return redirect()->back()->withInput()
                ->with('alert-error', 'Jumlah pengiriman melebihi produk yang dipesan');
        }

        if( $request->jml_pengiriman > $pesanan->jmlProduksi()/100 ) {
            return redirect()->back()->withInput()
                ->with('alert-error', 'Jumlah pengiriman melebihi produk yang yang sudah diproduksi');
        }

        if ( $request->kendaraan != null ) {
            $kendaraan = Kendaraan::findOrFail($request->kendaraan);
            if( $kendaraan->kapasitas < $request->jml_pengiriman ) {
                return redirect()->back()
                    ->with('alert-error', 'Jumlah pengiriman melebihi kapasistas');
            }
            $kendaraan->status = 0;
            $kendaraan->save();
        }

        // $pengiriman = Pengiriman::findOrFail($id);
        $pengiriman->kendaraan_id = $request->kendaraan;
        $pengiriman->jumlah = $request->jml_pengiriman * 100;

        if ( $request->kendaraan != null ) {
            $pengiriman->status = 1;
        }else{
            $pengiriman->status = 0;
        }
        $pengiriman->tanggal_kirim = date('Y-m-d');
        $pengiriman->kode_pengiriman = $newKode;

        $pengiriman->save();

        return redirect()->route('pengiriman.index')->with('alert-success', 'Berhasil membuat pengiriman');

    }

    public function cetakSJ($id) {
        $pengiriman = Pengiriman::where('id', $id)
                        // ->whereNotNull('kode_pengiriman')
                        ->firstOrFail();
        // $pengiriman = [];
        $pdf = PDF::loadView('cetak.surat-jalan', compact('pengiriman'))->setPaper('a5', 'landscape');
        return $pdf->stream('.pdf');
    }
}
