<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PegawaiTableSeeder::class);
        $this->call(KonsumenTableSeeder::class);
        $this->call(SupplierTableSeeder::class);
        $this->call(ProdukTableSeeder::class);
        $this->call(BahanBakuTableSeeder::class);
        $this->call(BahanBakuSupplierTableSeeder::class);
        $this->call(BOMTableSeeder::class);
        $this->call(KendaraanTableSeeder::class);
    }
}
