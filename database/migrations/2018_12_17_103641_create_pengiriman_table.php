<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengirimanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_pengiriman')->nullable();
            $table->string('pesanan_id', 20);
            $table->integer('kendaraan_id')->unsigned()->nullable();
            $table->bigInteger('jumlah')->unsigned();
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->date('tanggal_kirim')->nullable();
            $table->date('tanggal_terima')->nullable();

            $table->foreign('pesanan_id')->references('id')->on('pesanan');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman');
    }
}
