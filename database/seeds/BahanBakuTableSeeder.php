<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\BahanBaku;

class BahanBakuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
            0 =>
            array (
              'kode_bahan_baku' => 'B_BORA-01',
              'nama' => 'BORA 135/108',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            1 =>
            array (
              'kode_bahan_baku' => 'B_BSY-01',
              'nama' => 'BSY 140/108',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            2 =>
            array (
              'kode_bahan_baku' => 'B_BSY-02',
              'nama' => 'BSY 135/108',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            3 =>
            array (
              'kode_bahan_baku' => 'B_DTY-01',
              'nama' => 'DTY 150/72',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            4 =>
            array (
              'kode_bahan_baku' => 'B_DTY-02',
              'nama' => 'DTY 150/96',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            5 =>
            array (
              'kode_bahan_baku' => 'B_DTY-03',
              'nama' => 'DTY 75/36',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            6 =>
            array (
              'kode_bahan_baku' => 'B_DTY-04',
              'nama' => 'DTY 75/72',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            7 =>
            array (
              'kode_bahan_baku' => 'B_DTY-05',
              'nama' => 'DTY 150/144',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            8 =>
            array (
              'kode_bahan_baku' => 'B_DTY-06',
              'nama' => 'DTY 150/48',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            9 =>
            array (
              'kode_bahan_baku' => 'B_DTY-07',
              'nama' => 'DTY 100/96',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            10 =>
            array (
              'kode_bahan_baku' => 'B_FDY-01',
              'nama' => 'FDY 75/72',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            11 =>
            array (
              'kode_bahan_baku' => 'B_FINE-01',
              'nama' => 'FINE 80/48',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            12 =>
            array (
              'kode_bahan_baku' => 'B_ITY-01',
              'nama' => 'ITY 135/108',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            13 =>
            array (
              'kode_bahan_baku' => 'B_Obat-01',
              'nama' => 'Obat Kanji',
              'satuan' => 'Liter',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            14 =>
            array (
              'kode_bahan_baku' => 'B_PBS-01',
              'nama' => 'PBS 130/108',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            15 =>
            array (
              'kode_bahan_baku' => 'B_PSY-01',
              'nama' => 'PSY 135/108',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            16 =>
            array (
              'kode_bahan_baku' => 'B_SDY-01',
              'nama' => 'SDY 75/36',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            17 =>
            array (
              'kode_bahan_baku' => 'B_SDY-02',
              'nama' => 'SDY 150/96',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            18 =>
            array (
              'kode_bahan_baku' => 'B_SDY-03',
              'nama' => 'SDY 75/72',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            19 =>
            array (
              'kode_bahan_baku' => 'B_SDY-04',
              'nama' => 'SDY 50/36',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
            20 =>
            array (
              'kode_bahan_baku' => 'B_SILKRA-01',
              'nama' => 'SILKRA 60/36',
              'satuan' => 'Kg',
              'stock' => 0,
              'stock_sisa' => 0,
            ),
        );
        foreach ($users as $data) {
            $user = new BahanBaku;
            $user->kode_bahan_baku = $data['kode_bahan_baku'];
            $user->nama = $data['nama'];
            $user->satuan = $data['satuan'];
            $user->stock = $data['stock'];
            $user->stock_sisa = $data['stock_sisa'];

            $user->save();
        }

        // $pegawais =
    }
}
