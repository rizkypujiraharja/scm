@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Pengadaan</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pengadaan.index') }}">Pengadaan</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Pengadaan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- clone elements card start -->
            <div class="card">
                <div class="card-block">
                    <div class="wrapper wrapper-640">
                        <form action="{{ route('pengadaan.store') }}" method="POST" class="j-forms j-pro" id="j-forms" novalidate>
                            @csrf
                            <input type="hidden" name="pesanan" value="{{$pesanan}}">
                            <!-- end /.header-->
                            <div class="content">

                                @foreach ($boms as $bom)

                                    @php
                                        $bbs = $bom->supplier()->where('stock', '>', $kebutuhans[$bom->kode_bahan_baku])->orderBy('harga', 'ASC')->first();
                                    @endphp

                                    @continue($bom->stock_sisa >= $kebutuhans[$bom->kode_bahan_baku])

                                    <div class="j-unit span4">
                                        <label class="j-label">Bahan Baku</label>
                                        <div class="j-input ">
                                            <label class="j-icon-right">
                                                <i class="icofont icofont-layers"></i>
                                            </label>
                                            <input type="hidden" name="kode[]" value="{{ $bom->kode_bahan_baku }}">
                                            <input type="text" name="bom" value="{{ $bom->nama }}" readonly>
                                        </div>
                                    </div>
                                    <div class="j-unit span4">
                                        <label class="j-label">Stok Gudang</label>
                                        <div class="j-input ">
                                            <label class="j-icon-right">
                                                <i class="icofont icofont-layers"></i>
                                            </label>
                                            <input type="text" value="{{$bom->stock_sisa}}" readonly>
                                        </div>
                                    </div>
                                    <div class="j-unit span4">
                                        <label class="j-label">Total Kebutuhan</label>
                                        <div class="j-input ">
                                            <label class="j-icon-right">
                                                <i class="icofont icofont-layers"></i>
                                            </label>
                                            @if( !is_null($bbs) )
                                                <input type="text" value="{{ $bbs->min_order > $kebutuhans[$bom->kode_bahan_baku] ? $bbs->min_order : $kebutuhans[$bom->kode_bahan_baku]}}" name="jumlah[]" readonly>
                                            @else
                                                <input type="text" value="{{ $kebutuhans[$bom->kode_bahan_baku]}}" name="jumlah[]" readonly>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="j-unit">
                                        <label class="j-label">Pilih Supplier</label>
                                        <label class="input select" id="show-elements-select">
                                            <select name="bbs[]">
                                                
                                            @if( !is_null($bbs) )
                                                <option value="{{ $bbs->id }}" readonly>{{ $bbs->supplier->user->nama }} [Harga : {{$bbs->harga}}] [Stok : {{$bbs->stock}}] [Min Order : {{$bbs->min_order}}]</option>
                                            @else
                                                <option readonly>Stock supplier kurang</option>
                                            @endif
                                            </select>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                <div class="j-divider j-gap-bottom-25"></div>
                                @endforeach
                            </div>
                            <!-- end /.content -->
                            <div class="footer">
                                @if( !is_null($bbs) )
                                    <button type="submit" class="btn btn-primary m-b-0">Simpan</button>
                                @else
                                    <button type="button" class="btn btn-primary m-b-0 btn-disabled">Simpan</button>
                                @endif
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- clone elements card end -->
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')

    <script type="text/javascript" src="{{ asset('guru/default/assets/pages/j-pro/js/jquery-cloneya.min.js') }}"></script>
<script type="text/javascript">

$('#produk').on('change', function() {
    $.ajax({
        method : 'GET',
        url : '{{ route('bom.ajax') }}',
        data : {
            'id' : $('#produk option:selected').val(),
            '_token' : '{{csrf_token()}}'
        },
        success:function(s){
            if(s.success === true){
                $.each(s.data, function(index, value) {
                    $('#bom').append('<div class="j-row toclone-widget-right toclone"><div class="span6 unit"><input type="hidden" value="'+value.id+'" name="bom[]"><label class="j-label">Jenis</label><div class="j-input"><label class="j-icon-right"><i class="icofont icofont-box"></i></label><input type="text" value="'+value.jenis+'"></div></div><div class="span6 unit"><label class="j-label">Bahan Baku</label><div class="j-input"><label class="j-icon-right"><i class="icofont icofont-law"></i></label><input type="text" value="'+value.nama+'"></div></div><button type="button" class="btn btn-default clone-btn-right delete"><i class="icofont icofont-minus"></i></button></div>');
                });
            }else{
                swal('error', s.message, 'error');
            }
        },
        error:function(e){
           swal('error', 'Terjadi Kesalahan!', 'error');
        }
    });
});

$(document).ready(function(){

        /***************************************/
        /* Cloned element */
        /***************************************/
        $('body').on('click', '.delete', function() {
            $(this).parent().remove();
        });

        $('.clone-rightside-btn-1').cloneya();

        $('.clone-rightside-btn-2').cloneya();

        $('.clone-leftside-btn-1').cloneya();

        $('.clone-leftside-btn-2').cloneya();

        $('.clone-link').cloneya();
        /***************************************/
        /* end Cloned element */
        /***************************************/

    });
</script>
@endsection
