@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kirim Produk</h4>
                    <span>Kirim produk pengiriman</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pengiriman.index') }}">Pengiriman</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Kirim Produk</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- clone elements card start -->
            <div class="card">
                <div class="card-block">
                    <div class="wrapper wrapper-640">
                        <form action="{{ route('pengiriman.update', $pengiriman) }}" method="POST" class="j-forms j-pro" id="j-forms" novalidate>
                            @csrf
                            <input type="hidden" name="pesanan" value="{{$pengiriman}}">
                            <!-- end /.header-->
                            <div class="content">
                                <div class="j-unit">
                                    <label class="j-label">Produk</label>
                                    <div class="j-input ">
                                        <label class="j-icon-right">
                                            <i class="icofont icofont-layers"></i>
                                        </label>
                                        <input type="text" value="{{ $pesanan->produk->nama }}" readonly="">
                                    </div>
                                </div>

                                <div class="j-unit">
                                    <label class="j-label">Jumlah pengiriman (Tersedia : {{ ($pesanan->jmlProduksi() - $pesanan->jmlDikirim()) / 100 }} pcs)</label>
                                    <div class="j-input {{ $errors->has('jml_pengiriman') ? ' j-error-view' : '' }}"">
                                        <label class="j-icon-right">
                                            <i class="icofont icofont-layers"></i>
                                        </label>
                                        <input type="text" id="jml_pengiriman" name="jml_pengiriman" value="{{ old('jml_pengiriman') ?? $pengiriman->jumlah/100 }}">
                                        @if ( $errors->has('jml_pengiriman') )
                                            <span class="j-error-view">{{$errors->first('jml_pengiriman')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="j-unit">
                                    <label class="j-label">Pilih Kendaraan</label>
                                    <div class="j-input ">
                                        <label class="input select" id="show-elements-select">
                                                <select name="kendaraan">
                                                     @foreach ($kendaraans as $kendaraan)
                                                        <option value="{{$kendaraan->id}}" selected>
                                                            {{$kendaraan->nopol}} [Kapasitas : {{ $kendaraan->kapasitas }} pcs]
                                                        </option>
                                                    @endforeach
                                                </select>
                                        </label>
                                    </div>
                                </div>



                                <div class="j-divider j-gap-bottom-25"></div>
                            <!-- end /.content -->
                            <div class="footer">
                                <button type="submit" class="btn btn-primary m-b-0">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- clone elements card end -->
        </div>
    </div>

@endsection

