@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Produksi</h4>
                    <span>Meminta Bahan Baku Ke Gudang</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('produksi.index') }}">Produksi</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Produksi</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- clone elements card start -->
            <div class="card">
                <div class="card-block">
                    <div class="wrapper wrapper-640">
                        <form action="{{ route('produksi.store', $pesanan) }}" method="POST" class="j-forms j-pro" id="j-forms" novalidate>
                            @csrf
                            <input type="hidden" name="pesanan" value="{{$pesanan}}">
                            <!-- end /.header-->
                            <div class="content">
                                <div class="j-unit span6">
                                    <label class="j-label">Produk</label>
                                    <div class="j-input ">
                                        <label class="j-icon-right">
                                            <i class="icofont icofont-layers"></i>
                                        </label>
                                        <input type="text" value="{{ $pesanan->produk->nama }}" readonly="">
                                    </div>
                                </div>

                                <div class="j-unit span6">
                                    <label class="j-label">Jumlah Produksi</label>
                                    <div class="j-input ">
                                        <label class="j-icon-right">
                                            <i class="icofont icofont-layers"></i>
                                        </label>
                                        <input type="text" id="jml_produksi" name="jml_produksi" value="{{ $jumlah }}">
                                    </div>
                                </div>


                                    <div class="clearfix"></div>
                                <div class="j-divider j-gap-bottom-25"></div>

                                @foreach ($boms as $bom)
                                    <div class="j-unit span4">
                                        <label class="j-label">Bahan Baku</label>
                                        <div class="j-input ">
                                            <label class="j-icon-right">
                                                <i class="icofont icofont-layers"></i>
                                            </label>
                                            <input type="hidden" name="kode[]" value="{{ $bom->bahanBaku->kode_bahan_baku }}">
                                            <input type="text" name="bom" value="{{ $bom->bahanBaku->nama }}" readonly="">
                                        </div>
                                    </div>
                                    <div class="j-unit span4">
                                        <label class="j-label">Stok Gudang</label>
                                        <div class="j-input ">
                                            <label class="j-icon-right">
                                                <i class="icofont icofont-layers"></i>
                                            </label>
                                            <input type="text" name="stock_gudang[]" value="{{$bom->bahanBaku->stock}}" readonly="">
                                        </div>
                                    </div>

                                    <div class="j-unit span4">
                                        <label class="j-label">Total Kebutuhan</label>
                                        <div class="j-input ">
                                            <label class="j-icon-right">
                                                <i class="icofont icofont-layers"></i>
                                            </label>
                                            <input type="text" id="{{ $bom->id }}" value="{{ number_format( $bom->jumlah * $jumlah, 3 ) }}" name="jumlah[]" readonly="">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- end /.content -->
                            <div class="footer">
                                <button type="submit" class="btn btn-primary m-b-0">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- clone elements card end -->
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $('#jml_produksi').on('input', function() {
        @foreach ($boms as $bom)
            var s{{$bom->id}} = {{ $bom->jumlah }} * $("#jml_produksi").val()
            $('#{{$bom->id}}').val( s{{$bom->id}}.toFixed(3) );
        @endforeach
    });
</script>
@endsection
