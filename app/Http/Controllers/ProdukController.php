<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use Exception, DB, Image;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = $request->filter;

        $produks = Produk::where('nama', 'like', '%' . ($request->filter['nama'] ?? '')  . '%')
                    ->where('kode_produk', 'like', '%' . ($request->filter['kode'] ?? '')  . '%')
                    ->paginate(10);

        return view('produk.index', compact('produks', 'param'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'max:50|string',
            'harga' => 'numeric|digits_between:4,9',
            'max' => 'numeric|min:1',
            'gambar' => 'required|image'
        ]);

        $n = explode(' ', $request->name);
        $s = "";
        foreach($n as $k) {
            $s .= ucwords($k[0]);
        }
        $kode = 'K'.$s;
        $cek = Produk::where('kode_produk', 'like', $kode.'\_%')->orderBy('kode_produk', 'DESC')->get();
        dd($cek);
        if( is_null($cek) ){
            $kode = $kode.'_001';
        }else{
            $last = explode('_', $cek->kode_produk);
            $new = sprintf("%'03d", $last[1]+1);
            $kode = $kode.'_'.$new;
        }
        $imgname = Null;
        if( !is_null($request->gambar) ){
            $imageLocation = public_path('images/produk').'/';

            $imgname = $kode.'-'.time().'.'.$request->gambar->getClientOriginalExtension();
            Image::make($request->gambar)->resize(400, 300)
                    ->save($imageLocation .$imgname);
        }

        $produk = New Produk;
        $produk->kode_produk = $kode;
        $produk->nama = $request->name;
        $produk->harga = $request->harga;
        $produk->max_produksi_per_hari = $request->max;
        $produk->gambar = $imgname;
        $produk->save();

        return redirect()->route('produk.index')->with('alert-success', 'Berhasil menambah produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        return view('produk.edit', compact('produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk)
    {
        $this->validate($request, [
            'name' => 'min:3|max:50|string',
            'harga' => 'numeric|digits_between:4,9',
            'max' => 'numeric|min:1',
            'gambar' => 'image'
        ]);

        $imgname = Null;

        if( !is_null($request->gambar) ){
            $imageLocation = public_path('images/produk').'/';

            $img = $produk->gambar;
            if(!is_null($img)) !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

            $imgname = $produk->kode_produk.'-'.time().'.'.$request->gambar->getClientOriginalExtension();
            Image::make($request->gambar)->resize(400, 300)
                    ->save($imageLocation .$imgname);
        }

        $produk->nama = $request->name;
        $produk->harga = $request->harga;
        $produk->max_produksi_per_hari = $request->max;
        $produk->gambar = $imgname;
        $produk->save();

        return redirect()->route('produk.index')->with('alert-success', 'Berhasil mengubah produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        $produk->delete();

        return redirect()->route('produk.index')->with('alert-success', 'Berhasil menghapus produk');
    }
}
