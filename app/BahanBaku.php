<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahanBaku extends Model
{
    protected $table = 'bahan_baku';

    public $timestamps = false;

    protected $primaryKey = 'kode_bahan_baku';

	public $incrementing = false;

    public function supplier()
    {
    	return $this->hasMany('App\BahanBakuSupplier', 'kode_bahan_baku');
    }
}
