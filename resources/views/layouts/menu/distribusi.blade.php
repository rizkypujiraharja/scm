

                                <li class="{{ Request::is('kendaraan/*') ? 'active' : '' }}">
                                    <a href="{{ route('kendaraan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-car"></i><b>K</b></span>
                                        <span class="pcoded-mtext">Data Kendaraan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('pengiriman/*') ? 'active' : '' }}">
                                  <a href="{{ route('pengiriman.index') }}">
                                      <span class="pcoded-micon"><i class="ti-package"></i><b>K</b></span>
                                      <span class="pcoded-mtext">Pengiriman</span>
                                      @if($jml['permintaanPengiriman'] > 0)
                                      <span class="pcoded-badge label label-success">{{$jml['permintaanPengiriman']}}</span>
                                      @endif
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                              </li>
                              <li class="pcoded-hasmenu {{ Request::is('laporan*') ? 'active' : '' }}" dropdown-icon="style1" subitem-icon="style6">
                                  <a href="javascript:void(0)">
                                      <span class="pcoded-micon"><i class="ti-file"></i><b>P</b></span>
                                      <span class="pcoded-mtext">Laporan</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                  <ul class="pcoded-submenu">

                                      <li class="">
                                          <a href="{{ route('laporan.pengiriman') }}">
                                          <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                          <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Laporan Pengiriman</span>
                                          <span class="pcoded-mcaret"></span>
                                          </a>
                                      </li>
                                  </ul>
                              </li>





