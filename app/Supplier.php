<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';

	public $timestamps = false;

	protected $primaryKey = 'kode_supplier';

	public $incrementing = false;
	
	public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
