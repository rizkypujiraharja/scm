@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Proses Pesanan Design Baru</h4>
                    <span>Buat Produk, Buat Komposisi dan Buat Pesanan</span>
                </div>
            </div>
        </div>{{--
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bom.index') }}">BOM</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah BOM</a>
                    </li>
                </ul>
            </div>
        </div> --}}
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- clone elements card start -->
            <div class="card">
                <div class="card-block">
                    <div class="wrapper wrapper-640">
                        <form action="{{ route('pesanan.design.baru.buat', $designBaru->id) }}" method="POST" enctype="multipart/form-data" class="j-forms j-pro" id="j-forms" novalidate>
                            @csrf
                            <!-- end /.header-->
                                <div class="j-content">
                                <!-- start name -->
                                <div class="j-span6 j-unit">
                                    <label class="j-label">Nama Produk</label>
                                    <div class="j-input {{ $errors->has('name') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="name">
                                            <i class="icofont icofont-box"></i>
                                        </label>
                                        <input type="text" id="name" name="name" value="{{ old('name') ?? $designBaru->nama }}" >
                                        @if ( $errors->has('name') )
                                            <span class="j-error-view">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="j-span6 j-unit">
                                    <label class="j-label">Jumlah Pesanan</label>
                                    <div class="j-input {{ $errors->has('jml_pesan') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="name">
                                            <i class="icofont icofont-box"></i>
                                        </label>
                                        <input type="text" id="jml_pesan" name="jml_pesan" value="{{ old('jml_pesan') ?? $designBaru->jumlah }}" readonly="">
                                        @if ( $errors->has('jml_pesan') )
                                            <span class="j-error-view">{{$errors->first('jml_pesan')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->

                                    <div class="clearfix"></div>

                                <!-- start name -->
                                <div class="j-span6 j-unit">
                                    <label class="j-label">Mak Produksi (Yard/Hari)</label>
                                    <div class="j-input {{ $errors->has('max') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="max">
                                            <i class="icofont icofont-settings"></i>
                                        </label>
                                        <input type="text" id="max" name="max" value="{{ old('max') }}">
                                        @if ( $errors->has('max') )
                                            <span class="j-error-view">{{$errors->first('max')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->
                                <!-- start name -->
                                <div class="j-span6 j-unit">
                                    <label class="j-label">Harga</label>
                                    <div class="j-input {{ $errors->has('harga') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="harga">
                                            <i class="icofont icofont-coins"></i>
                                        </label>
                                        <input type="text" id="harga" name="harga" value="{{ old('harga') }}">
                                        @if ( $errors->has('harga') )
                                            <span class="j-error-view">{{$errors->first('harga')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->
                                <div class="clearfix"></div>

                                <div class="j-unit">
                                  <label class="j-label">Gambar</label>
                                  <div class="j-input j-append-small-btn {{ $errors->has('gambar') ? ' j-error-view' : '' }}">
                                    <div class="j-file-button">
                                        Browse
                                        <input type="file" id="file2" name="gambar" onchange="document.getElementById('file2_input').value = this.value;">
                                    </div>
                                    <input type="text" id="file2_input" readonly="" placeholder="Upload gambar produk">
                                    <span class="j-hint">Dimensi gambar 4:3 </span>
                                    @if ( $errors->has('gambar') )
                                        <span class="j-error-view">{{$errors->first('gambar')}}</span>
                                    @endif
                                  </div>
                                </div>

                                    <div class="clearfix"></div>
                                <!-- start response from server -->
                                <div class="j-response"></div>
                                @foreach ($jenis as $j)
                                <div class="span4 j-unit">
                                    <label class="j-label">Jenis</label>
                                    <div class="j-input">
                                        <label class="j-icon-right" for="jumlah">
                                            <i class="icofont icofont-law"></i>
                                        </label>
                                        <input type="text" id="jenis" name="jenis[]" value="{{ $j }}" readonly="">
                                    </div>
                                </div>
                                <div class="span5 j-unit">
                                    <label class="j-label">Bahan Baku</label>
                                    <div class="j-input ">
                                        <label class="input select" id="show-elements-select">
                                                <select name="bahan_baku[]">
                                                     @foreach ($bahanBakus as $bahanBaku)
                                                        <option value="{{$bahanBaku->kode_bahan_baku}}" selected>
                                                            {{$bahanBaku->nama}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="span3 j-unit">
                                    <label class="j-label">Jumlah (takaran)</label>
                                    <div class="j-input">
                                        <label class="j-icon-right" for="jumlah">
                                            <i class="icofont icofont-law"></i>
                                        </label>
                                        <input type="text" id="jumlah" name="jumlah[]">
                                    </div>
                                </div>

                                    <div class="clearfix"></div>
                                @endforeach
                            </div>
                            <!-- end /.content -->
                            <div class="footer">
                                <button type="submit" class="btn btn-primary m-b-0">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- clone elements card end -->
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')

    <script type="text/javascript" src="{{ asset('guru/default/assets/pages/j-pro/js/jquery-cloneya.min.js') }}"></script>
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus bom bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});

$(document).ready(function(){

        /***************************************/
        /* Cloned element */
        /***************************************/
        $('.clone-widget').cloneya();

        $('.clone-rightside-btn-1').cloneya();

        $('.clone-rightside-btn-2').cloneya();

        $('.clone-leftside-btn-1').cloneya();

        $('.clone-leftside-btn-2').cloneya();

        $('.clone-link').cloneya();
        /***************************************/
        /* end Cloned element */
        /***************************************/

    });
</script>
@endsection
