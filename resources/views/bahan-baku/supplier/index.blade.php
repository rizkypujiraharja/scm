@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Data Bahan Baku Supplier</h4>
                    <span>Ubah Data Bahan Baku Supplier dan Hapus Data Bahan Baku Supplier</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bahan-baku-supplier.index') }}">Bahan Baku Supplier</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">

        {{-- @if($level == 'supplier')
        <div class="card-header">
                <a href="{{ route('bahan-baku-supplier.create') }}"><button class="btn btn-sm btn-primary">Tambah Bahan Baku</button></a>
        </div>
        @endif --}}
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama Supplier</th>
                            <th>Bahan Baku</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Satuan</th>
                            <th>Minimal Pembelian</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <select class="form-control" name="filter[supplier]">
                                    <option value="">Semua</option>
                                    @foreach ($suppliers as $supplier)
                                        <option value="{{$supplier->kode_supplier}}" {{ !empty($param['supplier']) ? ($supplier->kode_supplier == $param['supplier'] ? 'selected' : '') : '' }}>[{{$supplier->kode_supplier}}] - {{$supplier->user->nama}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('bahan-baku-supplier.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr> --}}
                    @foreach ($bbsData as $bbs)
                        <tr>
                            <td>{{ $bbs->supplier->user->nama }}</td>
                            <td>{{ $bbs->bahanBaku->nama }}</td>
                            <td>Rp. {{ rupiah($bbs->harga) }}</td>
                            <td>{{ $bbs->stock }}</td>
                            <td>{{ $bbs->bahanBaku->satuan }}</td>
                            <td>{{ $bbs->min_order }}</td>
                            <td>
                                <a href="{{ route('bahan-baku-supplier.edit', $bbs) }}">
                                    <button class="btn btn-mini btn-warning">Edit</button>
                                </a>
                                <button id="delete" href="{{ route('bahan-baku-supplier.destroy', $bbs) }}" class="btn btn-mini btn-danger">Hapus</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $bbsData->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus Bahan Baku Supplier ini ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
