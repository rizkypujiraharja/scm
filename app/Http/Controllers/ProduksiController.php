<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Produksi, Pesanan, BOM, BahanBaku, PermintaanBahanBaku};
use Auth;
use Exception, DB;

class ProduksiController extends Controller
{
    public function indexPesanan($id)
    {
    	$produksis = Produksi::where('pesanan_id', $id)->orderBy('id', 'DESC')->paginate(10);
        $status = Produksi::status;

        $pesanan = Pesanan::findOrFail($id);
        return view('produksi.index-pesanan', compact('produksis', 'status', 'pesanan'));
    }

    public function index()
    {
        if(Auth::user()->konsumen()->exists()){
            $pesanans = Pesanan::where('konsumen_id', Auth::user()->konsumen->id)->orderBy('id', 'DESC')->paginate(10);
        }else{
            $pesanans = Pesanan::whereIn('status', [4, 5])->orderBy('id', 'DESC')->paginate(10);
        }

        return view('produksi.index', compact('pesanans'));
    }

    public function create($id)
    {
    	$pesanan = Pesanan::whereIn('status', [4, 5])->findOrFail($id);
    	$boms = BOM::where('kode_produk', $pesanan->kode_produk)->get();

        $jumlah = Produksi::select(DB::raw('SUM(jumlah) as jumlah'))
            ->where('pesanan_id', $pesanan->id)
            ->where('status', 1)->first()->jumlah;

        $sisa = $pesanan->jumlah - $jumlah;
        if($sisa < $pesanan->produk->max_produksi_per_hari ) {
            $jumlah = $sisa;
        }else{
            $jumlah = $pesanan->produk->max_produksi_per_hari;
        }

    	return view('produksi.create', compact('pesanan', 'boms', 'jumlah'));
    }

    public function store(Request $request, $id)
    {
    	DB::beginTransaction();
        try {

        	$produksi = New Produksi;
        	$produksi->pesanan_id = $id;
        	$produksi->tanggal = date('Y-m-d');
            $produksi->jumlah = $request->jml_produksi;
            $produksi->status = 0;
        	$produksi->save();

            foreach ($request->jumlah as $key => $value) {

                if($request->stock_gudang[$key] < $request->jumlah[$key] ){
                    return redirect()->back()->withInput()->with('alert-error', 'Stock Tidak Cukup');
                }

            	$permintaan = new PermintaanBahanBaku;
                $permintaan->produksi_id = $produksi->id;
                $permintaan->kode_bahan_baku = $request->kode[$key];
                $permintaan->jumlah = $request->jumlah[$key];
                $permintaan->tanggal = date('Y-m-d');
                $permintaan->save();
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());
        }


            return redirect()->route('produksi.index-pesanan', $produksi->pesanan_id)->with('alert-success', 'Berhasil menambah permintaan bahan baku untuk produksi');
    }

    public function detail($id)
    {
        $produksi = Produksi::findOrFail($id);

        $permintaans = PermintaanBahanBaku::where('produksi_id', $produksi->id)->get();

        $status = Produksi::status;

        return view('produksi.view', compact('produksi', 'status', 'permintaans'));
    }

    public function setujui($id)
    {
        $produksi = Produksi::findOrFail($id);
        $permintaans = $produksi->permintaanBB()->get();

        DB::beginTransaction();
        try {

            $produksi->status = 2;
            $produksi->save();

            foreach ($permintaans as $permintaan) {
                $bahanBaku = BahanBaku::where('kode_bahan_baku', $permintaan->kode_bahan_baku)->first();

                if($permintaan->jumlah > $bahanBaku->stock){
                    return redirect()->back()->with('alert-error', 'Bahan Baku "'.$bahanBaku->nama.'" tidak mencukupi');
                }

                $bahanBaku->stock = $bahanBaku->stock - $permintaan->jumlah;
                $bahanBaku->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('alert-error', 'Terjadi kesalahan');
        }

        return redirect()->back()->with('alert-success', 'Produksi #'.$id.' disetujiu!');
    }

    public function selesai($id)
    {

            $produksi = Produksi::findOrFail($id);
            $produksi->status = 1;
            $produksi->save();

            $pesanan = Pesanan::where('id', $produksi->pesanan_id)->firstOrFail();
            $pesanan->status = 5;
            if( $pesanan->jmlProduksi() >= $pesanan->jumlah ){
                $pesanan->status = 6;
            }

            $pesanan->save();

        return redirect()->back()->with('alert-success', 'Produksi #'.$id.' selesai!');
    }
}
