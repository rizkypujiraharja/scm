<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Produk;
class ProdukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
            0 =>
            array (
              'kode_produk' => 'K4_001',
              'nama' => '4205',
              'harga' => 8500,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            1 =>
            array (
              'kode_produk' => 'KA4_001',
              'nama' => 'AMUNZEN 44',
              'harga' => 7800,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            2 =>
            array (
              'kode_produk' => 'KA4_002',
              'nama' => 'ASAHI 433972',
              'harga' => 6600,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            3 =>
            array (
              'kode_produk' => 'KA9_001',
              'nama' => 'AT 9600',
              'harga' => 7100,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            4 =>
            array (
              'kode_produk' => 'KB(S1)_001',
              'nama' => 'BSY ( SN 125 )',
              'harga' => 9200,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            5 =>
            array (
              'kode_produk' => 'KB7_001',
              'nama' => 'BSY 7050',
              'harga' => 8800,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            6 =>
            array (
              'kode_produk' => 'KBS_001',
              'nama' => 'Baby Silk',
              'harga' => 6000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            7 =>
            array (
              'kode_produk' => 'KBT(1_001',
              'nama' => 'Baby Touch (SN 112)',
              'harga' => 6000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            8 =>
            array (
              'kode_produk' => 'KBT(S1)_001',
              'nama' => 'Baby Tauch ( SN 131 )',
              'harga' => 7000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            9 =>
            array (
              'kode_produk' => 'KC(7)_001',
              'nama' => 'Celana ( 743964 )',
              'harga' => 10000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            10 =>
            array (
              'kode_produk' => 'KDC_001',
              'nama' => 'Double Chiffon',
              'harga' => 15000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            11 =>
            array (
              'kode_produk' => 'KE_001',
              'nama' => 'EVALIA',
              'harga' => 6300,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            12 =>
            array (
              'kode_produk' => 'KH_001',
              'nama' => 'HWI-W',
              'harga' => 10800,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            13 =>
            array (
              'kode_produk' => 'KH_002',
              'nama' => 'Hi-Chiffon',
              'harga' => 11200,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            14 =>
            array (
              'kode_produk' => 'KH_003',
              'nama' => 'Hi-Count',
              'harga' => 6900,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            15 =>
            array (
              'kode_produk' => 'KHG_001',
              'nama' => 'HI-MULTY-01 / GMS-02',
              'harga' => 8700,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            16 =>
            array (
              'kode_produk' => 'KJ_001',
              'nama' => 'JEANS',
              'harga' => 7500,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            17 =>
            array (
              'kode_produk' => 'KL_001',
              'nama' => 'LEXSUS',
              'harga' => 6300,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            18 =>
            array (
              'kode_produk' => 'KM1_001',
              'nama' => 'MA 10530',
              'harga' => 14400,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            19 =>
            array (
              'kode_produk' => 'KM4_001',
              'nama' => 'MA 4200',
              'harga' => 7000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            20 =>
            array (
              'kode_produk' => 'KPA5_001',
              'nama' => 'PLANT ANTEX 58',
              'harga' => 6600,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            21 =>
            array (
              'kode_produk' => 'KPD_001',
              'nama' => 'Peach Dobby',
              'harga' => 6700,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            22 =>
            array (
              'kode_produk' => 'KPN_001',
              'nama' => 'PEACH NT',
              'harga' => 6200,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            23 =>
            array (
              'kode_produk' => 'KPP_001',
              'nama' => 'PEACH PP',
              'harga' => 6600,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            24 =>
            array (
              'kode_produk' => 'KPS_001',
              'nama' => 'PEACH SKIN',
              'harga' => 9000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            25 =>
            array (
              'kode_produk' => 'KS_002',
              'nama' => 'Satin',
              'harga' => 8500,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            26 =>
            array (
              'kode_produk' => 'KS_003',
              'nama' => 'ST-CH',
              'harga' => 7500,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            27 =>
            array (
              'kode_produk' => 'KS(S1)_001',
              'nama' => 'Sakura ( SN 148 )',
              'harga' => 6600,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            28 =>
            array (
              'kode_produk' => 'KS1_001',
              'nama' => 'SN 118',
              'harga' => 7600,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            29 =>
            array (
              'kode_produk' => 'KSC4_001',
              'nama' => 'SATIN CHARMUSE 44"',
              'harga' => 9000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            30 =>
            array (
              'kode_produk' => 'KSL_001',
              'nama' => 'Satin LD',
              'harga' => 8100,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            31 =>
            array (
              'kode_produk' => 'KSN_001',
              'nama' => 'Satin NBO',
              'harga' => 8000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            32 =>
            array (
              'kode_produk' => 'KSN_002',
              'nama' => 'ST NT',
              'harga' => 7100,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            33 =>
            array (
              'kode_produk' => 'KT_002',
              'nama' => 'TAFETTA',
              'harga' => 5500,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            34 =>
            array (
              'kode_produk' => 'KT(S1)_001',
              'nama' => 'TESSA ( SN 138 )',
              'harga' => 4800,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            35 =>
            array (
              'kode_produk' => 'KT4_001',
              'nama' => 'Tessa 44"',
              'harga' => 6000,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
            36 =>
            array (
              'kode_produk' => 'KWC4_001',
              'nama' => 'Wooly Crepe 44"',
              'harga' => 10100,
              'max_produksi_per_hari' => 5000,
              'status' => 1,
            ),
        );
        foreach ($users as $data) {
            $user = new Produk;
            $user->kode_produk = $data['kode_produk'];
            $user->nama = $data['nama'];
            $user->harga = $data['harga'];
            $user->max_produksi_per_hari = $data['max_produksi_per_hari'];
            $user->status = $data['status'];
            $user->gambar = $data['kode_produk'].'.jpg';

            $user->save();
        }

        // $pegawais =
    }
}
