<?php

namespace App\Http\Middleware;

use Closure;

class Level
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $level)
    {
        $user = $request->user();
        if($user == null) abort(403);

        $levels = explode("|", $level);
        $level = null;
        switch(true)
        {
            case $user->pegawai()->exists():
                $level = 'pegawai';
                break;

            case $user->konsumen()->exists():
                $level = 'konsumen';
                break;

            case $user->supplier()->exists():
                $level = 'supplier';
                break;

            default:
                break;
        }
        
        if( !$level || !in_array($level, $levels) ) abort(403);

        if($level == 'pegawai'){
            $jabatan = $user->pegawai->jabatan;
            $level = $jabatan;
        }

        session(['user_level' => $level]);

        return $next($request);
    }
}
