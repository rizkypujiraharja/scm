<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bom', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_produk', 20);
            $table->string('kode_bahan_baku', 20);
            $table->string('jenis');
            $table->float('jumlah', 12, 3);

            $table->foreign('kode_produk')->references('kode_produk')->on('produk');
            $table->foreign('kode_bahan_baku')->references('kode_bahan_baku')->on('bahan_baku');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bom');
    }
}
