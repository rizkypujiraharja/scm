<?php

namespace App\Providers;

use Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use DB;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view) {
            $user = Auth::user();
            $view->with('signed_user', $user);
            $level = session('user_level');
            $view->with('level', $level);
        });

        View::composer('layouts.sidebar', function($view) {
            $jml['designBaru'] = DB::table('pesanan_design_baru')->where('status', 0)->count();

            $jml['pengadaan_persetujuan'] = DB::table('pengadaan')->where('status', 0)->count();

            $jml['pesanan_persetujuan'] = DB::table('pesanan')->where('status', 0)->count();
            $jml['pesanan_pengadaan'] = DB::table('pesanan')->where('status', 1)->count();

            $jml['penerimaanBB'] = DB::table('penerimaan_bahan_baku')->where('status', 0)->count();

            $jml['produksi_proses'] = DB::table('produksi')->where('status', 2)->count();
            $jml['produksi_permintaanBB'] = DB::table('produksi')->where('status', 0)->count();
            $jml['permintaanPengiriman'] = DB::table('pengiriman')->where('status', 0)->count();
            if(session('user_level') == 'supplier')
            $jml['penjualan_bb'] = DB::table('eoq')->where('kode_supplier', Auth::user()->supplier->kode_supplier)->whereColumn('dikirim', '<', 'frekuensi')->count();
            if(session('user_level') == 'konsumen'){
                $jml['penerimaanKonsumen'] = DB::table('pesanan')
                    ->join('pengiriman', 'pesanan.id', '=', 'pengiriman.pesanan_id')
                    ->whereIn('pesanan.status', [5,6])
                    ->where('pesanan.konsumen_id', Auth::user()->konsumen->id)
                    ->where('pengiriman.status', 1)
                    ->count();
            }
            $view->with('jml', $jml);
        });

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
