@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Data Pesanan</h4>
                    <span>Manage Data Pesanan</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pesanan.index') }}">Pesanan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-header">
            <a href="{{ route('pesanan.produk', 'bb-perusahaan') }}"><button class="btn btn-sm btn-primary">Tambah Pesanan (BB Perusahaan)</button></a>
            <a href="{{ route('pesanan.produk', 'bb-konsumen') }}"><button class="btn btn-sm btn-warning">Tambah Pesanan (BB Konsumen)</button></a>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Konsumen</th>
                            <th>Produk</th>
                            <th>Jumlah (Yard)</th>
                            <th>Sisa Kirim</th>
                            <th>Status</th>
                            <th>Jenis</th>
                            <th>Tanggal Pesan</th>
                            {{-- <th>Tanggal Kirim</th> --}}
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                                <td></td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nopol]" value="{{ $param['nopol'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kapasitas]" value="{{ $param['kapasitas'] ?? ''}}">
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('pesanan.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr> --}}
                    @foreach ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $pesanan->id }}</td>
                            <td>{{ $pesanan->konsumen->user->nama }}</td>
                            <td>{{ $pesanan->produk->nama }}</td>
                            <td>{{ rupiah($pesanan->jumlah) }}</td>
                            <td>{{ rupiah($pesanan->jumlah - $pesanan->jmlDikirim()) }}</td>
                            @if ($level == 'konsumen' && in_array($pesanan->status, [1,4,5]))
                            <td align="center"><span class="label label-info">Proses</span></td>
                            @else
                            <td align="center"><span class="label label-{{$status[$pesanan->status][1]}}">{{$status[$pesanan->status][0]}}</span></td>
                            @endif
                            <td>{{ $pesanan->jenis }}</td>
                            <td>{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
                            {{-- <td>{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td> --}}
                            <td>
                                <a href="{{ route('pesanan.detail', $pesanan) }}">
                                    <button class="btn btn-mini btn-primary">Detail</button>
                                </a>
                                <a target="_blank" href="{{ route('pesanan.cetak', $pesanan) }}">
                                    <button class="btn btn-mini btn-success">Cetak</button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $pesanans->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus pesanan bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
