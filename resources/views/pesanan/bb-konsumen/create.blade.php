@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Pesanan</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pesanan.index') }}">Pesanan</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Pesanan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- clone elements card start -->
            <div class="card">
                <div class="card-block">
                    <div class="wrapper wrapper-640">
                        <form action="{{ route('pesanan.bb.konsumen.store') }}" method="POST" class="j-forms j-pro" id="j-forms" novalidate>
                            @csrf
                            <div class="content">
                            <center>
                                <img src="{{ $produk->getGambar() }}" style="width:50%;padding-bottom:10px;">
                                <h5>{{ $produk->nama }}</h5>
                              </center>
                            <input type="hidden" name="produk" value="{{ $produk->kode_produk }}">
                              <div class="clearfix"></div>

@if(session('user_level') != 'konsumen')
                                <div class="j-unit">
                                    <label class="j-label">Konsumen</label>
                                        <div class="j-input ">
                                            <label class="input select" id="show-elements-select">
                                                    <select name="konsumen">
                                                         @foreach ($konsumens as $konsumen)
                                                            <option value="{{$konsumen->id}}" {{ !empty($param['produk']) ? ($konsumen->id == $param['konsumen'] ? 'selected' : '') : '' }}>{{$konsumen->user->nama}}</option>
                                                        @endforeach
                                                    </select>
                                            </label>
                                        </div>
                                </div>
@else
<input type="hidden" name="konsumen" value="konsumen">
@endif

                                <!-- start name -->
                                <div class="j-unit">
                                    <br>
                                    <label class="j-label">Jumlah Pemesanan (Yard)</label>
                                    <div class="j-input {{ $errors->has('jumlah') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="jumlah">
                                            <i class="icofont icofont-layers"></i>
                                        </label>
                                        <input type="text" id="jumlah" name="jumlah" value="{{ old('name') }}">
                                        @if ( $errors->has('jumlah') )
                                            <span class="j-error-view">{{$errors->first('jumlah')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->

                                <div class="divider-text gap-top-45 gap-bottom-45">
                                    <span>Bahan Baku Dari Konsumen</span>
                                </div>

                                  @foreach ($produk->bom()->get() as $bom)
                                  <div class="j-row">
                                    <div class="span6 unit">
                                      <input type="hidden" value="{{ $bom->id }}" name="bom[]">
                                      <label class="j-label">Jenis</label>
                                      <div class="j-input">
                                        <label class="j-icon-right"><i class="icofont icofont-box"></i></label>
                                        <input type="text" value="{{ $bom->jenis }}" readonly>
                                      </div>
                                    </div>
                                    <div class="span6 unit">
                                      <label class="j-label">Bahan Baku</label>
                                      <div class="j-input">
                                        <label class="j-icon-right"><i class="icofont icofont-law"></i></label>
                                      <input type="text" value="{{ $bom->bahanBaku->nama }}" readonly>
                                      </div>
                                    </div>
                                  </div>
                                  @endforeach
                            </div>
                            <!-- end /.content -->
                            <div class="footer">
                                <button type="submit" class="btn btn-primary m-b-0">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- clone elements card end -->
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')

    <script type="text/javascript" src="{{ asset('guru/default/assets/pages/j-pro/js/jquery-cloneya.min.js') }}"></script>
<script type="text/javascript">

$('#produk').on('change', function() {
    $.ajax({
        method : 'GET',
        url : '{{ route('bom.ajax') }}',
        data : {
            'id' : $('#produk option:selected').val(),
            '_token' : '{{csrf_token()}}'
        },
        success:function(s){
            if(s.success === true){
                $.each(s.data, function(index, value) {
                    $('#bom').append('<div class="j-row"><div class="span6 unit"><input type="hidden" value="'+value.id+'" name="bom[]"><label class="j-label">Jenis</label><div class="j-input"><label class="j-icon-right"><i class="icofont icofont-box"></i></label><input type="text" value="'+value.jenis+'" readonly></div></div><div class="span6 unit"><label class="j-label">Bahan Baku</label><div class="j-input"><label class="j-icon-right"><i class="icofont icofont-law"></i></label><input type="text" value="'+value.nama+'" readonly></div></div></div>');
                });
            }else{
                swal('error', s.message, 'error');
            }
        },
        error:function(e){
           swal('error', 'Terjadi Kesalahan!', 'error');
        }
    });
});

$(document).ready(function(){

        /***************************************/
        /* Cloned element */
        /***************************************/
        $('.clone-rightside-btn-1').cloneya();

        $('.clone-rightside-btn-2').cloneya();

        $('.clone-leftside-btn-1').cloneya();

        $('.clone-leftside-btn-2').cloneya();

        $('.clone-link').cloneya();
        /***************************************/
        /* end Cloned element */
        /***************************************/

    });
</script>
@endsection
