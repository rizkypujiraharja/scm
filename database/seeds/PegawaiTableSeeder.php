<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Pegawai;

class PegawaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =array (
            0 =>
            array (
              'id' => 1,
              'user_id' => 1,
              'nip' => 10114482,
              'jabatan' => 'admin',
            ),
            1 =>
            array (
              'id' => 2,
              'user_id' => 2,
              'nip' => 10224154,
              'jabatan' => 'manager',
            ),
            2 =>
            array (
              'id' => 3,
              'user_id' => 3,
              'nip' => 12312321,
              'jabatan' => 'gudang',
            ),
            3 =>
            array (
              'id' => 5,
              'user_id' => 5,
              'nip' => 12345235,
              'jabatan' => 'purchasing',
            ),
            4 =>
            array (
              'id' => 6,
              'user_id' => 6,
              'nip' => 64263786,
              'jabatan' => 'produksi',
            ),
            5 =>
            array (
              'id' => 7,
              'user_id' => 7,
              'nip' => 87654321,
              'jabatan' => 'distribusi',
            ),
          );
        foreach ($users as $data) {
            $user = new Pegawai;
            $user->id = $data['id'];
            $user->user_id = $data['user_id'];
            $user->nip = $data['nip'];
            $user->jabatan = $data['jabatan'];

            $user->save();
        }

        // $pegawais =
    }
}
