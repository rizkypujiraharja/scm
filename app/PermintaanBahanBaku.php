<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermintaanBahanBaku extends Model
{
    protected $table = 'permintaan_bahan_baku';

	public $timestamps = false;

	public function produksi()
	{
		return $this->belongsTo('App\Produksi');
	}

	public function bahanBaku()
	{
		return $this->belongsTo('App\BahanBaku', 'kode_bahan_baku');
	}
}
