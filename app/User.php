<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'jenis_kelamin', 'alamat', 'kontak', 'email', 'password', 'status', 'foto'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pegawai()
    {
        return $this->hasOne('App\Pegawai', 'user_id');
    }

    public function konsumen()
    {
        return $this->hasOne('App\Konsumen', 'user_id');
    }

    public function supplier()
    {
        return $this->hasOne('App\Supplier', 'user_id');
    }

    public function getPhoto()
    {
        return url('/images/foto').'/'.($this->foto ?? 'user.png');
    }

    
}
