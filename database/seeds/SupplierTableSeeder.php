<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Supplier;

class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
            0 =>
            array (
              'kode_supplier' => 'S_Ia',
              'user_id' => 12,
            ),
            1 =>
            array (
              'kode_supplier' => 'S_CM',
              'user_id' => 13,
            ),
            2 =>
            array (
              'kode_supplier' => 'S_Pn',
              'user_id' => 14,
            ),
            3 =>
            array (
              'kode_supplier' => 'S_Sx',
              'user_id' => 15,
            ),
            4 =>
            array (
              'kode_supplier' => 'S_Sn',
              'user_id' => 16,
            ),
            5 =>
            array (
              'kode_supplier' => 'S_To',
              'user_id' => 20,
            ),
            6 =>
            array (
              'kode_supplier' => 'S_Ca',
              'user_id' => 21,
            ),
            7 =>
            array (
              'kode_supplier' => 'S_BX',
              'user_id' => 22,
            ),
            8 =>
            array (
              'kode_supplier' => 'S_ML',
              'user_id' => 23,
            ),
            9 =>
            array (
              'kode_supplier' => 'S_CN',
              'user_id' => 25,
            ),
        );
        foreach ($users as $data) {
            $user = new Supplier;
            $user->kode_supplier = $data['kode_supplier'];
            $user->user_id = $data['user_id'];

            $user->save();
        }

        // $pegawais =
    }
}
