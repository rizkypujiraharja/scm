<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTglSelesaiOnPesananDesignBaruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesanan_design_baru', function (Blueprint $table) {
            $table->date('tanggal_setuju')->nullable()->after('tanggal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesanan_design_baru', function (Blueprint $table) {
            $table->dropColumn('tanggal_setuju');
        });
    }
}
