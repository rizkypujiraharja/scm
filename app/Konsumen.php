<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konsumen extends Model
{
	protected $table = 'konsumen';

	public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function pesanan()
    {
    	return $this->hasMany('App\Pesanan');
    }
}
