<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';

    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    const data_jabatan = [
    		['admin', 'Admin'],
    		['gudang', 'Bagian Gudang'],
    		['manager', 'General Manager'],
    		['purchasing', 'Bagian Pengadaan'],
    		['produksi', 'Bagian Produksi'],
    		['distribusi', 'Bagian Pengiriman'],
    	];

    public function getJabatan()
    {
    	switch ($this->jabatan) {
    		case 'admin':
    			return 'Admin';
    			break;

    		case 'gudang':
    			return 'Bagian Gudang';
    			break;

    		case 'manager':
    			return 'General Manager';
    			break;

    		case 'purchasing':
    			return 'Bagian Pengadaan';
    			break;

    		case 'produksi':
    			return 'Bagian Produksi';
    			break;

    		case 'distribusi':
    			return 'Bagian Pengiriman';
    			break;

    		default:
    			return '-';
    			break;
    	}
    }
}
