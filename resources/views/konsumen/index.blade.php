@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-user bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Data Konsumen</h4>
                    <span>Tambah Data Konsumen, Ubah Data Konsumen dan Hapus Data Konsumen</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('konsumen.index') }}">Konsumen</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-header">
            <a href="{{ route('konsumen.create') }}"><button class="btn btn-sm btn-primary">Tambah Konsumen</button></a>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Kontak</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nama]" value="{{ $param['nama'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kontak]" value="{{ $param['kontak'] ?? ''}}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[email]" value="{{ $param['email'] ?? ''}}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[alamat]" value="{{ $param['alamat'] ?? ''}}">
                            </td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('konsumen.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr>
                    @foreach ($konsumens as $konsumen)
                        <tr>
                            <td>{{ $konsumen->nama }}</td>
                            <td>{{ $konsumen->kontak }}</td>
                            <td>{{ $konsumen->email }}</td>
                            <td>{{ $konsumen->alamat }}</td>
                            <td>
                                <a href="{{ route('konsumen.edit', $konsumen) }}">
                                    <button class="btn btn-mini btn-warning">Edit</button>
                                </a>
                                <button id="delete" data-title="{{$konsumen->nama}}" href="{{ route('konsumen.destroy', $konsumen) }}" class="btn btn-mini btn-danger">Hapus</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $konsumens->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus konsumen bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
