<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermintaanBahanBakuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_bahan_baku', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produksi_id')->unsigned();
            $table->string('kode_bahan_baku', 20);
            $table->float('jumlah', 12, 3);
            $table->date('tanggal');

            $table->foreign('produksi_id')->references('id')->on('produksi');
            $table->foreign('kode_bahan_baku')->references('kode_bahan_baku')->on('bahan_baku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_bahan_baku');
    }
}
