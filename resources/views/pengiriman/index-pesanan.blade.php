@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Data Pengiriman</h4>
                    <span>Manage Data Pengiriman</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pengiriman.index') }}">pengiriman</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">

        <div class="card-header">
          @if ($level == 'konsumen')
            @if($pesanan->status == 6 || $pesanan->status == 5)
              <a href="{{ route('pengiriman.request', $pesanan) }}"><button class="btn btn-sm btn-primary">Minta Pengiriman</button></a>
            @endif
          @else
            <a href="{{ route('pengiriman.create', $pesanan) }}"><button class="btn btn-sm btn-primary">Buat Pengiriman</button></a>
          @endif

        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Kode Pengiriman</th>
                            <th>Tgl Kirim</th>
                            <th>Tgl Terima</th>
                            @if ($level != 'konsumen')
                            <th>Konsumen</th>
                            @endif
                            <th>Produk</th>
                            <th>Jumlah (Yard)</th>
                            <th>Jumlah (Pcs)</th>
                            @if ($level != 'konsumen')
                            <th>Alamat</th>
                            @endif
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($pengirimans as $pengiriman)
                        <tr>
                            <td>{{ $pengiriman->kode_pengiriman }}</td>
                            <td>{{ date('d-m-Y', strtotime($pengiriman->tanggal_kirim)) }}</td>
                            <td>{{ $pengiriman->status == 2 ? date('d-m-Y', strtotime($pengiriman->tanggal_terima)) : '-'}}</td>

                            @if ($level != 'konsumen')
                            <td>{{ $pengiriman->pesanan->konsumen->user->nama }}</td>
                            @endif
                            <td>{{ $pengiriman->pesanan->produk->nama }}</td>
                            <td>{{ $pengiriman->jumlah }} (yard)</td>
                            <td>{{ $pengiriman->jumlah / 100 }} (pcs)</td>
                            @if ($level != 'konsumen')
                            <td>{{ $pengiriman->pesanan->konsumen->user->alamat }}</td>
                            @endif
                            <td>
                              <span class="label label-{{ $status[$pengiriman->status][1] }}">
                                {{ $status[$pengiriman->status][0] }}
                              </span>
                            </td>
                            <td>
                              @if ($level == 'konsumen' && $pengiriman->status == 1)
                              <a href="{{ route('pengiriman.terima', $pengiriman) }}">
                                  <span class="label label-success">
                                    Terima
                                  </span>
                              </a>
                              @endif
                              @if ($level == 'distribusi' && $pengiriman->status == 0)
                              <a href="{{ route('pengiriman.proses', $pengiriman) }}">
                                  <span class="label label-primary">
                                    Kirim
                                  </span>
                              </a>
                              @endif
                              @if ($level == 'distribusi' && $pengiriman->status != 0)
                              <a target="_blank" href="{{ route('pengiriman.cetaksj', $pengiriman) }}">
                                  <span class="label label-primary">
                                    Cetak
                                  </span>
                              </a>
                              @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

