@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Laporan Pengiriman</h4>
                    <span>Cetak laporan pengiriman per bulan</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Laporan</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Pengiriman</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form target="_blank" action="{{ route('laporan.pengiriman.cetak') }}" method="post" enctype="multipart/form-data" class="j-pro" id="j-pro" novalidate="" enctype="multipart/form-data">
                            @csrf
                            <div class="j-content">
                                <!-- start name -->
                                <div class="j-unit j-span6">
                                    <label class="j-label">Tahun</label>
                                    <div class="j-input">
                                        <label class="j-icon-right" for="name">
                                            <i class="icofont icofont-box"></i>
                                        </label>
                                        <select name="year">
                                          @for ($i = 2015; $i < 2025; $i++)
                                              <option value="{{$i}}" {{ date('Y') == $i ? 'selected' : ''}}>{{$i}}</opion>
                                          @endfor
                                        </select>
                                    </div>
                                </div>
                                <!-- end name -->

                                <!-- start name -->
                                <div class="j-unit j-span6">
                                  <label class="j-label">Bulan</label>
                                  <div class="j-input">
                                      <label class="j-icon-right" for="name">
                                          <i class="icofont icofont-box"></i>
                                      </label>
                                      <select name="month">
                                        @php
                                            $months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                        @endphp
                                        @foreach($months as $index => $month)
                                            <option value="{{$index < 10 ? 0: ''}}{{$index + 1}}">{{$month}}</opion>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                              <!-- end name -->

                                <!-- start response from server -->
                                <div class="j-response"></div>
                                <!-- end response from server -->
                            </div>
                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Cetak</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
