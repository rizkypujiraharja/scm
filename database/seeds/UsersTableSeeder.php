<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\{User, Pegawai};

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
            0 =>
            array (
              'id' => 1,
              'nama' => 'Ervan',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => '-',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'administrator@scm.com',
              'created_at' => '2018-11-26 21:07:55',
              'updated_at' => '2018-11-26 21:08:34',
            ),
            1 =>
            array (
              'id' => 2,
              'nama' => 'Agus Suhendar',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => '-',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'manager@scm.com',
              'created_at' => '2018-11-26 21:09:13',
              'updated_at' => '2018-11-26 21:09:13',
            ),
            2 =>
            array (
              'id' => 3,
              'nama' => 'Oma R',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => '-',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'gudang@scm.com',
              'created_at' => '2018-11-26 21:10:31',
              'updated_at' => '2018-11-26 21:10:31',
            ),
            3 =>
            array (
              'id' => 5,
              'nama' => 'Acung',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => '-',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'pengadaan@scm.com',
              'created_at' => '2018-11-26 21:12:53',
              'updated_at' => '2018-11-26 21:12:53',
            ),
            4 =>
            array (
              'id' => 6,
              'nama' => 'Antonius',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => '-',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'produksi@scm.com',
              'created_at' => '2018-11-26 21:13:39',
              'updated_at' => '2018-11-26 21:13:39',
            ),
            5 =>
            array (
              'id' => 7,
              'nama' => 'Ato H',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => '-',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'pengiriman@scm.com',
              'created_at' => '2018-11-26 21:14:18',
              'updated_at' => '2018-11-26 21:14:18',
            ),
            6 =>
            array (
              'id' => 8,
              'nama' => 'PT. Aswindo',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Leuwigajah, Bandung. Attn: Ko Cincin',
              'kontak' => '7947654',
              'status' => 1,
              'foto' => 'pegawai-1543247622.jpg',
              'email' => 'konsumen@scm.com',
              'created_at' => '2018-11-26 21:19:49',
              'updated_at' => '2018-11-26 22:53:43',
            ),
            7 =>
            array (
              'id' => 9,
              'nama' => 'Sari Sandang',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Saritem No.43, Bandung, Indonesia',
              'kontak' => '7947581',
              'status' => 1,
              'foto' => NULL,
              'email' => 'konsumen2@scm.com',
              'created_at' => '2018-11-26 21:20:44',
              'updated_at' => '2018-11-26 21:20:44',
            ),
            8 =>
            array (
              'id' => 10,
              'nama' => 'Mitra Abadi',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Raya Laswi No.62 Majalaya - Bandung',
              'kontak' => '7947582',
              'status' => 1,
              'foto' => NULL,
              'email' => 'konsumen3@scm.com',
              'created_at' => '2018-11-26 21:21:18',
              'updated_at' => '2018-11-26 21:21:18',
            ),
            9 =>
            array (
              'id' => 11,
              'nama' => 'Indorama',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Raya Batujajar Km. 5.5, Padalarang,Bandung 40561',
              'kontak' => '78485123',
              'status' => 1,
              'foto' => NULL,
              'email' => 'konsumen4@scm.com',
              'created_at' => '2018-11-26 21:22:25',
              'updated_at' => '2018-11-26 21:22:25',
            ),
            10 =>
            array (
              'id' => 12,
              'nama' => 'Indorama',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Raya Batujajar Km. 5.5, Padalarang,Bandung 40561',
              'kontak' => '78485123',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier@scm.com',
              'created_at' => '2018-11-26 21:49:19',
              'updated_at' => '2018-11-26 21:49:19',
            ),
            11 =>
            array (
              'id' => 13,
              'nama' => 'CFM',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Raya Rancaekek - Majalaya 389, Bandung',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier2@scm.com',
              'created_at' => '2018-11-26 21:49:36',
              'updated_at' => '2018-11-26 21:49:36',
            ),
            12 =>
            array (
              'id' => 14,
              'nama' => 'Polyfin',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Raya Rancaekek Km.19 No.28 Desa Cipacing Kab.Sumedang 45363 - Indonesia',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier3@scm.com',
              'created_at' => '2018-11-26 21:49:57',
              'updated_at' => '2018-11-26 21:49:57',
            ),
            13 =>
            array (
              'id' => 15,
              'nama' => 'Sipatex',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Raya Laswi No.101 Majalaya Kab.Bandung 40382, Jawa Barat - Indonesia',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier4@scm.com',
              'created_at' => '2018-11-26 21:50:16',
              'updated_at' => '2018-11-26 21:50:16',
            ),
            14 =>
            array (
              'id' => 16,
              'nama' => 'Sulindafin',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Karasak Barak Komplek Mekar Wangi, Bandung 40243, Indonesia',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier5@scm.com',
              'created_at' => '2018-11-26 21:50:38',
              'updated_at' => '2018-11-26 21:50:38',
            ),
            15 =>
            array (
              'id' => 20,
              'nama' => 'Texmaco',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Pusat Textile, Jl. Kebon Jati 88 Kav.E 7-8, Bandung',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier7@scm.com',
              'created_at' => '2018-11-26 21:54:21',
              'updated_at' => '2018-11-26 21:54:21',
            ),
            16 =>
            array (
              'id' => 21,
              'nama' => 'China',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jalan Cisirung No.97 Bandung - 40238, Indonesia',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier8@scm.com',
              'created_at' => '2018-11-26 21:54:39',
              'updated_at' => '2018-11-26 21:54:39',
            ),
            17 =>
            array (
              'id' => 22,
              'nama' => 'BRAGATEX',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Leuwigajah No.106-B, Cimahi, Bandung 40522 - Jawa Barat',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier9@scm.com',
              'created_at' => '2018-11-26 21:54:59',
              'updated_at' => '2018-11-26 21:54:59',
            ),
            18 =>
            array (
              'id' => 23,
              'nama' => 'MARFOZOL',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Raya Banjaran Km.14,7 Bandung, Jawa Barat',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier10@scm.com',
              'created_at' => '2018-11-26 21:55:13',
              'updated_at' => '2018-11-26 21:55:13',
            ),
            19 =>
            array (
              'id' => 25,
              'nama' => 'CV SEUNG BIN',
              'jenis_kelamin' => 'Laki-Laki',
              'alamat' => 'Jl. Pasir Panjang No.15, Bandung, Jawa Barat',
              'kontak' => '-',
              'status' => 1,
              'foto' => NULL,
              'email' => 'supplier6@scm.com',
              'created_at' => '2018-11-26 21:56:22',
              'updated_at' => '2018-11-26 21:56:22',
            ),
          );
        foreach ($users as $data) {
            $user = new User;
            $user->id = $data['id'];
            $user->nama = $data['nama'];
            $user->jenis_kelamin = $data['jenis_kelamin'];
            $user->alamat = $data['alamat'];
            $user->kontak = $data['kontak'];
            $user->email = $data['email'];
            $user->password = bcrypt('secret');

            $user->save();
        }

        // $pegawais =
    }
}
