<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBahanBakuSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahan_baku_supplier', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_bahan_baku', 20);
            $table->string('kode_supplier', 20);
            $table->integer('harga')->unsigned();
            $table->integer('min_order')->unsigned();
            $table->float('stock', 12, 3);

            $table->foreign('kode_bahan_baku')->references('kode_bahan_baku')->on('bahan_baku');
            $table->foreign('kode_supplier')->references('kode_supplier')->on('supplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahan_baku_supplier');
    }
}
