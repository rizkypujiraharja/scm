@extends('layouts.app')

@section('content')
<div class="page-header card">
  <div class="row align-items-end">
      <div class="col-lg-8">
          <div class="page-header-title">
              <i class="icofont icofont-box bg-c-green"></i>
              <div class="d-inline">
              <h4>Pilih Produk</h4>
                  <span>Pesanan (Bahan Baku Konsumen)</span>
              </div>
          </div>
      </div>
      <div class="col-lg-4">
          <div class="page-header-breadcrumb">
              <ul class="breadcrumb-title">
                  <li class="breadcrumb-item">
                      <a href="{{ route('index') }}">
                          <i class="icofont icofont-home"></i>
                      </a>
                  </li>
                  <li class="breadcrumb-item"><a href="{{ route('pesanan.index') }}">Pesanan</a>
                  </li>
                  <li class="breadcrumb-item"><a href="{{ route('pesanan.index') }}">Pilih Produk</a>
                  </li>
              </ul>
          </div>
      </div>
  </div>
</div>

<div class="page-body">
  <div class="card">
      <div class="row">
        @foreach ($produks as $produk)
          <div class="col-md-4">
            <a href="{{ route('pesanan.bb.konsumen.create', $produk->kode_produk) }}">
              <img src="{{ $produk->getGambar() }}" class="img-responsive" style="padding:15px;width:100%">
              <center><p>{{ $produk->nama }}</p></center>
            </a>
          </div>
        @endforeach
      </div>
  </div>
</div>
@endsection
