<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('tes', 'HomeController@tes');

Auth::routes();

Route::get('/register', function(){
   return "Disable"; 
});
Route::get('/logout', 'Auth\LoginController@logout')->name('logout')->middleware(['auth']);



Route::group(['middleware' => 'auth'], function(){

    Route::get('/home', 'HomeController@index')->name('index')->middleware(['level:pegawai|konsumen|supplier']);

    Route::resource('pegawai', 'PegawaiController')->middleware(['level:pegawai','jabatan:admin']);
    Route::resource('konsumen', 'KonsumenController')->middleware(['level:pegawai','jabatan:admin|manager']);
    Route::resource('produk', 'ProdukController')->middleware(['level:pegawai','jabatan:admin|manager|produksi|gudang']);
    Route::resource('supplier', 'SupplierController')->middleware(['level:pegawai','jabatan:admin|manager|purchasing']);
    Route::resource('kendaraan', 'KendaraanController')->middleware(['level:pegawai','jabatan:admin|manager|distribusi']);
    Route::get('kendaraan/switch/{id}', 'KendaraanController@switchStatus')->name('kendaraan.switch');

    Route::resource('bahan-baku-supplier', 'BahanBakuSupplierController')->middleware(['level:pegawai|supplier','jabatan:admin|manager|gudang|purchasing']);

    Route::group(['prefix' => 'bahan-baku/penerimaan', 'middleware' => ['level:pegawai','jabatan:admin|manager|gudang']], function() {
        Route::get('/', 'BahanBakuController@penerimaan')->name('penerimaan.index');
        Route::get('/terima/{id}', 'BahanBakuController@terima')->name('penerimaan.terima');
    });
    Route::resource('bahan-baku', 'BahanBakuController')->middleware(['level:pegawai','jabatan:admin|manager|gudang|supplier|distribusi|purchasing']);

    Route::get('/bom/get-ajax', 'BOMController@getAjax')->name('bom.ajax');
    Route::resource('bom', 'BOMController')->middleware(['level:pegawai','jabatan:admin|manager|produksi']);

    Route::group(['prefix' => 'pesanan', 'middleware' => ['level:pegawai|konsumen', 'jabatan:admin|manager|produksi']], function() {
        Route::get('/proses/{id}', 'PesananController@proses')->name('pesanan.proses');
    	Route::get('/tolak/{id}', 'PesananController@tolak')->name('pesanan.tolak');
    	Route::get('/selesai/{id}', 'PesananController@selesai')->name('pesanan.selesai');
    	Route::get('/detail/{id}', 'PesananController@detail')->name('pesanan.detail');

        Route::get('/index', 'PesananController@index')->name('pesanan.index');

        Route::get('/design-baru', 'PesananDesignBaruController@index')->name('pesanan.design.baru.index');
        Route::get('/design-baru/create', 'PesananDesignBaruController@create')->name('pesanan.design.baru.create');
        Route::post('/design-baru/store', 'PesananDesignBaruController@store')->name('pesanan.design.baru.store');
        Route::get('/design-baru/proses/{id}', 'PesananDesignBaruController@proses')->name('pesanan.design.baru.proses');
        Route::get('/design-baru/tolak/{id}', 'PesananDesignBaruController@tolak')->name('pesanan.design.baru.tolak');
        Route::post('/design-baru/buat/{id}', 'PesananDesignBaruController@buatPesanan')->name('pesanan.design.baru.buat');

        Route::get('/{bb}/produk', 'PesananController@produk')->name('pesanan.produk');

        Route::get('/bb-perusahaan/create/{kode}', 'PesananController@BBPerusahaanCreate')->name('pesanan.bb.perusahaan.create');
        Route::post('/bb-perusahaan/store', 'PesananController@BBPerusahaanStore')->name('pesanan.bb.perusahaan.store');

        Route::get('/bb-konsumen/create/{kode}', 'PesananController@BBKonsumenCreate')->name('pesanan.bb.konsumen.create');
        Route::post('/bb-konsumen/store', 'PesananController@BBKonsumenStore')->name('pesanan.bb.konsumen.store');

        Route::get('/cetak/{id}', 'PesananController@cetak')->name('pesanan.cetak');
    });

    Route::group(['prefix' => 'pengadaan', 'middleware' => ['level:pegawai', 'jabatan:admin|manager|gudang|purchasing']], function() {
        Route::get('/', 'PengadaanController@index')->name('pengadaan.index');
        Route::get('/pesanan', 'PengadaanController@indexPesanan')->name('pengadaan.index-pesanan');
        Route::get('/detail/{id}', 'PengadaanController@detail')->name('pengadaan.detail');
        Route::get('/setujui/{id}', 'PengadaanController@setujui')->name('pengadaan.setujui');
        Route::get('/tolak/{id}', 'PengadaanController@tolak')->name('pengadaan.tolak');
        Route::get('/create/', 'PengadaanController@create')->name('pengadaan.create');
        Route::post('/store/', 'PengadaanController@store')->name('pengadaan.store');
    });

    Route::get('/profile', 'ProfileController@form')->name('profile');
    Route::put('/profile', 'ProfileController@update');


    Route::group(['prefix' => 'penjualan', 'middleware' => ['level:supplier']], function() {
        Route::get('/', 'PenjualanBahanBakuController@index')->name('supplier.penjualan.index');
        Route::get('/pengiriman', 'PenjualanBahanBakuController@pengiriman')->name('supplier.pengiriman');
        Route::get('/{id}', 'PenjualanBahanBakuController@kirim')->name('supplier.penjualan.kirim');
    });

    Route::group(['prefix' => 'produksi', 'middleware' => ['level:pegawai|konsumen', 'jabatan:admin|produksi|gudang']], function() {
        Route::get('/', 'ProduksiController@index')->name('produksi.index');
        Route::get('/pesanan/{id}', 'ProduksiController@indexPesanan')->name('produksi.index-pesanan');
        Route::get('/detail/{id}', 'ProduksiController@detail')->name('produksi.detail');
        Route::get('/setujui/{id}', 'ProduksiController@setujui')->name('produksi.setujui');
        Route::get('/selesai/{id}', 'ProduksiController@selesai')->name('produksi.selesai');
        Route::get('/create/{id}', 'ProduksiController@create')->name('produksi.create');
        Route::post('/store/{id}', 'ProduksiController@store')->name('produksi.store');
    });

    Route::group(['prefix' => 'pengiriman'], function () {
        Route::get('/', 'PengirimanController@index')->name('pengiriman.index');
        Route::get('/pesanan/{id}', 'PengirimanController@indexPesanan')->name('pengiriman.pesanan');
        Route::get('/create/{id}', 'PengirimanController@create')->name('pengiriman.create');
        Route::post('/store/{id}', 'PengirimanController@store')->name('pengiriman.store');
        Route::get('/proses/{id}', 'PengirimanController@proses')->name('pengiriman.proses');
        Route::post('/update/{id}', 'PengirimanController@update')->name('pengiriman.update');
        Route::get('/terima/{id}', 'PengirimanController@terima')->name('pengiriman.terima');
        Route::get('/request/{id}', 'PengirimanController@requestPengiriman')->name('pengiriman.request');
        Route::get('/cetak/{id}', 'PengirimanController@cetakSJ')->name('pengiriman.cetaksj');
    });

    Route::group(['prefix' => 'laporan'], function () {
        Route::get('/pesanan', 'LaporanController@pesanan')->name('laporan.pesanan');
        Route::post('/pesanan/cetak', 'LaporanController@pesananCetak')->name('laporan.pesanan.cetak');
        Route::get('/pengiriman', 'LaporanController@pengiriman')->name('laporan.pengiriman');
        Route::post('/pengiriman/cetak', 'LaporanController@pengirimanCetak')->name('laporan.pengiriman.cetak');
        Route::get('/pengadaan', 'LaporanController@pengadaan')->name('laporan.pengadaan');
        Route::post('/pengadaan/cetak', 'LaporanController@pengadaanCetak')->name('laporan.pengadaan.cetak');
    });


    Route::get('/', function() {
        return redirect()->route('index');
    });


});
