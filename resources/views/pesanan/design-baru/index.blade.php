@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Data Pesanan Design Baru</h4>
                    <span>Manage Data Design Baru</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pesanan.index') }}">Pesanan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
            @if(session('user_level') != 'produksi')
        <div class="card-header">
            <a href="{{ route('pesanan.design.baru.create') }}"><button class="btn btn-sm btn-primary">Tambah Pesanan Design Baru</button></a>
        </div>
            @endif
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Konsumen</th>
                            <th>Produk</th>
                            <th>Jumlah</th>
                            <th>Design</th>
                            <th>Tanggal Pesan</th>
                            <th>Tanggal Disetujui</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                                <td></td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[nopol]" value="{{ $param['nopol'] ?? '' }}">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm" name="filter[kapasitas]" value="{{ $param['kapasitas'] ?? ''}}">
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('pesanan.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr> --}}
                    @foreach ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $pesanan->id }}</td>
                            <td>{{ $pesanan->konsumen->user->nama }}</td>
                            <td>{{ $pesanan->nama }}</td>
                            <td>{{ rupiah($pesanan->jumlah) }}</td>
                            <td>
                                <a href="{{ $pesanan->getDesign() }}" target="_blank">
                                <img src="{{ $pesanan->getDesign() }}" style="width: 50px; height: 50px">
                                </a>
                            </td>
                            <td>{{ date('d-m-Y', strtotime($pesanan->tanggal)) }}</td>
                            <td>{{ !is_null($pesanan->tanggal_setuju) ? date('d-m-Y', strtotime($pesanan->tanggal_setuju)) : '-' }}</td>
                            <td align="center"><span class="label label-{{$status[$pesanan->status][1]}}">{{$status[$pesanan->status][0]}}</span></td>
                            <td>
                                @if(session('user_level') == 'produksi' && $pesanan->status == 0)
                                <a href="{{ route('pesanan.design.baru.proses', $pesanan) }}">
                                    <button class="btn btn-mini btn-primary">Proses</button>
                                </a>

                                <a href="{{ route('pesanan.design.baru.tolak', $pesanan) }}">
                                    <button class="btn btn-mini btn-danger">Tolak</button>
                                </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $pesanans->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus pesanan bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
