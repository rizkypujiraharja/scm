<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
	protected $table = 'produk';

	public $timestamps = false;

	protected $primaryKey = 'kode_produk';

	public $incrementing = false;

	public function bom()
	{
		return $this->hasMany('App\BOM', 'kode_produk');
    }

    public function getGambar() {
        return url('/images/produk/'.$this->gambar);
    }
}
