<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengadaan extends Model
{
	const status = [
			['Menunggu Persetujuan', 'warning'],
			['Telah Disetujui', 'success'],
			['Tidak Disetujui', 'danger']
    	];

	protected $table = 'pengadaan';

    public $timestamps = false;
    
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    public function pesanan(){
    	return $this->belongsTo('App\Pesanan');
    }

    public function eoq(){
    	return $this->hasMany('App\EOQ');
    }
}
