<?php

namespace App\Http\Controllers;

use App\{Pesanan, Produk, Konsumen, BOM, BahanBaku, BahanBakuSupplier, PesananDesignBaru, Pengadaan, PermintaanBahanBaku};
use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth, Image, PDF;
use Exception, DB;

class PesananController extends Controller
{
    public function index()
    {
        if(Auth::user()->konsumen()->exists()){
            $pesanans = Pesanan::where('konsumen_id', Auth::user()->konsumen->id)->orderBy('id', 'DESC')->paginate(10);
        }else{
            $pesanans = Pesanan::orderBy('id', 'DESC')->paginate(10);
        }
        $status = Pesanan::status;

        return view('pesanan.index', compact('pesanans', 'status'));
    }

    public function produk($bb) {

        $produks = Produk::get();

        if ( $bb == 'bb-perusahaan' ) {
            $from = 'Perusahaan';
            return view('pesanan.bb-perusahaan.produk', compact(['produks']));
        } else {
            $from = 'Konsumen';
            return view('pesanan.bb-konsumen.produk', compact(['produks']));
        }

    }

    public function BBPerusahaanCreate($kode)
    {
        $produk = Produk::findOrFail($kode);

        $konsumens = Konsumen::get();

        return view('pesanan.bb-perusahaan.create', compact('produk', 'konsumens'));
    }

    public function BBPerusahaanStore(Request $request)
    {
        $this->validate($request, [
            'jumlah' => 'numeric|min:30000',
        ],[
            'jumlah.min' => 'Pemesanan minimal 30000'
        ]);

        $produk = Produk::where('kode_produk', $request->produk)->first();

        if($request->konsumen == 'konsumen'){
            $konsumen = Auth::user()->konsumen->id;
            $status = 0;
            $pegawai = 1;
            $tgl_selesai = null;
        }else{
            $konsumen = $request->konsumen;
            $status = 1;
            $pegawai = Auth::user()->id;

            $max = $produk->max_produksi_per_hari;

            $hari['pengadaan'] = 2;
            $hari['weaving'] = ceil($request->jumlah/$max);
            $hari['lusiPakan'] = 2;
            $hari['finishing'] = 2;
            $hari['pengiriman'] = 2;

            $estimasi = (int)array_sum($hari);
            $tgl_selesai = Carbon::now()->addDays($estimasi)->toDateString();
        }

        $n = explode(' ', $request->name);
            $kode = 'G-'.date('ym');

            $cek = Pesanan::where('id', 'like', $kode.'.%')->orderBy('id', 'DESC')->first();

            if( is_null($cek) ){
                $kode = $kode.'.01';
            }else{
                $last = explode('.', $cek->id);
                $new = sprintf("%'02d", $last[1]+1);
                $kode = $kode.'.'.$new;
            }

        $pesanan = New Pesanan;
        $pesanan->id = $kode;
        $pesanan->pegawai_id = $pegawai;
        $pesanan->konsumen_id = $konsumen;
        $pesanan->kode_produk = $request->produk;
        $pesanan->jumlah = $request->jumlah;
        $pesanan->total_harga = $request->jumlah * $produk->harga;
        $pesanan->jenis = 'bbperusahaan';
        $pesanan->status = $status;
        $pesanan->tgl_pesan = date('Y-m-d');
        $pesanan->tgl_selesai = $tgl_selesai;
        $pesanan->save();

        return redirect()->route('pesanan.index')->with('alert-success', 'Berhasil Menambah Pesanan');
    }


    public function BBKonsumenCreate($kode)
    {
        $produk = Produk::findOrFail($kode);

        $konsumens = Konsumen::get();

        return view('pesanan.bb-konsumen.create', compact('produk', 'konsumens'));
    }

    public function BBKonsumenStore(Request $request)
    {
        $this->validate($request, [
            'jumlah' => 'numeric|min:30000',
        ],[
            'jumlah.min' => 'Pemesanan minimal 30000'
        ]);

        $produk = Produk::where('kode_produk', $request->produk)->firstOrFail();

        if($request->konsumen == 'konsumen'){
            $konsumen = Auth::user()->konsumen->id;
            $status = 0;
            $pegawai = 1;
            $tgl_selesai = null;
        }else{
            $konsumen = $request->konsumen;
            $status = 1;
            $pegawai = Auth::user()->id;

            $max = $produk->max_produksi_per_hari;

            $hari['pengadaan'] = 2;
            $hari['weaving'] = ceil($request->jumlah/$max);
            $hari['lusiPakan'] = 2;
            $hari['finishing'] = 2;
            $hari['pengiriman'] = 2;

            $estimasi = (int)array_sum($hari);
            $tgl_selesai = Carbon::now()->addDays($estimasi)->toDateString();
        }

         $n = explode(' ', $request->name);
            $kode = 'G-'.date('ym');

            $cek = Pesanan::where('id', 'like', $kode.'%')->orderBy('id', 'DESC')->first();

            if( is_null($cek) ){
                $kode = $kode.'.01';
            }else{
                $last = explode('.', $cek->id);
                $new = sprintf("%'02d", $last[1]+1);
                $kode = $kode.'.'.$new;
            }

        $pesanan = New Pesanan;
        $pesanan->id = $kode;
        $pesanan->pegawai_id = $pegawai;
        $pesanan->konsumen_id = $konsumen;
        $pesanan->kode_produk = $request->produk;
        $pesanan->jumlah = $request->jumlah;
        
        if(count($request->bom) > 0){
            $minus = 0;
            foreach ($request->bom as $bom) {
                $data = BOM::findOrFail($bom);
                $bahanBakuTermurah = BahanBakuSupplier::where('kode_bahan_baku', $data->kode_bahan_baku)->orderBy('harga', 'ASC')->first();
                $minus += $bahanBakuTermurah->harga * $request->jumlah * $data->jumlah;
                if($request->konsumen != 'konsumen'){
                    $addbb = BahanBaku::where('kode_bahan_baku', $bahanBakuTermurah->kode_bahan_baku)->first();
                    $addbb->stock = $addbb->stock + ($data->jumlah * $request->jumlah + ($data->jumlah * $request->jumlah * 10/100) );
                    $addbb->save();
                }
            }

            $boms = BOM::where('kode_produk', $request->produk)->whereNotIn('id', $request->bom)->get();

            if( count($boms) < 1 ){
                $status = $status == 1 ? 4 : 0 ;
            }
        }
        
        $pesanan->minus_harga_bb_konsumen = $minus;
        $pesanan->total_harga = ($request->jumlah * $produk->harga) - $minus;
        $pesanan->jenis = 'bbkonsumen';
        $pesanan->status = $status;
        $pesanan->tgl_pesan = date('Y-m-d');
        $pesanan->tgl_selesai = $tgl_selesai;
        $pesanan->save();

        return redirect()->route('pesanan.index')->with('alert-success', 'Berhasil Menambah Pesanan');
    }

    public function detail($id)
    {
        $pesanan = Pesanan::where('id', $id)->first();
        $status = Pesanan::status;

        $boms = BOM::select('id')->where('kode_produk', $pesanan->kode_produk)->first()->toArray();
        return view('pesanan.view', compact('pesanan', 'status'));
    }

    public function proses($id)
    {
        $pesanan = Pesanan::where('status', 0)->where('id', $id)->first();

        if(is_null($pesanan)) return redirect()->back()->with('alert-error', 'Pesanan tidak ditemukan');

        $max = $pesanan->produk->max_produksi_per_hari;

        $hari['pengadaan'] = 2;
        $hari['weaving'] = ceil($pesanan->jumlah/$max);
        $hari['lusiPakan'] = 2;
        $hari['finishing'] = 2;
        $hari['pengiriman'] = 2;

        $estimasi = (int)array_sum($hari);

        $pesanan->status = 1;
        $pesanan->tgl_selesai = Carbon::now()->addDays($estimasi)->toDateString();

        if( $pesanan->jenis == 'bbkonsumen' ){
            $pesanan->status = 4;
            $boms = BOM::where('kode_produk', $pesanan->kode_produk)->get();
            foreach ($boms as $bom) {
                $addbb = BahanBaku::where('kode_bahan_baku', $bom->kode_bahan_baku)->first();
                $addbb->stock = $addbb->stock + ($bom->jumlah * $pesanan->jumlah);
                $addbb->save();
            }
        }

        $pesanan->save();

        return redirect()->back()->with('alert-success', 'Pesanan #'.$id.' diproses!');
    }

    public function tolak($id)
    {
        $pesanan = Pesanan::where('status', 0)->where('id', $id)->first();

        if(is_null($pesanan)) return redirect()->back()->with('alert-error', 'Pesanan tidak ditemukan');

        $pesanan->status = 3;
        $pesanan->save();

        return redirect()->back()->with('alert-success', 'Pesanan #'.$id.' ditolak!');
    }

    public function selesai($id)
    {
        $pesanan = Pesanan::where('status', 6)->where('id', $id)->firstOrFail();

        if ( $pesanan->jenis == 'bbperusahaan' ) {
            $pengadaan = Pengadaan::where('pesanan', 'like', '%'.$pesanan->id.'%')
                ->where('status', 1)
                ->first();
            $eoqs = $pengadaan->eoq()->get();
        }

        DB::beginTransaction();
        try {

            $pesanan->status = 2;
            $pesanan->save();

            foreach ($eoqs as $eoq) {

                $kebutuhan = $pesanan->jumlah - $eoq->sisa_stock;
                $biayaSimpan = 0.05 * $eoq->harga;
                $jumlahStock = ceil( sqrt( (2 * $kebutuhan * 500000) / $biayaSimpan))  + ($pesanan->jumlah * 10/100);

                $produksi = $pesanan->produksi()->select('id')->where('status', 1)->get();
                $jumlahDipakai = PermintaanBahanBaku::whereIn('produksi_id', $produksi)->sum('jumlah');

                $bb = BahanBaku::where('kode_bahan_baku', $eoq->kode_bahan_baku)->first();
                $bb->stock_sisa = $jumlahStock - $jumlahDipakai;
                $bb->save();

            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('alert-error', 'Terjadi Kesalahan!');
        }

        return redirect()->back()->with('alert-success', 'Pesanan #'.$id.' sudah selesai!');
    }

    public function designBaruIndex()
    {
        $pesanans = PesananDesignBaru::orderBy('id', 'DESC')->paginate(15);

        $status = PesananDesignBaru::status;

        return view('pesanan.design-baru.index',compact('pesanans', 'status'));
    }

    public function designBaruCreate()
    {
        $konsumens = Konsumen::get();

        return view('pesanan.design-baru.create', compact('konsumens'));
    }

    public function designBaruStore(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'jumlah' => 'numeric|min:30000',
            'design'    => 'required|file'
        ],[
            'jumlah.min' => 'Pemesanan minimal 30000'
        ]);

        $imgname = Null;
        if( !is_null($request->design) ){
            $imageLocation = public_path('images/design').'/';

            $imgname = 'design-'.time().'.'.$request->design->getClientOriginalExtension();
            Image::make($request->design)
                    ->save($imageLocation .$imgname);
        }

        $design = new PesananDesignBaru;

        $design->konsumen_id = $request->konsumen;
        $design->nama = $request->nama;
        $design->design = $imgname;
        $design->jumlah = $request->jumlah;
        $design->tanggal = date('Y-m-d');
        $design->status = 0;

        $design->save();

        return redirect()->route('pesanan.design.baru.index')->with('alert-success', 'Berhasil menambah pesanan design baru');
    }

    public function cetak($id)
    {
        $pesanan = Pesanan::where('id', $id)->first();
        $status = Pesanan::status;

        $boms = BOM::where('kode_produk', $pesanan->kode_produk)->get();
        
        $pdf = PDF::loadView('cetak.pesanan', compact('pesanan', 'status', 'boms'))->setPaper('a4', 'portrait');
        return $pdf->stream('.pdf');
    }
}
