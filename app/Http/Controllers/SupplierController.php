<?php

namespace App\Http\Controllers;

use App\{Supplier, User};
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Image;
use Exception, DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = $request->filter;

        $suppliers = Supplier::join('users', 'users.id', '=', 'supplier.user_id')
                ->select('supplier.kode_supplier as id', 'users.nama', 'users.email', 'users.kontak', 'users.alamat')
                ->where('supplier.kode_supplier', 'like', '%' . ($request->filter['kode'] ?? '')  . '%')
                ->where('users.nama', 'like', '%' . ($request->filter['nama'] ?? '')  . '%')
                ->where('users.email', 'like', '%' . ($request->filter['email'] ?? '') . '%')
                ->where('users.kontak', 'like', '%' . ($request->filter['kontak'] ?? '') . '%')
                ->where('users.alamat', 'like', '%' . ($request->filter['alamat'] ?? '')  . '%')
                ->paginate(10);

        return view('supplier.index', compact('suppliers', 'param'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|alpha',
            'email' => 'email|unique:users,email',
            'password' => 'required|string|min:6',
            'kontak' => 'required|numeric',
            'foto'  => 'nullable|image',
            'alamat' => 'required|string|max:200',
        ]);

        $imgname = Null;
        if( !is_null($request->foto) ){
            $imageLocation = public_path('images/foto').'/';

            $imgname = 'supplier-'.time().'.'.$request->foto->getClientOriginalExtension();
            Image::make($request->foto)->resize(300, 300)
                    ->save($imageLocation .$imgname);
        }

        DB::beginTransaction();
        try {

            $user = new User;
            $user->nama = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->jenis_kelamin = $request->jeniskelamin;
            $user->alamat = $request->alamat ?? '-';
            $user->kontak = $request->kontak ?? '-';
            $user->foto = $imgname;
            $user->save();

            $supplier = New Supplier;
            $supplier->user_id = $user->id;
            $supplier->kode_supplier = 'S_'.ucwords($request->name[0]).substr(ucwords($request->name), -1);
            $supplier->save();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        return redirect()->route('supplier.index')->with('alert-success', 'Berhasil menambah supplier!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);
        return view('supplier.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);

        $user = User::findOrFail($supplier->user_id);

        $this->validate($request, [
            'name' => 'required|min:4|max:50|alpha',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            'password' => 'nullable|string|min:6',
            'kontak' => 'required|numeric',
            'foto'  => 'nullable|image',
            'alamat' => 'required|string|max:200'
        ]);

        DB::beginTransaction();
        try {

            $imgname = Null;

            if( !is_null($request->foto) ){
                $imageLocation = public_path('images/foto').'/';

                $img = $user->foto;
                !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

                $imgname = 'supplier-'.time().'.'.$request->foto->getClientOriginalExtension();
                Image::make($request->foto)->resize(300, 300)
                        ->save($imageLocation .$imgname);
            }

            $supplier->save();

            $user->nama = $request->name;
            $user->email = $request->email;
            if(!is_null($request->password))
                $user->password = bcrypt($request->password);
            $user->jenis_kelamin = $request->jeniskelamin;
            $user->alamat = $request->alamat ?? '-';
            $user->kontak = $request->kontak ?? '-';
            $user->foto = $imgname;
            $user->save();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        return redirect()->route('supplier.index')->with('alert-success', 'Berhasil mengubah supplier!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);
        $user = User::findOrFail($supplier->user_id);

        $img = $user->foto;

        DB::beginTransaction();
        try {

            $supplier->delete();
            $user->delete();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        $imageLocation = public_path('images/foto').'/';
        if (!is_null($img)) !file_exists($imageLocation.$img) or unlink($imageLocation.$img);

        return redirect()->back()->with('alert-success', 'Berhasil menghapus data supplier.');
    }
}
