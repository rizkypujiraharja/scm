<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Kendaraan;

class KendaraanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =array (
            0 =>
            array (
              'id' => 1,
              'nopol' => 'D 7651 VDG',
              'kapasitas' => 300,
              'supir' => 'Novan',
              'status' => '1',
            ),
            1 =>
            array (
              'id' => 2,
              'nopol' => 'D 7884 VAQ',
              'kapasitas' => 300,
              'supir' => 'Iyan',
              'status' => '1',
            ),
            2 =>
            array (
              'id' => 3,
              'nopol' => 'D 4551 WFG',
              'kapasitas' => 300,
              'supir' => 'Asep',
              'status' => '1',
            ),
            3 =>
            array (
              'id' => 4,
              'nopol' => 'D 7184 VSC',
              'kapasitas' => 300,
              'supir' => 'Rudi',
              'status' => '1',
            ),
            4 =>
            array (
              'id' => 5,
              'nopol' => 'D 7712 VAQ',
              'kapasitas' => 300,
              'supir' => 'Supri',
              'status' => '1',
            ),
            5 =>
            array (
              'id' => 6,
              'nopol' => 'D 7637 VDG',
              'kapasitas' => 300,
              'supir' => 'Anto',
              'status' => '1',
            ),
          );
        foreach ($users as $data) {
            $user = new Kendaraan;
            $user->id = $data['id'];
            $user->nopol = $data['nopol'];
            $user->kapasitas = $data['kapasitas'];
            $user->supir = $data['supir'];
            $user->status = $data['status'];

            $user->save();
        }

        // $pegawais =
    }
}
