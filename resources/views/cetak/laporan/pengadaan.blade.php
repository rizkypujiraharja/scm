<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <style>
    body{
      line-height: normal;
      font-family: Arial, Helvetica, sans-serif;
    }
    .table {
      border-collapse: collapse;
      font-size: 11px;
    }

    table {
      font-size: 11px;
    }

    .table th, .table td {
      border: 1px solid black;
    }

    .table th {
      vertical-align: middle;
      text-align: center;
      background-color: lightgray;
      height: 25px;
    }
  </style>
<title>Laporan Pengadaan Bulan {{$times[1]}} {{$times[0]}}</title>
</head>
<body>
<center>PENGADAAN BAHAN BAKU PT. TATA CAKRA INVESTAMA
  <br> {{$times[1]}} {{$times[0]}}
</center>
<hr>
<table class="table">
  <tr>
      <th width="25px">No</th>
      <th width="85px">Tanggal</th>
      <th width="120px">Nama Bahan Baku</th>
      <th width="90px">Harga</th>
      <th width="90px">Jumlah</th>
      <th width="150px">Total Harga</th>
      <th width="120px">Supplier</th>
  </tr>
  @php
      $sumTotalHarga = 0;
  @endphp
  @foreach ($pengadaans as $i => $pengadaan)
    @foreach ($pengadaan->eoq()->get() as $j => $eoq)
      @if ($j == 0)
        @php
            $rows = count($pengadaan->eoq()->get());
        @endphp
        <tr>
            <td rowspan="{{ $rows }}">{{$i+1}}</td>
            <td rowspan="{{ $rows }}">{{ date('d-m-Y', strtotime($pengadaan->tanggal)) }}</td>
            <td>{{$eoq->bahanBaku->nama}}</td>
            <td>Rp. {{rupiah($eoq->harga)}}</td>
            <td>{{rupiah($eoq->jumlah)}}</td>
            <td>Rp. {{rupiah($eoq->jumlah * $eoq->harga)}}</td>
            <td>{{$eoq->supplier->user->nama}}</td>
        </tr>
      @else
      <tr>
        <td>{{$eoq->bahanBaku->nama}}</td>
        <td>Rp. {{rupiah($eoq->harga)}}</td>
        <td>{{rupiah($eoq->jumlah)}}</td>
        <td>Rp. {{rupiah($eoq->jumlah * $eoq->harga)}}</td>
        <td>{{$eoq->supplier->user->nama}}</td>
      </tr>
      @endif
      @php
          $sumTotalHarga += $eoq->jumlah * $eoq->harga;
      @endphp
    @endforeach
  @endforeach
  <tr>
      <td align="center" colspan="5"><b>Total</b></td>
      <td colspan="2"><b>Rp. {{rupiah($sumTotalHarga)}}</b></td>
  </tr>
</table>


<br>
<br>
<table border="0">
<tr>

    <td>
        <div style="margin-left:90px">
            <center>
              <br>
          General Manager
          <br><br><br><br>
          <b><u>{{ $manager->user->nama }}</b></u>
            </center>
          </div>
    </td>
  <td>
      <div style="margin-left:330px">
          <center>
        Bandung, {{ date('d-m-Y') }}<br>
        {{ ucwords($level) }}
        <br><br><br><br>
        <b><u>{{ Auth::user()->nama }}</b></u>
          </center>
        </div>
  </td>
</tr>
</table>
</body>
</html>
