<?php

namespace App\Http\Middleware;

use Closure;

class Jabatan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $jabatan)
    {
        $user = $request->user();
        if($user == null) abort(403);

        if( $user->pegawai()->exists() )
        {
            $userJabatan = $user->pegawai->jabatan;
            $jabatans = explode("|", $jabatan);
            if( empty($userJabatan) || !in_array($userJabatan, $jabatans) ) {
                abort(403);
            }
        }
        return $next($request);
    }
}
