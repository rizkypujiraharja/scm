<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\BahanBakuSupplier;

class BahanBakuSupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array (
            0 =>
            array (
              'id' => 1,
              'kode_bahan_baku' => 'B_DTY-01',
              'kode_supplier' => 'S_Ia',
              'harga' => 24200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            1 =>
            array (
              'id' => 2,
              'kode_bahan_baku' => 'B_DTY-01',
              'kode_supplier' => 'S_Pn',
              'harga' => 24400,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            2 =>
            array (
              'id' => 3,
              'kode_bahan_baku' => 'B_DTY-02',
              'kode_supplier' => 'S_Ia',
              'harga' => 24200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            3 =>
            array (
              'id' => 4,
              'kode_bahan_baku' => 'B_DTY-02',
              'kode_supplier' => 'S_Sx',
              'harga' => 24400,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            4 =>
            array (
              'id' => 5,
              'kode_bahan_baku' => 'B_FDY-01',
              'kode_supplier' => 'S_Ia',
              'harga' => 28200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            5 =>
            array (
              'id' => 6,
              'kode_bahan_baku' => 'B_FDY-01',
              'kode_supplier' => 'S_Sx',
              'harga' => 28000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            6 =>
            array (
              'id' => 7,
              'kode_bahan_baku' => 'B_SDY-01',
              'kode_supplier' => 'S_Ia',
              'harga' => 28600,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            7 =>
            array (
              'id' => 8,
              'kode_bahan_baku' => 'B_SDY-01',
              'kode_supplier' => 'S_CM',
              'harga' => 28800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            8 =>
            array (
              'id' => 9,
              'kode_bahan_baku' => 'B_SDY-02',
              'kode_supplier' => 'S_Ia',
              'harga' => 28700,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            9 =>
            array (
              'id' => 10,
              'kode_bahan_baku' => 'B_SDY-02',
              'kode_supplier' => 'S_CM',
              'harga' => 28500,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            10 =>
            array (
              'id' => 11,
              'kode_bahan_baku' => 'B_PBS-01',
              'kode_supplier' => 'S_Ia',
              'harga' => 24800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            11 =>
            array (
              'id' => 12,
              'kode_bahan_baku' => 'B_PBS-01',
              'kode_supplier' => 'S_Pn',
              'harga' => 24600,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            12 =>
            array (
              'id' => 13,
              'kode_bahan_baku' => 'B_BSY-01',
              'kode_supplier' => 'S_CM',
              'harga' => 21800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            13 =>
            array (
              'id' => 14,
              'kode_bahan_baku' => 'B_BSY-01',
              'kode_supplier' => 'S_Sx',
              'harga' => 21600,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            14 =>
            array (
              'id' => 15,
              'kode_bahan_baku' => 'B_DTY-03',
              'kode_supplier' => 'S_CM',
              'harga' => 29200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            15 =>
            array (
              'id' => 16,
              'kode_bahan_baku' => 'B_DTY-03',
              'kode_supplier' => 'S_To',
              'harga' => 29000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            16 =>
            array (
              'id' => 17,
              'kode_bahan_baku' => 'B_DTY-04',
              'kode_supplier' => 'S_CM',
              'harga' => 29000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            17 =>
            array (
              'id' => 18,
              'kode_bahan_baku' => 'B_DTY-04',
              'kode_supplier' => 'S_To',
              'harga' => 29200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            18 =>
            array (
              'id' => 19,
              'kode_bahan_baku' => 'B_BSY-02',
              'kode_supplier' => 'S_Pn',
              'harga' => 21600,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            19 =>
            array (
              'id' => 20,
              'kode_bahan_baku' => 'B_BSY-02',
              'kode_supplier' => 'S_Sn',
              'harga' => 21800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            20 =>
            array (
              'id' => 21,
              'kode_bahan_baku' => 'B_DTY-05',
              'kode_supplier' => 'S_Pn',
              'harga' => 24000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            21 =>
            array (
              'id' => 22,
              'kode_bahan_baku' => 'B_DTY-05',
              'kode_supplier' => 'S_CN',
              'harga' => 24200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            22 =>
            array (
              'id' => 23,
              'kode_bahan_baku' => 'B_FINE-01',
              'kode_supplier' => 'S_Pn',
              'harga' => 25800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            23 =>
            array (
              'id' => 24,
              'kode_bahan_baku' => 'B_FINE-01',
              'kode_supplier' => 'S_Sn',
              'harga' => 26000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            24 =>
            array (
              'id' => 25,
              'kode_bahan_baku' => 'B_ITY-01',
              'kode_supplier' => 'S_Pn',
              'harga' => 25800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            25 =>
            array (
              'id' => 26,
              'kode_bahan_baku' => 'B_ITY-01',
              'kode_supplier' => 'S_Sn',
              'harga' => 26000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            26 =>
            array (
              'id' => 27,
              'kode_bahan_baku' => 'B_DTY-06',
              'kode_supplier' => 'S_Sx',
              'harga' => 24000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            27 =>
            array (
              'id' => 28,
              'kode_bahan_baku' => 'B_DTY-06',
              'kode_supplier' => 'S_CN',
              'harga' => 24200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            28 =>
            array (
              'id' => 29,
              'kode_bahan_baku' => 'B_SDY-03',
              'kode_supplier' => 'S_Sx',
              'harga' => 28200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            29 =>
            array (
              'id' => 30,
              'kode_bahan_baku' => 'B_SDY-03',
              'kode_supplier' => 'S_To',
              'harga' => 28000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            30 =>
            array (
              'id' => 31,
              'kode_bahan_baku' => 'B_PSY-01',
              'kode_supplier' => 'S_Sn',
              'harga' => 24600,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            31 =>
            array (
              'id' => 32,
              'kode_bahan_baku' => 'B_PSY-01',
              'kode_supplier' => 'S_CN',
              'harga' => 24800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            32 =>
            array (
              'id' => 33,
              'kode_bahan_baku' => 'B_SILKRA-01',
              'kode_supplier' => 'S_Sn',
              'harga' => 24000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            33 =>
            array (
              'id' => 34,
              'kode_bahan_baku' => 'B_SILKRA-01',
              'kode_supplier' => 'S_Ca',
              'harga' => 24200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            34 =>
            array (
              'id' => 35,
              'kode_bahan_baku' => 'B_BORA-01',
              'kode_supplier' => 'S_Ia',
              'harga' => 27200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            35 =>
            array (
              'id' => 36,
              'kode_bahan_baku' => 'B_BORA-01',
              'kode_supplier' => 'S_Ca',
              'harga' => 27000,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            36 =>
            array (
              'id' => 37,
              'kode_bahan_baku' => 'B_DTY-07',
              'kode_supplier' => 'S_CN',
              'harga' => 24400,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            37 =>
            array (
              'id' => 38,
              'kode_bahan_baku' => 'B_DTY-07',
              'kode_supplier' => 'S_To',
              'harga' => 24200,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            38 =>
            array (
              'id' => 39,
              'kode_bahan_baku' => 'B_SDY-04',
              'kode_supplier' => 'S_To',
              'harga' => 28600,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            39 =>
            array (
              'id' => 40,
              'kode_bahan_baku' => 'B_SDY-04',
              'kode_supplier' => 'S_Ca',
              'harga' => 28800,
              'min_order' => 3000,
              'stock' => 1000000,
            ),
            40 =>
            array (
              'id' => 41,
              'kode_bahan_baku' => 'B_Obat-01',
              'kode_supplier' => 'S_BX',
              'harga' => 3000,
              'min_order' => 6000,
              'stock' => 1000000,
            ),
            41 =>
            array (
              'id' => 42,
              'kode_bahan_baku' => 'B_Obat-01',
              'kode_supplier' => 'S_ML',
              'harga' => 3200,
              'min_order' => 6000,
              'stock' => 1000000,
            ),
        );
        foreach ($users as $data) {
            $user = new BahanBakuSupplier;
            $user->id = $data['id'];
            $user->kode_bahan_baku = $data['kode_bahan_baku'];
            $user->kode_supplier = $data['kode_supplier'];
            $user->harga = $data['harga'];
            $user->min_order = $data['min_order'];
            $user->stock = $data['stock'];

            $user->save();
        }

        // $pegawais =
    }
}
