<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <style>
    body{
      line-height: normal;
      font-family: Arial, Helvetica, sans-serif;
    }
    .table {
      border-collapse: collapse;
      font-size: 11px;
    }

    table {
      font-size: 11px;
    }

    .table th, .table td {
      border: 1px solid black;
    }

    .table th {
      vertical-align: middle;
      text-align: center;
      background-color: lightgray;
    }
  </style>
<title>Laporan Pesanan Bulan {{$times[1]}} {{$times[0]}}</title>
</head>
<body>
<center>LAPORAN PEMESANAN (PO) PT. TATA CAKRA INVESTAMA
  <br> {{$times[1]}} {{$times[0]}}
</center>
<hr>
<table class="table">
  <tr>
      <th rowspan="2" width="25px">No</th>
      <th colspan="2">Tanggal</th>
      <th rowspan="2" width="100px">Nama Pemesan</th>
      <th rowspan="2" width="100px">Nama Barang</th>
      <th rowspan="2" width="60px">Harga</th>
      <th rowspan="2" width="50px">Jumlah<br>(Yard)</th>
      <th rowspan="2" width="100px">Total Harga</th>
      <th rowspan="2" width="90px">Keterangan</th>
  </tr>
  <tr>
      <th width="75px">Order</th>
      <th width="75px">Pengiriman</th>
  </tr>
  @php
      $sumJumlah = 0;
      $sumTotalHarga = 0;
  @endphp
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  @foreach ($pesanans as $i => $pesanan)
      <tr>
          <td align="center">{{$i+1}}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
          <td align="center">{{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
          <td>{{ $pesanan->konsumen->user->nama }}</td>
          <td>{{ $pesanan->produk->nama }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga / $pesanan->jumlah) }}</td>
          <td align="center">{{ rupiah($pesanan->jumlah) }}</td>
          <td align="right">Rp. {{ rupiah($pesanan->total_harga) }}</td>
          <td align="center">{{ $pesanan->jenis }}</td>
      </tr>
      @php
          $sumJumlah += $pesanan->jumlah;
          $sumTotalHarga += $pesanan->total_harga;
      @endphp
  @endforeach
  <tr>
      <td colspan="6" align="center"><b>Total</b></td>
      <td align="center"><b>{{ rupiah($sumJumlah) }}<b></td>
      <td align="right"><b>Rp. {{ rupiah($sumTotalHarga) }}<b></td>
      <td></td>
  </tr>
</table>
<br>
<br>
<table border="0">
<tr>
  <td>
      <div style="margin-left:530px">
          <center>
        Bandung, {{ date('d-m-Y') }}<br>
        {{ ucwords($level) }}
        <br><br><br><br>
        <b><u>{{ Auth::user()->nama }}</b></u>
          </center>
        </div>
  </td>
</tr>
</table>
</body>
</html>
