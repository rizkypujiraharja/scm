<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produksi extends Model
{
    const status = [
			['Menunggu Bahan Baku', 'warning'],
			['Selesai', 'success'],
			['Sedang diproses', 'info']
    	];

	protected $table = 'produksi';

    public $timestamps = false;

    public function permintaanBB()
    {
    	return $this->hasMany('App\PermintaanBahanBaku');
    }

    public function pesanan() {
        return $this->belongsTo('App\Pesanan');
    }
}
