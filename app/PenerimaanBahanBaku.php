<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenerimaanBahanBaku extends Model
{
    protected $table = 'penerimaan_bahan_baku';

	public $timestamps = false;

	public function eoq()
	{
		return $this->belongsTo('App\EOQ');
	}
}
