

                                <li class="{{ Request::is('konsumen/*') ? 'active' : '' }}">
                                    <a href="{{ route('konsumen.index') }}">
                                        <span class="pcoded-micon"><i class="ti-user"></i><b>K</b></span>
                                        <span class="pcoded-mtext">Data Konsumen</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('pesanan/*') ? 'active' : '' }}">
                                    <a href="{{ route('pesanan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Pemesanan</span>
                                        @if($jml['pesanan_persetujuan'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['pesanan_persetujuan']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                 <li class="{{ Request::is('pesanan/design-baru*') ? 'active' : '' }}">
                                    <a href="{{ route('pesanan.design.baru.index') }}">
                                        <span class="pcoded-micon"><i class="ti-receipt"></i><b>D</b></span>
                                        <span class="pcoded-mtext">Design Baru</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="{{ Request::is('pengadaan/*') ? 'active' : '' }}">
                                    <a href="{{ route('pengadaan.index') }}">
                                        <span class="pcoded-micon"><i class="ti-package"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Pengadaan</span>
                                        @if($jml['pengadaan_persetujuan'] > 0)
                                        <span class="pcoded-badge label label-success">{{$jml['pengadaan_persetujuan']}}</span>
                                        @endif
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                <li class="pcoded-hasmenu {{ Request::is('laporan*') ? 'active' : '' }}" dropdown-icon="style1" subitem-icon="style6">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-file"></i><b>P</b></span>
                                        <span class="pcoded-mtext">Laporan</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="{{ route('laporan.pesanan') }}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.subscription.bank">Laporan Pemesanan</span>
                                            <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>




