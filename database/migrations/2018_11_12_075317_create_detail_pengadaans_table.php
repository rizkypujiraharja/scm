<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPengadaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eoq', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pengadaan_id', 20);
            $table->string('kode_bahan_baku', 20);
            $table->string('kode_supplier', 20);
            $table->integer('harga');
            $table->float('sisa_stock', 12, 3);
            $table->float('kebutuhan', 12, 3);
            $table->float('min_order', 12, 3);
            $table->float('jumlah', 12, 3);
            $table->integer('waktu_antar');
            $table->integer('frekuensi');
            $table->integer('dikirim');
            $table->text('catatan');

            $table->foreign('pengadaan_id')->references('id')->on('pengadaan');
            $table->foreign('kode_bahan_baku')->references('kode_bahan_baku')->on('bahan_baku');
            $table->foreign('kode_supplier')->references('kode_supplier')->on('supplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eoq');
    }
}
