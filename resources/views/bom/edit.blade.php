@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Edit Data BOM</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bom.index') }}">BOM</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Edit BOM</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="{{ route('bom.update', $bom) }}" method="post" class="j-pro" id="j-pro" novalidate="" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="j-content">
                                <!-- start name -->
                                <div class="j-unit">
                                    <label class="j-label">Produk</label>
                                        <div class="j-input ">
                                            <label class="input select" id="show-elements-select">
                                                    <select name="produk" readonly>
                                                            <option value="{{$produk->kode_produk}}" {{ !empty($param['produk']) ? ($produk->kode_produk == $param['produk'] ? 'selected' : '') : '' }}>{{$produk->nama}}</option>
                                                    </select>
                                            </label>
                                        </div>
                                </div>
                                <!-- end name -->

                                <!-- start email phone -->
                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Bahan Baku</label>
                                        <div class="j-input">
                                            <input type="text" value="{{ $bahanBaku->nama }}" readonly="">
                                        </div>
                                    </div>
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Jumlah (takaran)</label>
                                        <div class="j-input {{ $errors->has('jumlah') ? 'j-error-view' : '' }}">
                                            <label class="j-icon-right" for="jumlah">
                                                <i class="icofont icofont-law"></i>
                                            </label>
                                            <input type="text" id="jumlah" name="jumlah" value="{{ old('jumlah') ?? $bom->jumlah }}">
                                            @if ( $errors->has('jumlah') )
                                                <span class="j-error-view">{{$errors->first('jumlah')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="j-divider j-gap-bottom-25"></div>
                            </div>
                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus bom bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
