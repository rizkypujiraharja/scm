@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Data Kendaraan</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('kendaraan.index') }}">Kendaraan</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Kendaraan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="{{ route('kendaraan.store') }}" method="post" class="j-pro" id="j-pro" novalidate="" enctype="multipart/form-data">
                            @csrf
                            <div class="j-content">

                                <!-- start email phone -->
                                <div class="j-row">
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">No Pol</label>
                                        <div class="j-input {{ $errors->has('nopol') ? ' j-error-view' : '' }}">
                                            <label class="j-icon-right" for="nopol">
                                                <i class="icofont icofont-barcode"></i>
                                            </label>
                                            <input type="text" id="nopol" name="nopol" value="{{ old('nopol') }}">
                                        @if ( $errors->has('nopol') )
                                            <span class="j-error-view">{{$errors->first('nopol')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="j-span6 j-unit">
                                        <label class="j-label">Kapasitas (PCS)</label>
                                        <div class="j-input {{ $errors->has('kapasitas') ? ' j-error-view' : '' }}">
                                            <label class="j-icon-right" for="kapasitas">
                                                <i class="icofont icofont-box"></i>
                                            </label>
                                            <input type="text" id="kapasitas" name="kapasitas" value="{{ old('kapasitas') }}">
                                        @if ( $errors->has('kapasitas') )
                                            <span class="j-error-view">{{$errors->first('kapasitas')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- start name -->
                                <div class="j-unit">
                                    <label class="j-label">Nama Supir</label>
                                    <div class="j-input {{ $errors->has('name') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="name">
                                            <i class="icofont icofont-ui-user"></i>
                                        </label>
                                        <input type="text" id="name" name="name" value="{{ old('name') }}">
                                        @if ( $errors->has('name') )
                                            <span class="j-error-view">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->
                                <!-- start response from server -->
                                <div class="j-response"></div>
                                <!-- end response from server -->
                            </div>
                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus kendaraan bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
