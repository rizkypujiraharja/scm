@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-forms.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Data BOM</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bom.index') }}">BOM</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah BOM</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- clone elements card start -->
            <div class="card">
                <div class="card-block">
                    <div class="wrapper wrapper-640">
                        <form action="{{ route('bom.store') }}" method="POST" class="j-forms j-pro" id="j-forms" novalidate>
                            @csrf
                            <!-- end /.header-->
                            <div class="content">
                                <div class="j-unit">
                                    <label class="j-label">Produk</label>
                                    <div class="j-input ">
                                        <label class="input select" id="show-elements-select">
                                                <select name="produk">
                                                     @foreach ($produks as $produk)
                                                        <option value="{{$produk->kode_produk}}" {{ !empty($param['produk']) ? ($produk->kode_produk == $param['produk'] ? 'selected' : '') : '' }}>{{$produk->nama}}</option>
                                                    @endforeach
                                                </select>
                                        </label>
                                    </div>
                                </div>
                                @foreach ($jenis as $j)
                                <div class="span4 j-unit">
                                    <label class="j-label">Jenis</label>
                                    <div class="j-input">
                                        <label class="j-icon-right" for="jumlah">
                                            <i class="icofont icofont-law"></i>
                                        </label>
                                        <input type="text" id="jenis" name="jenis[]" value="{{ $j }}" readonly="">
                                    </div>
                                </div>
                                <div class="span5 j-unit">
                                    <label class="j-label">Bahan Baku</label>
                                    <div class="j-input ">
                                        <label class="input select" id="show-elements-select">
                                                <select name="bahan_baku[]">
                                                     @foreach ($bahanBakus as $bahanBaku)
                                                        <option value="{{$bahanBaku->kode_bahan_baku}}" selected>
                                                            {{$bahanBaku->nama}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="span3 j-unit">
                                    <label class="j-label">Jumlah (takaran)</label>
                                    <div class="j-input">
                                        <label class="j-icon-right" for="jumlah">
                                            <i class="icofont icofont-law"></i>
                                        </label>
                                        <input type="text" id="jumlah" name="jumlah[]">
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <!-- end /.content -->
                            <div class="footer">
                                <button type="submit" class="btn btn-primary m-b-0">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
            <!-- clone elements card end -->
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')

    <script type="text/javascript" src="{{ asset('guru/default/assets/pages/j-pro/js/jquery-cloneya.min.js') }}"></script>
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus bom bernama "+ title +" ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});

$(document).ready(function(){

        /***************************************/
        /* Cloned element */
        /***************************************/
        $('.clone-widget').cloneya();

        $('.clone-rightside-btn-1').cloneya();

        $('.clone-rightside-btn-2').cloneya();

        $('.clone-leftside-btn-1').cloneya();

        $('.clone-leftside-btn-2').cloneya();

        $('.clone-link').cloneya();
        /***************************************/
        /* end Cloned element */
        /***************************************/

    });
</script>
@endsection
