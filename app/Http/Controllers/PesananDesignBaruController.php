<?php

namespace App\Http\Controllers;

use App\{Pesanan, Produk, Konsumen, BOM, BahanBaku, PesananDesignBaru};
use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth, Image, DB, Exception;

class PesananDesignBaruController  extends Controller
{
    public function index()
    {
        if(Auth::user()->konsumen()->exists()){
            $pesanans = PesananDesignBaru::where('konsumen_id', Auth::user()->konsumen->id)->orderBy('id', 'DESC')->paginate(10);
        }else{
            $pesanans = PesananDesignBaru::orderBy('id', 'DESC')->paginate(10);
        }
        $status = PesananDesignBaru::status;

        return view('pesanan.design-baru.index',compact('pesanans', 'status'));
    }

    public function Create()
    {
        $konsumens = Konsumen::get();

        return view('pesanan.design-baru.create', compact('konsumens'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'jumlah' => 'numeric|min:30000',
            'design'    => 'required|file'
        ],[
            'jumlah.min' => 'Pemesanan minimal 30000'
        ]);

        $imgname = Null;
        if( !is_null($request->design) ){
            $imageLocation = public_path('images/design').'/';

            $imgname = 'design-'.time().'.'.$request->design->getClientOriginalExtension();
            Image::make($request->design)
                    ->save($imageLocation .$imgname);
        }

        $design = new PesananDesignBaru;

        if($request->konsumen == 'konsumen'){
            $design->konsumen_id = Auth::user()->konsumen->id;
        }else{
            $design->konsumen_id = $request->konsumen;
        }
        $design->nama = $request->nama;
        $design->design = $imgname;
        $design->jumlah = $request->jumlah;
        $design->tanggal = date('Y-m-d');
        $design->status = 0;

        $design->save();

        return redirect()->route('pesanan.design.baru.index')->with('alert-success', 'Berhasil menambah pesanan design baru');
    }

    public function tolak($id)
    {
        $pesanan = PesananDesignBaru::where('status', 0)->find($id);

        if(is_null($pesanan)) return redirect()->back()->with('alert-error', 'Pesanan tidak ditemukan');

        $pesanan->status = 2;
        $pesanan->save();

        return redirect()->back()->with('alert-success', 'Pesanan #'.$id.' ditolak!');
    }

    public function proses($id)
    {
        $designBaru = PesananDesignBaru::where('status', 0)->findOrFail($id);

        $bahanBakus = BahanBaku::get();

        $jenis = ['Benang Lusi', 'Benang Pakan','Obat Kanji'];

        return view('pesanan.design-baru.proses', compact('designBaru', 'bahanBakus', 'jenis'));
    }

    public function buatPesanan(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'max:50|string',
            'harga' => 'numeric|digits_between:4,9',
            'max' => 'numeric|min:1',
            'gambar' => 'required|image'
            ]);

        DB::beginTransaction();

        try {
            $designBaru = PesananDesignBaru::findOrFail($id);
            $designBaru->status = 1;
            $designBaru->tanggal_setuju = date('Y-m-d');
            $designBaru->save();

            $n = explode(' ', $request->name);
            $s = "";
            foreach($n as $k) {
                $s .= ucwords($k[0]);
            }
            $kode = 'K'.$s;

            $cek = Produk::where('kode_produk', 'like', $kode.'\_%')->orderBy('kode_produk', 'DESC')->first();

            if( is_null($cek) ){
                $kode = $kode.'_001';
            }else{
                $last = explode('_', $cek->kode_produk);
                $new = sprintf("%'03d", $last[1]+1);
                $kode = $kode.'_'.$new;
            }

            $imgname = Null;

            if( !is_null($request->gambar) ){
                $imageLocation = public_path('images/produk').'/';
                $imgname = $kode.'-'.time().'.'.$request->gambar->getClientOriginalExtension();
                Image::make($request->gambar)->resize(400, 300)
                        ->save($imageLocation .$imgname);
            }
           
            $produk = New Produk;
            $produk->kode_produk = $kode;
            $produk->nama = $request->name;
            $produk->harga = $request->harga;
            $produk->max_produksi_per_hari = $request->max;
            $produk->save();
            $old = [];

            for ($i=0; $i < count($request->bahan_baku) ; $i++) {

                $cek = BOM::where('kode_produk', $produk->kode_produk)
                        ->where('kode_bahan_baku', $request->bahan_baku[$i])
                        ->count();
                
                // if ($cek > 0 || in_array($request->bahan_baku[$i], $old)){
                //     throw new Exception("Bahan Baku Tidak Boleh Duplikat");
                // }

                if (intVal($request->jumlah[$i]) < 0){
                    throw new Exception("Jumlah (takaran tidak boleh kosong)");
                }

                $bom = new BOM;

                $bom->kode_produk = $produk->kode_produk;
                $bom->kode_bahan_baku = $request->bahan_baku[$i];
                $bom->jenis = $request->jenis[$i];
                $bom->jumlah = $request->jumlah[$i];

                $bom->save();
                $old[] = $request->bahan_baku[$i];

            }


                $konsumen = $designBaru->konsumen_id;
                $status = 1;
                $pegawai = Auth::user()->id;

            $n = explode(' ', $request->name);
                $kode = 'G-'.date('ym');

                $cek = Pesanan::where('id', 'like', $kode.'%')->orderBy('id', 'DESC')->first();

                if( is_null($cek) ){
                    $kode = $kode.'.01';
                }else{
                    $last = explode('.', $cek->id);
                    $new = sprintf("%'02d", $last[1]+1);
                    $kode = $kode.'.'.$new;
                }

            $pesanan = New Pesanan;
            $pesanan->id = $kode;
            $pesanan->pegawai_id = $pegawai;
            $pesanan->konsumen_id = $konsumen;
            $pesanan->kode_produk = $produk->kode_produk;
            $pesanan->jumlah = $request->jml_pesan;
            $pesanan->total_harga = $request->jml_pesan * $produk->harga;
            $pesanan->jenis = 'designbaru';
            $pesanan->status = $status;
            $pesanan->tgl_pesan = date('Y-m-d');
            $pesanan->save();

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());
        }

        return redirect()->route('pesanan.design.baru.index')->with('alert-success', 'Berhasil Menambah Pesanan');
    }
}
