@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Pengiriman Pesanan</h4>
                    <span>Pilih pesanan untuk pengiriman</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('pengiriman.index') }}">Pengiriman</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Konsumen</th>
                            <th>Produk</th>
                            <th>Jumlah</th>
                            <th>Jumlah Dikirim</th>
                            <th>Permintaan Pengiriman</th>
                            <th>Sisa Kirim</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $pesanan->id }}</td>
                            <td>{{ $pesanan->konsumen->user->nama }}</td>
                            <td>{{ $pesanan->produk->nama }}</td>
                            <td>{{ $pesanan->jumlah }}</td>
                            <td>{{ rupiah($pesanan->jmlDikirim()) }}</td>
                            <td>{{ rupiah($pesanan->jmlRequestPengiriman()) }}</td>
                            <td>{{ rupiah($pesanan->jumlah - $pesanan->jmlDikirim()) }}</td>
                            <td>
                              <a href="{{ route('pengiriman.pesanan', $pesanan) }}">
                                  <button class="btn btn-mini btn-primary">Pengiriman</button>
                              </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $pesanans->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
