@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-layers bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kelola Penerimaan</h4>
                    <span>Penerimaan Bahan baku dari supplier</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('bom.index') }}">Penerimaan</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID Pengadaan</th>
                            <th>Nama Bahan Baku</th>
                            <th>Jumlah</th>
                            <th>Tanggal</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr style="background:#e9ecef">
                            <form action="" method="GET" name="search_form" id="search_form">
                            <td>
                                <select class="form-control" name="filter[produk]">
                                    <option value="">Semua</option>
                                    @foreach ($produks as $produk)
                                        <option value="{{$produk->kode_produk}}" {{ !empty($param['produk']) ? ($produk->kode_produk == $param['produk'] ? 'selected' : '') : '' }}>[{{$produk->kode_produk}}] - {{$produk->nama}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-info btn-bordered btn-sm">Cari
                                </button>
                                <button type="reset" onclick="javascript:window.location.assign('{{ route('bom.index') }}');" class="btn btn-info btn-bordered btn-sm">
                                    Reset
                                </button>
                            </td>
                            </form>
                        </tr> --}}
                    @foreach ($penerimaanData as $penerimaan)
                        <tr>
                            <td>{{ $penerimaan->eoq->pengadaan->id }}</td>
                            <td>{{ $penerimaan->eoq->bahanBaku->nama }}</td>
                            <td>{{ $penerimaan->jumlah }}</td>
                            <td>{{ date('d-m-Y', strtotime($penerimaan->tanggal)) }}</td>
                            <td>
                                @if ($penerimaan->status == 0 )
                                    <a href="{{ route('penerimaan.terima', $penerimaan->id) }}"><button class="btn btn-mini btn-primary">Terima</button></a>
                                @else
                                    <span class="label label-success">Selesai</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div style="padding: 15px;width: 100%">
                    <center>{{ $penerimaanData->appends(request()->except('page'))->links() }}</center>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script type="text/javascript">
$('button#delete').on('click', function(){
    var href = $(this).attr('href');
    var title = $(this).data('title');
    swal({
      title: "Anda yakin akan menghapus Penerimaan ini ?",
      text: "Setelah dihapus data tidak dapat dikembalikan !",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
      }
    });
});
</script>
@endsection
