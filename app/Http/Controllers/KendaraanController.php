<?php

namespace App\Http\Controllers;

use App\Kendaraan;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Exception, DB;

class KendaraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param = $request->filter;
        $kendaraans = Kendaraan::where('nopol', 'like', '%' . ($request->filter['nopol'] ?? '')  . '%')
                ->where('kapasitas', 'like', ($request->filter['kontak'] ?? '%') )
                ->where('supir', 'like', ('%'.$request->filter['supir'].'%' ?? '%') )
                ->paginate(10);

        return view('kendaraan.index', compact('kendaraans', 'param'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kendaraan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nopol' => 'string|min:5|max:11|unique:kendaraan,nopol',
            'kapasitas' => 'numeric|digits_between:1,5',
            'name' => 'required|max:50|alpha',
        ]);

        $kendaraan = new Kendaraan;
        $kendaraan->nopol = $request->nopol;
        $kendaraan->kapasitas = $request->kapasitas;
        $kendaraan->supir = $request->name;
        $kendaraan->save();

        return redirect()->route('kendaraan.index')->with('alert-success', 'Berhasil menambah kendaraan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function show(Kendaraan $kendaraan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function edit(Kendaraan $kendaraan)
    {
        return view('kendaraan.edit', compact('kendaraan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kendaraan $kendaraan)
    {
        $this->validate($request, [
            'nopol' => [
                'string',
                'min:5',
                'max:11',
                Rule::unique('kendaraan')->ignore($kendaraan->id),
            ],
            'kapasitas' => 'numeric|digits_between:1,5',
            'name' => 'required|max:50|alpha',
        ]);

        $kendaraan->nopol = $request->nopol;
        $kendaraan->kapasitas = $request->kapasitas;
        $kendaraan->supir = $request->name;
        $kendaraan->save();

        return redirect()->route('kendaraan.index')->with('alert-success', 'Berhasil mengubah kendaraan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kendaraan  $kendaraan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kendaraan $kendaraan)
    {
        DB::beginTransaction();
        try {

            $kendaraan->delete();

            DB::commit();

        } catch (Exception $e) {

            DB::rollBack();
            return redirect()->back()->withInput()->with('alert-error', $e->getMessage());

        }

        return redirect()->route('kendaraan.index')->with('alert-success', 'Berhasil menghapus kendaraan!');
    }

    public function switchStatus($id) {
        $kendaraan = Kendaraan::findOrFail($id);

        if($kendaraan->status == 1){
            $kendaraan->status = 0;
        }else{
            $kendaraan->status = 1;
        }
        $kendaraan->save();

        return redirect()->back()->with('alert-success', 'Berhasil mengubah status kendaraan');
    }
}
