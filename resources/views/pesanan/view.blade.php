@extends('layouts.app')

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container">
        <!-- Main content starts -->
        <div>
            <!-- Invoice card start -->
            <div class="card">
                <div class="row invoice-contact">
                    <div class="col-md-12">
                        <div class="invoice-box row">
                            <div class="col-sm-12">
                                <table class="table table-responsive invoice-table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img src="{{ asset('images/logo.png') }}" class="m-b-10" alt="PT. Tata Cakra Investama" style="width: 100px;float: left;padding: 15px">
                                                <h5>PT. Tata Cakra Investama</h5>
                                                <p>Jl. Raya Bandung – Garut Km.28
                                                    <br>Cicalengka 40395 Bandung Indonesia
                                                    <br><b><i>Telp (022) 779-6567 ( hunting ) Fax (022) 779-8068 Mail pt.tata.cakra.investama@gmail.com</i></b>
                                                </p>
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                            <td><a href="mailto:tatacakraInvestama@gmail.com" target="_top">mailto:tatacakraInvestama@gmail.com</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>06464132354</td>
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row invoive-info">
                        <div class="col-md-4 col-xs-12 invoice-client-info">
                            <h6>Data Konsumen :</h6>
                            <h6 class="m-0">{{ $pesanan->konsumen->user->nama }}</h6>
                            <p class="m-0 m-t-10">{{ $pesanan->konsumen->user->alamat }}</p>
                            <p class="m-0">{{ $pesanan->konsumen->user->kontak }}</p>
                            <p><a href="mailto:{{$pesanan->konsumen->user->email}}" target="_top">{{$pesanan->konsumen->user->email}}</a></p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h6>Informasi Pesanan :</h6>
                            <table class="table table-responsive invoice-table invoice-order table-borderless">
                                <tbody>
                                    <tr>
                                        <th>Tanggal Order</th>
                                        <td> : {{ date('d-m-Y', strtotime($pesanan->tgl_pesan)) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Selesai</th>
                                        <td> : {{ date('d-m-Y', strtotime($pesanan->tgl_selesai)) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td> : 
                                            <span class="label label-{{$status[$pesanan->status][1]}}">{{$status[$pesanan->status][0]}}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h6 class="m-b-20">ID Pesanan <span>#{{$pesanan->id}}</span></h6>
                            <h6 class="text-uppercase text-primary">Total Harga :
                                <span>Rp. {{ rupiah($pesanan->total_harga) }}</span>
                            </h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table  invoice-detail-table">
                                    <thead>
                                        <tr class="thead-default">
                                            <th>Produk</th>
                                            <th>Jumlah Pesanan (Yard)</th>
                                            <th>Harga</th>
                                            <th>Total Harga</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h6>{{ $pesanan->produk->nama }}</h6>
                                            </td>
                                            <td>{{ rupiah($pesanan->jumlah) }}</td>
                                            <td>Rp. {{ rupiah($pesanan->produk->harga) }}</td>
                                            <td>Rp. {{ rupiah($pesanan->jumlah * $pesanan->produk->harga) }}</td>
                                        </tr>
                                        @if ( $pesanan->jenis == 'bbkonsumen')
                                        <tr>
                                            <td>
                                                <h6 style="float: right;">Bahan Baku Dari Konsumen</h6>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td>- Rp. {{ rupiah($pesanan->minus_harga_bb_konsumen) }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                @if ( $pesanan->jenis == 'bbkonsumen')
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-responsive invoice-table invoice-total">
                                <tbody>
                                    <tr class="text-info">
                                        <td>
                                            <hr>
                                            <h5 class="text-primary">Total :</h5>
                                        </td>
                                        <td>
                                            <hr>
                                            <h5 class="text-primary">Rp. {{ rupiah($pesanan->total_harga) }}</h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                    {{-- <div class="row">
                        <div class="col-sm-12">
                            <h6>Terms And Condition :</h6>
                            <p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
                        </div>
                    </div> --}}
                </div>
            </div>
            <!-- Invoice card end -->
            <div class="row text-center">
                <div class="col-sm-12 invoice-btn-group text-center">
                        <a href="{{ route('pesanan.index') }}">
                            <button class="btn btn-inverse">Kembali</button>
                        </a>
                    @if($level != 'konsumen')
                        @if ($pesanan->status == 0)
                            <a href="{{ route('pesanan.proses', $pesanan->id) }}">
                                <button class="btn btn-primary">Setujui</button>
                            </a>
                            <a href="{{ route('pesanan.tolak', $pesanan->id) }}">
                                <button class="btn btn-danger">Tolak</button>
                            </a>
                        @endif
                    @endif

                    @if($pesanan->jmlDikirim() >= $pesanan->jumlah)
                      <a href="{{ route('pesanan.selesai', $pesanan->id) }}">
                          <button class="btn btn-success">Selesai</button>
                      </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Container ends -->
</div>
@endsection
