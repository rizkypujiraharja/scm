@extends('layouts.app')

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Data Produksi</h4>
                    <span>Manage Data Produksi</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('produksi.index') }}">Produksi</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="card">
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Konsumen</th>
                            <th>Produk</th>
                            <th>Jumlah (Yard)</th>
                            <th>Selesai</th>
                                    @if ($level != 'konsumen')
                            <th width="150px">Produksi</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $pesanan->id }}</td>
                            <td>{{ $pesanan->konsumen->user->nama }}</td>
                            <td>{{ $pesanan->produk->nama }}</td>
                            <td>{{ rupiah($pesanan->jumlah) }}</td>
                            <td>{{ rupiah($pesanan->jmlProduksi()) }}</td>
                          @if($level != 'konsumen')
                            @if ($pesanan->jmlProduksi() < $pesanan->jumlah)
                            <td align="center">
                                <a href="{{ route('produksi.index-pesanan', $pesanan->id) }}" class="btn btn btn-sm btn-success">
                                        + Produksi
                                </a>
                            </td>
                            @endif
                          @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

