@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/demo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('guru/default/assets/pages/j-pro/css/j-pro-modern.css') }}">
@endsection

@section('content')
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-box bg-c-green"></i>
                <div class="d-inline">
                    <h4>Tambah Data Produk</h4>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ route('index') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('produk.index') }}">Produk</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Tambah Produk</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="j-wrapper j-wrapper-640">
                        <form action="{{ route('produk.store') }}" method="post" enctype="multipart/form-data" class="j-pro" id="j-pro" novalidate="" enctype="multipart/form-data">
                            @csrf
                            <div class="j-content">
                                <!-- start name -->
                                <div class="j-unit">
                                    <label class="j-label">Nama Produk</label>
                                    <div class="j-input {{ $errors->has('name') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="name">
                                            <i class="icofont icofont-box"></i>
                                        </label>
                                        <input type="text" id="name" name="name" value="{{ old('name') }}">
                                        @if ( $errors->has('name') )
                                            <span class="j-error-view">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->

                                <div class="clearfix"></div>

                                <!-- start name -->
                                <div class="j-span6 j-unit">
                                    <label class="j-label">Mak Produksi (Yard/Hari)</label>
                                    <div class="j-input {{ $errors->has('max') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="max">
                                            <i class="icofont icofont-settings"></i>
                                        </label>
                                        <input type="text" id="max" name="max" value="{{ old('max') }}">
                                        @if ( $errors->has('max') )
                                            <span class="j-error-view">{{$errors->first('max')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->

                                <!-- start name -->
                                <div class="j-span6 j-unit">
                                    <label class="j-label">Harga</label>
                                    <div class="j-input {{ $errors->has('harga') ? ' j-error-view' : '' }}">
                                        <label class="j-icon-right" for="harga">
                                            <i class="icofont icofont-coins"></i>
                                        </label>
                                        <input type="text" id="harga" name="harga" value="{{ old('harga') }}">
                                        @if ( $errors->has('harga') )
                                            <span class="j-error-view">{{$errors->first('harga')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <!-- end name -->
                                <div class="clearfix"></div>

                                <div class="j-unit">
                                  <label class="j-label">Gambar</label>
                                  <div class="j-input j-append-small-btn {{ $errors->has('gambar') ? ' j-error-view' : '' }}">
                                    <div class="j-file-button">
                                        Browse
                                        <input type="file" id="file2" name="gambar" onchange="document.getElementById('file2_input').value = this.value;">
                                    </div>
                                    <input type="text" id="file2_input" readonly="" placeholder="Upload Gambar Produk">
                                    <span class="j-hint">Dimensi gambar 4:3 </span>
                                    @if ( $errors->has('gambar') )
                                        <span class="j-error-view">{{$errors->first('gambar')}}</span>
                                    @endif
                                  </div>
                                </div>
                                <!-- start response from server -->
                                <div class="j-response"></div>
                                <!-- end response from server -->
                            </div>
                            <!-- end /.content -->
                            <div class="j-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                            <!-- end /.footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
