<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Pesanan,Pengiriman,Pengadaan, Pegawai};
use PDF;

class LaporanController extends Controller
{
    public function pesanan()
    {
        return view('laporan.pesanan');
    }

    public function pesananCetak(Request $request)
    {
        $month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $times = [$request->year, $month[intval($request->month) - 1]];

        $pesanans = Pesanan::whereNotIn('status', [0])
                        ->where('tgl_pesan', 'like', '%'.$request->year.'-'.$request->month.'%')
                        ->get();

        $pdf = PDF::loadView('cetak.laporan.pesanan', compact('times', 'pesanans'))
                    ->setPaper('a4', 'portrait');

        return $pdf->stream('.pdf');
    }

    public function pengadaan()
    {
        return view('laporan.pengadaan');
    }

    public function pengadaanCetak(Request $request)
    {
        $month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $times = [$request->year, $month[intval($request->month) - 1]];

        $pengadaans = Pengadaan::whereNotIn('status', [0])
            ->where('tanggal', 'like', '%'.$request->year.'-'.$request->month.'%')
            ->get();
        $pengadaans->load('eoq');

        $manager = Pegawai::where('jabatan', 'manager')->first();

        $pdf = PDF::loadView('cetak.laporan.pengadaan', compact('times', 'pengadaans', 'manager'))
                    ->setPaper('a4', 'portrait');

        return $pdf->stream('.pdf');
    }

    public function pengiriman()
    {
        return view('laporan.pengiriman');
    }

    public function pengirimanCetak(Request $request)
    {
        $month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $times = [$request->year, $month[intval($request->month) - 1]];

        $pengirimans = Pengiriman::
                    where('tanggal_kirim', 'like', '%'.$request->year.'-'.$request->month.'%')
                    ->orderBy('pesanan_id')
                    ->get();

        $manager = Pegawai::where('jabatan', 'manager')->first();

        $pdf = PDF::loadView('cetak.laporan.pengiriman', compact('times', 'pengirimans', 'manager'))
                    ->setPaper('a4', 'portrait');

        return $pdf->stream('.pdf');
    }
}
