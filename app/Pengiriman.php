<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    const status = [
        ['Menunggu Pengiriman', 'warning'],
        ['Dikirim', 'info'],
        ['Diterima', 'success']
    ];

    protected $table = 'pengiriman';
    public $timestamps = false;

    public function pesanan() {
        return $this->belongsTo('App\Pesanan');
    }

    public function kendaraan() {
        return $this->belongsTo('App\Kendaraan');
    }

}
