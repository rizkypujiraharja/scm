<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesananDesignBaruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan_design_baru', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('konsumen_id')->unsigned();
            $table->string('nama');
            $table->string('design');
            $table->integer('jumlah');
            $table->date('tanggal');
            $table->integer('status');

            $table->foreign('konsumen_id')->references('id')->on('konsumen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan_design_baru');
    }
}
